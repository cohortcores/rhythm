/** Copyright (c) 2004-2009 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2009 Backend Simulator (BESIM)  Banking Command Module 

*/

#define VALID(v,n,x)  (((v>=n)&&(v<=x))?(1):(0))
			 
#define CUBLEN 256

#define DOBANKING "DoBankingCommand() Called\n"

#define BOILERPLATE_START \
"<html>\n"\
"<head><title>SPECweb2009 BESIM</title></head>\n"\
"<body>\n"\
"<p>SERVER_SOFTWARE = Apache/2.2.22 (Unix) PHP/5.4.0 mod_fastcgi/mod_fastcgi-SNAP-0910052141\n"\
"<p>REMOTE_ADDR = 127.0.0.1\n"\
"<p>SCRIPT_NAME = /fcgi-bin/besim_fcgi.fcgi\n"\
"<p>QUERY_STRING = %s\n"\
"<pre>\n"

#define BOILERPLATE_END "\n</pre>\n</body></html>\n"

typedef struct {
  int acct_num;
  int acct_type;
  int dollars;
  int cents;
  int ttl_dep_dolrs;
  int ttl_dep_cents;
  int avg_dep_dolrs;
  int avg_dep_cents;
  int ttl_wthdrw_dolrs;
  int ttl_wthdrw_cents;
  int avg_wthdrw_dolrs;
  int avg_wthdrw_cents;
} besim_acct_info;

#define RandomTime 1370825392
#define MinUserId 1
#define MaxUserId 25000
#define UidRange 25000
#define RandomLoad 500
#define ResetDate "20130609"
#define CheckURLBase "/home/sandeep/cohort/sites/specweb/php/bank/images/subdir0000"
#define NumChkSubdirs 10

/*
  Banking Command: 1 - LoginPasswd
  Request Params:  N_UserID
  Response:		    N_Password
  Script Mapping:  Login
  DB Op:		    Select
*/
 
int LoginPasswd (char* Arg[], int ArgCnt, char* MsgTxt){

    MD5_CTX context;
    unsigned char digest[16];

    int i;
    char *ptr;

    ptr = MsgTxt;

/*  This code assumes the user's passwd is the same as the USER_ID and
    returns the MD5 for the 10 characters in that numeric string
*/

    MD5_Init(&context);
    MD5_Update(&context, (unsigned char *)Arg[0], strlen(Arg[0]));
    MD5_Final(digest, &context);

//    ptr +=  sprintf(ptr,OK_STATUS);
    for(i=0; i<16; i++)
      ptr +=  sprintf(ptr,"%02x", digest[i]);
    sprintf(ptr,"\n");
    /*sprintf(ptr,"Debug: Arg0=%s, strlen Arg0=%d\n", 
      (unsigned char *)Arg[0], strlen(Arg[0]));*/


    return 0;

}



		 
/*
  Banking Command: 2 - AcctBalances
  Request Params:  N_UserID
  Response:		    N_NumberOfAccts Lines: N_Acct_#&N_Type&D_Balance
  Script Mapping:  Login, Order_check, Transfer
  DB Op:		    Select
*/

int AcctBalances (char* Arg[], int ArgCnt, char* MsgTxt){

    besim_acct_info Accts[10];
    int i;
    int uid;
    int num_of_accts;
    char *ptr;

    ptr = MsgTxt;

    uid = atoi(Arg[0]);
    num_of_accts = (uid % 10) +1;
    
		 
//    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_accts; i++) {
      Accts[i].acct_num = (uid + UidRange + i);
      Accts[i].acct_type = (RandomTime % 10) + i;
      Accts[i].dollars = (RandomTime % 10000) + (uid % (10 + i)) ;
      Accts[i].cents = ((uid+i) % 100);
      
      ptr += sprintf(ptr, "%010d&%02d&%d.%02d\n", Accts[i].acct_num, 
		 Accts[i].acct_type, Accts[i].dollars, Accts[i].cents);
    }
    return 0;
}



/*
  Banking Command: 3 -AcctSummary
  Request Params:  N_UserID
  Response:		    N_NumberOfAcctsLines: N_Acct_#&N_Type&D_Balance&\
		 		    D_TotalDeposit&D_AvgDeposit&D_TotalWitdrawal&D_AvgWithdrawal
  Script Mapping:  Account_summary
  DB Op:		    Select


*/
 
int AcctSummary (char* Arg[], int ArgCnt, char* MsgTxt) {

    besim_acct_info Accts[10];
    int i;
    int uid;
    int num_of_accts;
    char *ptr;

    ptr = MsgTxt;

    uid = atoi(Arg[0]);
    num_of_accts = (uid % 10) +1;
		 
    //ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_accts; i++) {
      Accts[i].acct_num = (uid + UidRange + i);
      Accts[i].acct_type = (RandomTime % 10) + i;
      Accts[i].dollars = (RandomTime % 10000) + (uid % (10 + i)) ;
      Accts[i].cents = (uid % 100);
      Accts[i].ttl_dep_dolrs = (Accts[i].dollars % 100) + (i * 100);
      Accts[i].ttl_dep_cents =  Accts[i].cents;
      Accts[i].avg_dep_dolrs = Accts[i].dollars % 100;
      Accts[i].avg_dep_cents = Accts[i].cents;
      Accts[i].ttl_wthdrw_dolrs = Accts[i].dollars % 99 + (i * 50);
      Accts[i].ttl_wthdrw_cents = Accts[i].cents;
      Accts[i].avg_wthdrw_dolrs = Accts[i].dollars % 99;
      Accts[i].avg_wthdrw_cents = Accts[i].cents;

      ptr += sprintf(ptr,
        "%010d&%02d&%d.%02d&%d.%02d&%d.%02d&%d.%02d&%d.%02d\n", 
        Accts[i].acct_num, Accts[i].acct_type, 
        Accts[i].dollars, Accts[i].cents,
        Accts[i].ttl_dep_dolrs, Accts[i].ttl_dep_cents, 
        Accts[i].avg_dep_dolrs, Accts[i].avg_dep_cents, 
        Accts[i].ttl_wthdrw_dolrs, Accts[i].ttl_wthdrw_cents, 
        Accts[i].avg_wthdrw_dolrs, Accts[i].avg_wthdrw_cents);
    }
    return 0;

}


		 
/*
  Banking Command: 4 - CheckDetail1
  Request Params:  N_UserID&N_Check
  Response:		    N_Acct&S_FrontURL&S_BackURL
  Script Mapping:  Check_detail_html, Check_detail_image
  DB Op:		    Select

  Note: URL's for check images are created using the base URL path info 
  supplied with the Reset (0) Command and appending:
  "/user<10char_UID>/<CIF|CIB><6digit_Check_Number>"
  (where CIF stands for check image front and CIB stands for check image back)
*/

int CheckDetail (char* Arg[], int ArgCnt, char* MsgTxt){

    int uid;
    int check_number;
    int acct_num;
    char *ptr;
    char CheckFront[CUBLEN];
    char CheckBack[CUBLEN];

    ptr = MsgTxt;

    uid = atoi(Arg[0]);
    check_number = atoi(Arg[1]);

    acct_num = uid + UidRange;
	/* add support for subdirectories -- GWK */
	if (NumChkSubdirs > 0) {
		char tmpBuf[CUBLEN];
		char buffer[10];
		int subdirLen;
		int baseStrLen = strlen(CheckURLBase);
		int subdirNum = uid%NumChkSubdirs;
		if (subdirNum == 0) subdirNum = NumChkSubdirs;
		subdirLen = sprintf(buffer, "%d", subdirNum);
		strncpy(tmpBuf,CheckURLBase,baseStrLen-subdirLen);
                /* null terminate char string */
		tmpBuf[baseStrLen-subdirLen] = 0; 
		strcat(tmpBuf, buffer);
		sprintf(CheckFront, "%s/user%010d/CIF%06d", 
			tmpBuf, uid, check_number);
		sprintf(CheckBack, "%s/user%010d/CIB%06d", 
			tmpBuf, uid, check_number);
	}
	else {
		sprintf(CheckFront, "%s/user%010d/CIF%06d",
			CheckURLBase, uid, check_number);
		sprintf(CheckBack, "%s/user%010d/CIB%06d", 
			CheckURLBase, uid, check_number);
	}
    //ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%010d&%s&%s\n", acct_num, CheckFront, CheckBack);
    return 0;

}


		  
/*
  Banking Command: 5 - Bill Payment
  Request Params:  N_UserID
  Response:		    N_NumberOfPayees Lines: N_PayeeId&D_Payment&N_Date
  Script Mapping:  Bill_pay
  DB Op:		    Select
*/

int BillPayment (char* Arg[], int ArgCnt, char* MsgTxt){
    
    int i;
    int uid;
    int bills;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);

    bills = uid%10 + 1;
    //ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < bills; i++) {
      ptr += sprintf(ptr, "%s&%s&%s\n",PayeesId[i], Payments[i], 
		 ResetDate);
    }
    return 0;
}


		 
/*
  Banking Command: 6 - PostPayee
  Request Params:  
		N_UserID&N_PayeeID&S_Name&S_Address&S_City&S_State&N_Zip&S_Phone
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Post_payee
  DB Op:		    Insert
*/

int PostPayee (char* Arg[], int ArgCnt, char* MsgTxt){

    int uid;
    int confirmation;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    confirmation = atoi(Arg[6]) + uid + RandomTime - RandomLoad;
    //ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%d\n", confirmation);
    return 0;

}


		 
/*
  Banking Command: 7 - QuickPay
  Request Params:  N_UserID&N_PayeeId&N_Date&D_Amount
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Quick_pay
  DB Op:		    Insert
*/

int QuickPay (char* Arg[], int ArgCnt, char* MsgTxt){

    int uid;
    int confirmation;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    confirmation = atoi(Arg[3]) + uid + RandomTime - RandomLoad;

    //ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%d\n", confirmation);

    return 0;


}



/*
  Banking Command: 8 - ReviewBillPay
  Request Params:  N_UserID&N_StartDate&N_EndDate
  Response:		    N_NumberOfPayees Lines: N_PayeeId&N_Date&D_Amount
  Script Mapping:  Bill_pay_status_output
  DB Op:		    Select
*/

int ReviewBillPay (char* Arg[], int ArgCnt, char* MsgTxt){
    
    int i;
    int uid;
    char *ptr;
    int bills;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    bills = uid%10 + 1;
    //ptr +=  sprintf(ptr,OK_STATUS);
    for (i=0; i < bills; i++) {
      ptr += sprintf(ptr, "%s&%s&%s\n",PayeesId[i], Arg[1], Payments[i]);
    }
    return 0;
}


 
/*
  Banking Command: 9 - Profile
  Request Params:  N_UserID
  Response:		    S_Address&S_Email&S_Phone
  Script Mapping:  Profile
  DB Op:		    Select
*/
 
int Profile (char* Arg[], int ArgCnt, char* MsgTxt){

    int i;
    int uid;
    int streetnum;
    char *ptr;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    i = uid%10;
    streetnum = (100+uid)% 1000;

    //ptr +=  sprintf(ptr,OK_STATUS);
    sprintf (ptr, "%d %s&%s&%s&%s&%s@isp.net&%s\n",
        streetnum, Street[i], City[i], State[i], Zip[i], Arg[0], Phone[i]);
    
    return 0;

}


		 
/*
  Banking Command: 10 - ChangeProfile
  Request Params:  N_UserID& S_Address&S_Email&S_Phone
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Change_profile
  DB Op:		    Update
*/
 
int ChangeProfile (char* Arg[], int ArgCnt, char* MsgTxt){
 
    int uid;
    int confirmation;
    char *ptr;
    strncpy(MsgTxt, DOBANKING, sizeof(DOBANKING));

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    confirmation = uid  + RandomTime - RandomLoad; 
    
    //ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%d\n", confirmation);
    return 0;

}


		 
/*
  Banking Command: 11 - PlaceChkOrder
  Request Params:  N_UserID&N_Acct#&N_Date&D_Price
  Response:		    S_Status (DONE)
                            N_Confirmation
  Script Mapping:  Place_check_order
  DB Op:		    Select,Update,Insert
*/
 
int PlaceChkOrder (char* Arg[], int ArgCnt, char* MsgTxt){
    int uid;
    char *ptr;
    int acct;
    unsigned int confirmation;

    strncpy(MsgTxt, DOBANKING, sizeof(DOBANKING));

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    acct = atoi(Arg[1]);

    confirmation = acct + uid + RandomTime - RandomLoad;

    //ptr +=  sprintf(ptr,OK_STATUS);
    sprintf(ptr, "%u\n",confirmation);
    return 0;

}



/*
  Banking Command: 12  - PostTransfer
  Request Params:  N_UserID&N_Acct1&D_Amount&N_Acct2,N_Date
  Response:		    N_NumberOfAccts Linesi(2): N_Acct_# &D_Balance
  Script Mapping:  Post_transfer
  DB Op:		    Select,Update,Insert,Insert,Select
*/
 
int PostTransfer (char* Arg[], int ArgCnt, char* MsgTxt){
    int uid;
    char *ptr;
    double xferamt, balance1, balance2;
    int acct1, acct2;

    ptr = MsgTxt;
    uid = atoi(Arg[0]);
    acct1 = atoi(Arg[1]);
    xferamt = atof(Arg[2]);
    acct2 = atoi(Arg[3]);
    balance1 = (RandomTime%1000) + (uid%10000) - xferamt;
    balance2 = (RandomTime%1000) + xferamt;
    //ptr +=  sprintf(ptr,OK_STATUS);
    ptr += sprintf(ptr, "%010d&%-10.2f\n",acct1,balance1);
    ptr += sprintf(ptr, "%010d&%-10.2f\n",acct2,balance2);
    return 0;

}

int (*banking_cmdFp[16])(char **, int, char *) = 
	{ NULL, LoginPasswd,	AcctBalances,	AcctSummary,
	  CheckDetail,	 BillPayment,	PostPayee,	QuickPay,
	  ReviewBillPay, Profile,	ChangeProfile,	PlaceChkOrder,
	  PostTransfer,  NULL,	NULL,	NULL };