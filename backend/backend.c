#if !defined(BACKEND_C_INCLUDED)
#define BACKEND_C_INCLUDED

#include "utils/utils.c"
#include "utils/md5.c"

#define BESIM_PORT "6060"
#define BESIM_HOST "152.3.137.21"
#define OK_STATUS "0\n"
#define ERR1_STATUS "1\n"

backend_data* backend_create()
{
 backend_data* b = (backend_data*)alloc_host(sizeof(backend_data));
 b->req = (char*)alloc_pinned_host(COHORT_SIZE*BESIM_REQUEST_LEN);
 b->resp = (char*)alloc_pinned_host(COHORT_SIZE*BESIM_RESPONSE_LEN);
 b->d_req = (char*)alloc_device(COHORT_SIZE*BESIM_REQUEST_LEN);
 b->d_resp = (char*)alloc_device(COHORT_SIZE*BESIM_RESPONSE_LEN);
 b->d_transposed_req = (char*)alloc_device(COHORT_SIZE*BESIM_REQUEST_LEN);
 b->d_transposed_resp = (char*)alloc_device(COHORT_SIZE*BESIM_RESPONSE_LEN);
 return b;
}

#if defined(_USE_REMOTE_BESIM_)
int besim_open()
{
 int httpd = 0;
 struct addrinfo hints, *res;
 
 memset(&hints, 0, sizeof(hints));
 hints.ai_family = AF_UNSPEC;
 hints.ai_socktype = SOCK_STREAM;
 if(getaddrinfo(BESIM_HOST, BESIM_PORT, &hints, &res) != 0)
  error_die("getaddrinfo");
 httpd = socket(res->ai_family,res->ai_socktype,res->ai_protocol);
 if (httpd == -1)
  error_die("socket");
 if (connect(httpd, res->ai_addr,res->ai_addrlen) < 0)
  error_die("connect");
 return httpd;
}

void besim_command(int httpd, char* request, char* response)
{
 int bytes = 0;
 if (send(httpd, request, strlen(request), 0) < 0)
  error_die("send");
 if ((bytes = recv(httpd, response, BESIM_RESPONSE_LEN, 0)) < 0)
  error_die("recv");
 response[bytes] = '\0';
}

void besim_close(int httpd)
{
 close(httpd);
}

void backend_process(backend_data* b)
{
 int i;
 for(i=0; i<COHORT_SIZE; i++) {
  /* Apache currently closes a connection after 100 requests. So for now, open/close a connection for each request */
  int httpd = besim_open();
  besim_command(httpd, (b->req + i*BESIM_REQUEST_LEN), 
                (b->resp + i*BESIM_RESPONSE_LEN));
  besim_close(httpd);
 }
}

DEVICE char* process_backend_response_transposed(char* resp, int* resp_len)
{
 char* p1, *p2;
 p1 = d_strstr_transposed(resp, "<pre>");
 assert(p1 != NULL);
 p2 = d_strstr_transposed(p1, "</pre>");
/* if(!strncmp(p1 + 6, OK_STATUS, 2)) {
  ret = 0;
 }*/
 if(resp_len)
  *resp_len = (int)(p2 - p1) - 9;
 *(p2 - TRANSPOSE(1)) = '\0';
 return (p1 + TRANSPOSE(8));
}
#else
#include "backend/besim_banking.c"

void _parse_besim_command(char* buf, char* query_string)
{
 char* line = buf;
 char uri[512];
 char* tmp;
 int i = 0, j = 0;
 
  /* get method */
 while(!d_isspace(line[j])) {
  i++; j++;
 }
 /* get uri */
 i = 0;
 while(d_isspace(line[j]))
  j++;
 while(!d_isspace(line[j])) {
  uri[i] = line[j];
  i++; j++;
 }
 uri[i] = '\0';
 /* check for query string */
 tmp = uri;
 while((*tmp != '?') && (*tmp != '\0'))
  tmp++;
 if(*tmp == '?') {
  *tmp = '\0';
  tmp++;
  d_strcpy(query_string, tmp);
 }
}

void besim_command(char* request, char* response)
{
 int CommandType;
 char *pStr;
 char query_string[1024];
 char *Arg[100];
 int ArgCnt;

 _parse_besim_command(request, query_string);
 /* Place each ampersand separated argument into the Arg list */
 Arg[0] = query_string;
 for (ArgCnt = 0, pStr = query_string; *pStr != '\0'; pStr++) {
  if ('&' == *pStr) {
   *pStr = '\0';
   ArgCnt++;
   Arg[ArgCnt] = pStr + 1;
  } else if (',' == *pStr) {
   *pStr = '\0';
   ArgCnt++;
   Arg[ArgCnt] = pStr + 1;
  }
 }
 CommandType = atoi(Arg[1]);
 (*banking_cmdFp[CommandType])(&Arg[2], (ArgCnt-1), response);
}

request_handler* request_handler_get_reference(int id);

void* backend_thread_worker_1(void* data)
{
 int j, id;
 nvtxNameOsThread(pthread_self(), "Backend thread 1");
 nvtxEventAttributes_t eventAttrib = {0};
 eventAttrib.version = NVTX_VERSION;
 eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
 eventAttrib.colorType = NVTX_COLOR_ARGB;
 eventAttrib.color = 0xFF00FF00;
 eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;
 eventAttrib.message.ascii = "Backend response ready";
 while(1) {
  while(read(pipefds_server2backend1[0], &id, 4) > 0) {
   request_handler* r = request_handler_get_reference(id);
   //long int start_time = profiler_get_time();
   for(j=0; j<COHORT_SIZE; j++) {
    besim_command((r->be->req + j*BESIM_REQUEST_LEN), 
                (r->be->resp + j*BESIM_RESPONSE_LEN));
   }
   nvtxMarkEx(&eventAttrib);
   if(write(pipefds_backend2server[1], &id, 4) != 4) {
    error_die("write");
   }
   //long int end_time = profiler_get_time();
   //fprintf(stderr, "\nBackend time = %ld", (long int)(end_time - start_time));
  }
 }
 pthread_exit(NULL);
 return NULL;
}

void* backend_thread_worker_2(void* data)
{
 int j, id;
 nvtxNameOsThread(pthread_self(), "Backend thread 2");
 nvtxEventAttributes_t eventAttrib = {0};
 eventAttrib.version = NVTX_VERSION;
 eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
 eventAttrib.colorType = NVTX_COLOR_ARGB;
 eventAttrib.color = 0xFF00FF00;
 eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;
 eventAttrib.message.ascii = "Backend response ready";
 while(1) {
  while(read(pipefds_server2backend2[0], &id, 4) > 0) {
   request_handler* r = request_handler_get_reference(id);
   for(j=0; j<COHORT_SIZE; j++) {
    besim_command((r->be->req + j*BESIM_REQUEST_LEN), 
                (r->be->resp + j*BESIM_RESPONSE_LEN));
   }
   nvtxMarkEx(&eventAttrib);
   if(write(pipefds_backend2server[1], &id, 4) != 4) {
    error_die("write");
   }
  }
 }
 pthread_exit(NULL);
 return NULL;
}

void backend_thread_init()
{
 pthread_t t;
 pthread_create(&t, NULL, &backend_thread_worker_1, NULL);
 pthread_create(&t, NULL, &backend_thread_worker_2, NULL);
}

void backend_process(request_handler* r, void (*cb)(void*))
{
#if defined(_THREADED_BACKEND_)
 static int backend_chooser = 1;
 int fd;
 r->cb = cb;
 switch(backend_chooser) {
  case 1:
   fd = pipefds_server2backend1[1];
   backend_chooser = 2;
   break;
  case 2:
   fd = pipefds_server2backend2[1];
   backend_chooser = 1;
   break;
 }
 if(write(fd, &r->id, 4) != 4) {
  error_die("write");
 }
#else
 int j;
 for(j=0; j<COHORT_SIZE; j++) {
  besim_command((r->be->req + j*BESIM_REQUEST_LEN), 
                (r->be->resp + j*BESIM_RESPONSE_LEN));
 }
 cb(r);
#endif 
}

DEVICE char* process_backend_response_transposed(char* resp, int* resp_len)
{
 return resp;
}
#endif

void backend_copy_requests_to_host(backend_data* b, cudaStream_t stream)
{
 dim3 grid(COHORT_SIZE/TILE_DIM,BESIM_REQUEST_LEN/TILE_DIM), threads(TILE_DIM,BLOCK_ROWS);
 transpose<<<grid, threads, 0, stream>>>(b->d_req, b->d_transposed_req, COHORT_SIZE, BESIM_REQUEST_LEN);
 copy_to_host_async(b->req, b->d_req, COHORT_SIZE*BESIM_REQUEST_LEN, stream);
}

void backend_copy_response_to_device(backend_data* b, cudaStream_t stream)
{
 dim3 grid(BESIM_RESPONSE_LEN/TILE_DIM,COHORT_SIZE/TILE_DIM), threads(TILE_DIM,BLOCK_ROWS);
 copy_to_device_async(b->d_resp, b->resp, COHORT_SIZE*BESIM_RESPONSE_LEN, stream);
 transpose<<<grid, threads, 0, stream>>>(b->d_transposed_resp, b->d_resp, BESIM_RESPONSE_LEN, COHORT_SIZE);
}

#endif
