#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#define REQUEST_URI_NONE 0
#define REQUEST_URI_IMAGE 1000
#define MAX_VALUES 10
#define VALUE_LEN 32
#define USERID_LEN 32

#define TRANSPOSE(x) (COHORT_SIZE*x)

#define APPEND_TO_BUFFER(str, len) \
 {  \
  int j; \
  for(j=0; j<len; j++) { \
   buffer_iter[j] = str[j]; \
  } \
  buffer_iter += len; \
 }

#define APPEND_TO_BUFFER_TRANSPOSED_3(str, len) \
 {  \
  int j; \
  for(j=0; j<len; j++) { \
   buffer_iter[TRANSPOSE(j)] = str[j]; \
  } \
  buffer_iter += TRANSPOSE(len); \
 }

#define APPEND_TO_BUFFER_TRANSPOSED_2(str) \
 {  \
  char* str1 = (char*)str; \
  while(*str1 != '\0') { \
   *buffer_iter = *str1; \
   str1 += TRANSPOSE(1); \
   buffer_iter += TRANSPOSE(1); \
  } \
 }

#define APPEND_TO_BUFFER_TRANSPOSED(str) \
 {  \
  char* str1 = (char*)str; \
  while(*str1 != '\0') { \
   *buffer_iter = *str1; \
   str1++; \
   buffer_iter += TRANSPOSE(1); \
  } \
 }
 
#define GENERATE_PADDING(prefix) \
 { \
  int padding_len = 0; \
  char padding[RESPONSE_LEN]; \
  memset(padding, 0, RESPONSE_LEN); \
  char* h_padding = NULL; \
  read_file("dynamic_padding/"#prefix, padding, &padding_len); \
  h_padding = (char*)alloc_device(padding_len+1); \
  check_cuda(cudaMemcpyToSymbol(prefix##_padding, &h_padding, sizeof(char*))); \
  check_cuda(cudaMemcpy(h_padding, padding, padding_len+1, cudaMemcpyHostToDevice)); \
 }

#define APPEND_WHITESPACE_PADDING() \
 tile[threadIdx.x] = (int)((buffer_iter - response)/COHORT_SIZE); \
 tile[threadIdx.x] = max(tile[threadIdx.x], tile[warpId+(laneId^16)]); \
 tile[threadIdx.x] = max(tile[threadIdx.x], tile[warpId+(laneId^8)]); \
 tile[threadIdx.x] = max(tile[threadIdx.x], tile[warpId+(laneId^4)]); \
 tile[threadIdx.x] = max(tile[threadIdx.x], tile[warpId+(laneId^2)]); \
 tile[threadIdx.x] = max(tile[threadIdx.x], tile[warpId+(laneId^1)]); \
 diff = tile[threadIdx.x] - (int)((buffer_iter - response)/COHORT_SIZE); \
 while(diff) { \
  *buffer_iter = ' '; \
  buffer_iter += TRANSPOSE(1); \
  diff--; \
 }

#define TILE_DIM 32
#define BLOCK_ROWS 8

GLOBAL void transpose(char* odata, char* idata, int width, int height)
{
 SHARED int tile[TILE_DIM][TILE_DIM+1];
 int i;
 int xIndex = blockIdx.x*TILE_DIM + threadIdx.x;
 int yIndex = blockIdx.y*TILE_DIM + threadIdx.y;
 int index_in = xIndex + (yIndex)*width;
 xIndex = blockIdx.y*TILE_DIM + threadIdx.x;
 yIndex = blockIdx.x*TILE_DIM + threadIdx.y;
 int index_out = xIndex + (yIndex)*height;

 for(i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
  tile[threadIdx.y+i][threadIdx.x] = idata[index_in+i*width];
 }
 __syncthreads();
 for(i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
  odata[index_out+i*height] = tile[threadIdx.x][threadIdx.y+i];
 }
}

typedef struct {
 int tm_sec;    /* seconds after the minute (0 to 61) */
 int tm_min;    /* minutes after the hour (0 to 59) */
 int tm_hour;   /* hours since midnight (0 to 23) */
 int tm_mday;   /* day of the month (1 to 31) */
 int tm_mon;    /* months since January (0 to 11) */
 int tm_year;   /* years since 1900 */
 int tm_wday;   /* days since Sunday (0 to 6 Sunday=0) */
 int tm_yday;   /* days since January 1 (0 to 365) */
 int tm_isdst;  /* Daylight Savings Time */
} d_tm;

typedef struct {
 int uri;
 char query_string[QUERY_STRING_LEN];
 long int read_time; /* Request was read from the network */
 long int parser_start_time;
 long int dispatch_start_time;
 long int be_start1_time;
 long int be_finish1_time;
 long int be_start2_time;
 long int be_finish2_time;
 long int response_time;
} profiler_data;

typedef enum {
 REQUEST_TYPE_NONE,
 REQUEST_TYPE_GET,
 REQUEST_TYPE_POST
} request_type;

typedef enum {
 PARSER_READY,
 PARSER_BUSY
} parser_state_t;

typedef enum {
 READER_READY,
 READER_FULL
} reader_state_t;

typedef struct {
 request_type type;
 int uri;
 int image_index;
 int client; /* file descriptor of the client */
 int session_id; /* HTTP Header */
 int content_length; /* HTTP Header */
 int connection_close; /* HTTP Header */
 char query_string[QUERY_STRING_LEN];
} request;

typedef struct {
 request_type type;
 int uri;
 int image_index;
 int client; /* file descriptor of the client */
 int session_id; /* HTTP Header */
 int content_length; /* HTTP Header */
 int connection_close; /* HTTP Header */
} request_image;

typedef struct {
 cudaStream_t stream;
 int type;
 char** resp_images;
 char* resp;
 int* resp_len;
 char* headers;
 int* headers_len;
 int* fds;
 int n_resp;
 int* d_fds;
 char* d_resp;
 char* d_transposed_resp;
 int* d_resp_len;
 char* d_headers;
 char* d_transposed_headers;
 int* d_headers_len;
 unsigned int* d_session_id;
 unsigned int* d_new_session;
 int id;
// profiler_data* profile;
} response_data;

typedef enum {
 COHORT_FREE = 0,
 COHORT_PARTIALLY_FULL,
 COHORT_READY,
 COHORT_BEING_PROCESSED, /* Intermediary state to avoid cohort being dispatched again */
 COHORT_RECYCLE
}_cohort_state_t;

typedef struct {
 request buffer[COHORT_SIZE];
} cohort;

typedef struct {
 request_image buffer[COHORT_SIZE];
} cohort_image;

typedef struct {
 int uri; /* request type */
 unsigned int count; /* number of requests held */
 unsigned int state;
} cohort_state;

typedef struct {
 cohort_state state[COHORTS_NUM];
 cohort_state state_image[COHORTS_IMAGES_NUM];
} cohort_data;

typedef enum {
 SESSION_NODE_FREE = 0,
 SESSION_NODE_USED
} session_state_enum;

typedef struct sess_node {
 int state;
 char userid[USERID_LEN];
}session_node;

typedef struct {
 session_node nodes[MAX_SESSIONS_PER_BUCKET];
} session_bucket;

typedef struct {
 char req_buf[COHORT_SIZE*REQ_BUF_SIZE];
 char transposed_req_buf[COHORT_SIZE*REQ_BUF_SIZE];
 int fds[COHORT_SIZE];
 int bytes_read[COHORT_SIZE];
} request_data;

typedef struct {
 request_data reqs;
 int n_clients; /* Number of clients currently held */
 int state;
} reader_data;

typedef struct {
 cudaStream_t stream;
 int state;
 /* Device */
 request_data* d_reqs;
 reader_data* reader;
} parser_data;

typedef struct {
 /* Host */
 char* req;
 char* resp;
 /* Device */
 char* d_req;
 char* d_resp;
 char* d_transposed_req;
 char* d_transposed_resp;
} backend_data;

typedef struct {
 cudaStream_t stream;
 int id;
 int uri;
 int* req_state;
 int* d_req_state;
 backend_data* be; /* Backend state */
 response_data* resp; /* Response data */
 int cohort_id;
 session_bucket* d_session_array;
 void (*cb)(void*);
} request_handler;

inline cudaError_t check_cuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
 if (result != cudaSuccess) {
  fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
  assert(result == cudaSuccess);
 }
#endif
 return result;
}

#endif
