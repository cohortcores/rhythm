\section{\rhythm Server Software Architecture}
\label{sec:rhythm}
\rhythm is a software architecture that extends event-based staged
servers and cohort scheduling~\cite{SEDA,CohortScheduling,nginx}
to support high throughput
servers on emerging highly threaded data parallel accelerators.
Conceptually, \rhythm pipelines the processing of request \emph{cohorts}---a set
of requests that require similar computation.  In this section we
first provide an overview of the \rhythm design, including our core
design goals.  This is followed by a more detailed description of the
\rhythm pipeline and the requisite data structures. \rhythm is a general
architecture that could be implemented in many ways;
Section~\ref{sec:implementation} describes a specific implementation.
Furthermore, \rhythm can be used to implement a variety of services,
but in this work we focus on its use for web servers.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.4]{figs/serverflow.pdf}
  \caption{Typical Web Server Request Processing Overview}
  \label{fig:serverflow}
\end{figure}

\subsection{Overview}

Figure~\ref{fig:serverflow} shows the typical pattern for processing
requests in web servers. Requests arrive over the network and are
dispatched for processing, generally based on the type of request
(e.g., a particular PHP file or query type). Request processing may be
followed by access to a backend database (e.g., SQL or memcached), and
the results from that query can lead to further processing before a
response is sent back to the client.  Conventional servers process
requests individually, using a thread per
request~\cite{apache2} or a staged event-based server~\cite{SEDA,CohortScheduling,nginx}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.7\linewidth]{figs/pipeline.pdf}
  \caption{\rhythm Pipeline (Each stage can have multiple instances)}
  \label{fig:pipeline}
\end{figure}
\rhythm extends event-based staged servers by providing a pipelined
architecture for processing cohorts of requests on data parallel 
hardware. The \rhythm pipeline
is composed of five stages, as shown in Figure~\ref{fig:pipeline}:
1) Reader, 2) Parser, 3) Dispatch, 4) Process, 5) Response.  For each
of these stages there may be one or more instances, allowing for
parallelism across and within stages.  For a given service, the process stage is composed
of $n$ backend stages and $n+1$ process stages.  A specific
implementation of the \rhythm pipeline can map each of these stages to
either a general purpose CPU or an SIMT accelerator.  We defer
discussion of a specific implementation to
Section~\ref{sec:implementation}.

The overall goal of \rhythm is to enable high throughput server
implementations that can exploit the efficiencies of GPU-style
accelerators.  To achieve this, our design goals for \rhythm include:
1) Asynchronous, 2) Event driven, 3) Lock free \& wait free, and 4)
Utilize the most efficient computational resource (general purpose core or 
accelerator). The first three guidelines are well-known for high
throughput server design on commodity general purpose processors
(e.g., nginx~\cite{nginx}).  The last design guideline is unique to an
accelerator-based design and reflects our desire to exploit the
efficiency of the accelerator as much as possible, but some requests that 
do not conform to a data parallel model may be executed more efficiently 
on a general purpose CPU.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{figs/eventserver.pdf}
  \caption{\rhythm Event-based Server: Each Finite State Machine (FSM)
  is associated with a cohort of requests.}
  \label{fig:eventserver}
\end{figure}

The \rhythm pipeline is controlled by an event-based
server with a single thread (see Figure~\ref{fig:eventserver}) that
provides cohort scheduling~\cite{CohortScheduling}.  The
server thread is generally responsible for delaying requests an appropriate
amount of time to form cohorts, determining when a cohort is ready to
launch on the accelerator, launching cohorts onto the accelerator,
managing cohort context transitions, and sending responses back
to the originating clients.

Requests can be delayed for a limited amount of time and still achieve
acceptable response times~\cite{Meisner:2012:DAS}.  \rhythm includes a
timeout so that requests are not delayed indefinitely during cohort
formation. Setting a specific timeout value is a policy decision that
depends on particular service level agreements, \rhythm simply
provides the mechanism. A similar timeout mechanism could be used to
ensure that stragglers (e.g., long backend accesses) do not delay other
requests in a cohort during execution. Furthermore, since cohort
formation can occur after each stage, the set of requests in a cohort
may change. Straggler responses from the backend can either be executed
on the host CPU or added to a subsequent cohort.

The \rhythm pipeline stalls only when insufficient resources are
available (i.e., structural hazards such as memory buffers or
device execution units).  The single threaded control avoids thread 
switching overheads, is lock-free, wait-free 
and fully asynchronous. The design also allows for multiple instances
of each stage (reader, parser, process and response) and each of the 
stages can be tuned for optimal concurrency. \rhythm maintains 
state that allows it to efficiently schedule cohorts on the 
host or the accelerator based on the current state of the system.

\paragraph{Cohort Management}
A cohort context contains information necessary to identify the
specific request type and other properties of a cohort. \rhythm uses a
cohort pool to track the availability of cohort contexts and manages
their allocation throughout the pipeline. A cohort context can be \emph{Free},
\emph{Partially\_Full}, \emph{Full} or \emph{Busy}. A \emph{Free}
context can be used to form a new cohort. Requests are added to a
context by either the Reader or the Parser. The first request
added to a \emph{Free} cohort context transitions the context to
\emph{Partially\_Full} where it can continue to accumulate requests
until it becomes \emph{Full}. When a cohort context begins execution,
either because it was \emph{Full} or a timeout occurred, it becomes
\emph{Busy}. A cohort context is \emph{Busy} while the requests
transition through the various process stages. A cohort context is
\emph{Free}d after the responses are sent to their respective clients.

%The cohort state identifies the state of the cohort in the pipeline. 
%The cohort state is used by the event server to track the flow of 
%cohorts in the pipeline, and ensure forward progress.
%A cohort can be either in the Reader, Parser, Dispatch, Process or 
%Response states, which define the cohort's FSM.

\subsection{The \rhythm Pipeline}
We now elaborate more on the design and function of the individual 
stages of the \rhythm pipeline.  An implementation would provide
resources (execution units and memory) to support one or more
instances of each stage.

\paragraph{Reader}
The reader accumulates requests from the network to form a cohort, 
based purely on the request order. When a sufficient number of requests
arrive, the reader passes the cohort to the next stage of the
pipeline---the parser. The reader latency is primarily limited by 
the request arrival rate or network bandwidth. If all parsers are busy,
the reader stalls.

\paragraph{Parser}
Request parsing follows a standard protocol defined by the HTTP 
Specification~\cite{httpspec}, making it an ideal candidate for 
SIMT execution. The parser extracts the method (GET or POST), the 
request type (PHP file or image), the content length, cookie 
information, the client file descriptors and the query string 
parameters for each request in the cohort. This information is then 
composed into a \emph{request} structure and added to the 
cohort. Cohorts are formed based on the file accessed or any other 
metric of similarity. The parser then signals dispatch if a cohort 
becomes \emph{Full}.

\paragraph{Dispatch}
Dispatch is performed on the host and determines if a request should
be executed on the host or the device.  Some requests that simply
access the file system are best processed on the host rather than
the device.  GPU access to the file system (e.g.,
GPUfs~\cite{asplos:gpufs}) would enable dispatch execution on the
device and decrease transfer overheads, but we leave exploring this
option to future work. A \emph{Full} cohort context 
is ready for dispatch if the requisite resources on the device are available. Based on the
resource requested (e.g., specific PHP file), the appropriate process
stage is executed and the cohort context is updated to \emph{Busy} to
prevent redundant dispatch.

\paragraph{Process}
The process phase is defined by the different request types supported
by the server and is generally composed
of $n$ backend stages and $n+1$ process stages. Typically, a web service
response contains the HTTP header, static HTML content, database
content, and dynamic HTML generated based on the database content. The overall
process phase alternates between content generation and backend
access.  For a typical remote backend, individual threads in a given
process stage generate request strings which are sent to the backend, and
the backend response is then passed on to the next stage of content
generation (i.e., another process stage). We note that it is easy to
incorporate a portion of the backend, such as a cache lookup, as part
of a process stage. When the response is ready,
an event is raised to signal process completion.

\paragraph{Response}
The response stage sends the responses to the respective clients and 
frees the associated cohort context. Its latency is primarily limited 
by available network bandwidth.

The \rhythm pipeline is general and could be implemented entirely on a
single machine or distributed across several machines. For example,
read and parse could be implemented on a front-end machine, processing
of specific request types on separate machines, and response formation
on yet another machine.  The next section presents an implementation
on a single machine; we leave exploring alternative implementations as
future work.

% \paragraph{Access to Operating System Services.}
% \arl{Not sure where this should go...but we need to say something somewhere}.
% \begin{figure}[htb]
%   \centering
%   \includegraphics[width=0.4\textwidth]{figs/syscalls.pdf}
%   \caption{Frequency of SPECWeb Banking Accessing Operating System Services}
%   \label{fig:syscalls}
% \end{figure}
% As mentioned above, server workloads often require access to files and
% the network.  To better understand the potential impact of accessing
% operating system services, we measured the number of instructions
% between system calls for the SPECWeb Banking workload running under
% HipHop.  We profiled 63 distinct requests that access all 16 request
% types.  Figure~\ref{fig:syscalls} shows a histogram of the number of
% instructions between system calls.  This data reveals that about 50\%
% of the time the number of instructions between system calls is less
% than 10,000 and 50\% of the time it is greater than 10,0000.  On
% average there are approximately 33,000 instructions between system
% calls.  Furthermore, we note that 97\% of the system calls are related
% to filesystem or network IO.  As a first step, our initial prototype
% system will utilize the host CPU to perform all OS services.  We
% anticipate this approach to provide functionally correct systems, but
% with relatively low performance due to the overheads of transitioning
% to/from the GPU/CPU to perform system calls.  An interesting avenue
% for future work is to integrate GPU-FS with \rhythm to enable access to
% OS services directly from the GPU.

% Our general approach is to decompose the application into two main
% components: a server and application requests.  This separation 
% enables us to develop the appropriate APIs such that the server code
% can be reused across multiple server applications.  One of our goals
% is to develop an appropriate runtime system that makes it easier to
% develop server applications that can exploit highly threaded hardware.

% Modern event-based servers (e.g., ngnix) do not coordinate among different
% requests and thus improve throughput by avoiding synchronization. In
% this spirit, \rhythm avoids having the host CPU parse and group
% requests, instead we use 