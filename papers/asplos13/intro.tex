\section{Introduction}
\label{sec:intro}
Data centers provide the computational and storage infrastructure
required to meet today's ever increasing demand for Internet content.
For example, Facebook utilizes more
than 100,000 servers to provide approximately one trillion page views
per month (about 350K per second) and 1.2 million photo views per second.
Meeting the increasing demand for content often requires adding more
machines to existing data centers and building more data centers.  Although
content distribution networks can offload some of the demand for
static content, state-of-the-art data centers house vast numbers of
servers and require 2-6 Mega Watts of power.  Therefore server
performance, scaling and energy efficiency (throughput/Watt) are
crucial factors in reducing total cost of ownership (TCO) in today's
server-based
industries~\cite{Andersen:2009:FAWN,Chase:2001:MES:502034.502045,Fan:2007:PPW:1250662.1250665,Ferdman:2012:CCS:2150976.2150982,Lotfi-Kamran:2012:SP,Meisner:2012:DAS}.

For example, at Facebook, the majority of the traffic goes through
front-end web servers and the majority of the data center servers (and
thus power) are devoted to this front-end~\cite{facebook:datacenter}. 
Reducing CPU load inspired the design of HipHop, which translates PHP 
to native code thereby increasing throughput per server by $>5\times$~\cite{facebook:hiphop}.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.4]{figs/designspace.pdf}
  \caption{Server Design Space}
  \label{fig:designspace}
\end{figure}

Current system designs based on commodity multicore processors may not
be the most power/energy efficient for all server workloads.  There is
ongoing debate over which architecture is best suited for specific
workloads~\cite{Ferdman:2012:CCS:2150976.2150982,JanapaReddi:2010:WSU:1815961.1816002,Kgil:2008:PUS:1412587.1412589,Lotfi-Kamran:2012:SP,Mudge:2010:COE}.
Figure~\ref{fig:designspace} shows an overview of the server design space
based on throughput normalized to an x86 core versus 
energy efficiency (performance/Watt) normalized to an ARM core.  An ideal design would
achieve throughput at or above an x86 core with energy efficiency at
or above an ARM core.

Recent highly-threaded accelerators (i.e., NVIDIA
Kepler~\cite{nvidia:kepler:spec}, Intel Xeon
Phi~\cite{intel:xeon:phi}) achieve $>$1 teraflop of performance
within a 225W power envelope, achieving $>$5 gigaflops/W.  This efficiency
arises from several factors, including the amortization of overheads
(e.g., instruction fetch) due to extensive use of vector (SIMD) and
multi-threaded (SIMT) hardware, and simpler designs enabled by
supporting a more restricted programming model; these accelerators excel at
executing regular data-parallel programs, but are often considered unsuitable for
arbitrary multi-threaded applications.  

Nonetheless, as GPU-like
hardware becomes increasingly general purpose, the potential
advantages of highly-threaded, yet somewhat restricted hardware, are
compelling.  Even servers based on low power cores (e.g., ARM, Atom)
could further benefit from amortizing microarchitecture power
overheads (e.g., instruction fetch) over multiple requests, or
utilizing heterogeneous System-on-Chip solutions (e.g.,
Tegra~\cite{nvidia:tegra3}, AMD Fusion~\cite{AMD:Fusion}, or many core
systems~\cite{Li:2011:SIS}) for servers.
The challenge is to determine if traditional task parallel
request-based server workloads can exploit the efficiencies of highly
threaded data-parallel hardware in future server platforms.  

This paper takes the first steps toward meeting
this challenge by building on several observations about technology
trends and server workloads. In a typical data center server environment, requests may be
distributed to a set of servers based on information within the
request (e.g., based on a hash of the user ID, URL), on the type of
service requested (e.g., login, static image, database query), or other
sharding methods.  In this scenario, many of the requests perform the same
task(s), such as login or search query.  \emph{The key insight is, given
  an incoming stream of requests, a server could delay some requests
  in order to “align” the execution of similar requests---allowing them
  to execute concurrently.}  Our goal is to trade an increase
in response time for improvement in server throughput per Watt by exploiting similarity across requests
using cohort scheduling~\cite{CohortScheduling} to launch data
parallel executions. 

The contributions of this work include:
\begin{itemize}
\item{\emph{Rhythm}, a software architecture for high throughput SIMT-based
    servers. (\rhythm derives from the Greek word \emph{rhythmos}, meaning 
    any regular recurring motion or symmetry)}
\item{A prototype implementation of \rhythm and the SPECWeb Banking service on NVIDIA GPUs that
    demonstrates it is possible to run server workloads on a GPU.\footnote{Our initial prototype targets GPUs, but our
  ideas are applicable to a broad class of accelerators.  Without loss
  of generality, we use GPU or device to refer to this broad class.} 
}
\item{Evaluation of future server platform architectures.
Our prototype achieves 1.5M requests/sec on an NVIDIA GTX Titan 
GPU card for an idealized environment that removes network and storage I/O limitations.
This is 4$\times$ the throughput of a Core i7 running 8 threads at efficiencies
comparable to a dual core ARM Cortex A9. Furthermore,
approximately 192 1.2GHz, 1W ARM cores are required to achieve the same
throughput with less than 40 Watts (21\% overall) in available power to
support the massively scaled system. We also demonstrate that array transpose
offload can increase \rhythm throughput to over 3M reqs/sec.
}
\item{Standalone C and C+CUDA implementations of the SPECWeb2009
    Banking workload that we plan to release
    publicly.}
\end{itemize}

The remainder of this paper is organized as follows.
Section~\ref{sec:motivation} examines technology trends and workload
characteristics that motivate our work.  We present our overall
server architecture (\emph{Rhythm}) in Section~\ref{sec:rhythm} and provide
implementation details in
Section~\ref{sec:implementation}. Sections~\ref{sec:methodology} and
~\ref{sec:eval} describe our methodology and evaluates our prototype's
potential performance on future server platforms.  Related
work is discussed in Section~\ref{sec:related}, while
Section~\ref{sec:conclusion} concludes and discusses future work.

% Preliminary
% evaluations (see Sec~\ref{sec:apps}) of a PHP dynamic content web
% workload (SPECWeb2009~\cite{SPECweb} with
% Apache~\cite{Apache,ApacheProject}) shows that significant similarity
% among requests exists.  Our results show that cohort execution on
% idealized SIMD hardware of different instances of the same request
% type can achieve nearly linear speedup over sequential execution.
% While cohort execution across all 14 request types (61 total PHP
% requests) may provide a speedup up to $12\times$ over sequential
% execution, with near linear speedup for a subset of 5 request types
% ($18\times$ for 20 individual requests).



