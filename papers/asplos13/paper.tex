\newcommand{\rhythm}{{\em{Rhythm}}~}

\documentclass[10pt]{sigplanconf}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[caption=false]{subfig}
\usepackage{multirow}
\usepackage{hyperref}

\begin{document}

\special{papersize=8.5in,11in}
\setlength{\pdfpageheight}{\paperheight}
\setlength{\pdfpagewidth}{\paperwidth}

\conferenceinfo{ASPLOS~'14}{March 1--4, 2014, Salt Lake City, Utah, USA} 
\copyrightyear{2014} 
\copyrightdata{978-1-4503-2305-5/14/03} 
\doi{2541940.2541956}

\title{Rhythm: Harnessing Data Parallel Hardware for Server Workloads}

\authorinfo{Sandeep R Agrawal}
           {Duke University}
           {sandeep@cs.duke.edu}
\authorinfo{Valentin Pistol}
           {Duke University}
           {pistol@cs.duke.edu}
\authorinfo{Jun Pang}
           {Duke University}
           {pangjun@cs.duke.edu}
\authorinfo{John Tran}
           {NVIDIA}
           {johntran@nvidia.com}
\authorinfo{David Tarjan\titlenote{Now with Samsung Electronics, d.tarjan@samsung.com}}
           {NVIDIA}
           {}
\authorinfo{Alvin R Lebeck}
           {Duke University}
           {alvy@cs.duke.edu}

\maketitle

\begin{abstract}
Trends in increasing web traffic demand an increase in server throughput 
while preserving energy efficiency and total cost of ownership. Present 
work in optimizing data center efficiency primarily focuses on the data 
center as a whole, using off-the-shelf hardware for individual servers. 
Server capacity is typically increased by adding more machines, which is cheap, 
though inefficient in the long run in terms of energy and area.

Our work builds on the observation that server workload execution
patterns are not completely unique across multiple requests. We
present a framework---called Rhythm---for high throughput
servers that can exploit similarity across requests to improve 
server performance and power/energy efficiency by
launching data parallel executions for request cohorts.
An implementation of the SPECWeb Banking workload using Rhythm on
NVIDIA GPUs provides a basis for evaluating both software and hardware
for future cohort-based servers. Our evaluation of 
Rhythm on future server platforms shows that it achieves
4$\times$ the throughput (reqs/sec) of a core i7 at 
efficiencies (reqs/Joule) comparable to a dual core ARM Cortex A9.
A Rhythm implementation that generates transposed responses 
achieves 8$\times$ the i7 throughput while processing 2.5$\times$ 
more requests/Joule compared to the A9.
\end{abstract}

\category{C.5.5}{Computer System Implementation}{Servers}

\keywords
high throughput; power efficiency; execution similarity

\input{intro}
\input{motivation}
\input{overview}
\input{implementation}
\input{methodology}
\input{evaluation}
\input{related}
\input{conclusion}

\acks
We would like to thank the Duke Computer Architecture Group, Mark Silberstein and 
Emmett Witchel for their support, and the reviewers for their valuable time and 
feedback. This work was supported in part by the National Science Foundation (CCF-1335443) and NVIDIA.

\bibliographystyle{abbrvnat}

\begin{thebibliography}{10}
\softraggedright

\bibitem[iee()]{ieee802.3}
http://www.ieee802.org/3/.

\bibitem[mem()]{memorycube}
Hybrid memory cube.
\newblock \url{http://hybridmemorycube.org}.

\bibitem[pci()]{pciesig}
Pci express 4.0 faq.
\newblock \url{http://www.pcisig.com/news_room/faqs/FAQ_PCI_Express_4.0/}.

\bibitem[Andersen et~al.(2009)Andersen, Franklin, Kaminsky, Phanishayee, Tan,
  and Vasudevan]{Andersen:2009:FAWN}
D.~G. Andersen, J.~Franklin, M.~Kaminsky, A.~Phanishayee, L.~Tan, and
  V.~Vasudevan.
\newblock Fawn: a fast array of wimpy nodes.
\newblock In \emph{Proceedings of the ACM SIGOPS 22nd symposium on Operating
  systems principles}, SOSP '09, pages 1--14, New York, NY, USA, 2009. ACM.
\newblock ISBN 978-1-60558-752-3.
\newblock \doi{10.1145/1629575.1629577}.
\newblock URL \url{http://doi.acm.org/10.1145/1629575.1629577}.

\bibitem[Atta et~al.(2013)Atta, T\"{o}z\"{u}n, Tong, Ailamaki, and
  Moshovos]{Atta:2013:SBI:2485922.2485946}
I.~Atta, P.~T\"{o}z\"{u}n, X.~Tong, A.~Ailamaki, and A.~Moshovos.
\newblock Strex: Boosting instruction cache reuse in oltp workloads through
  stratified transaction execution.
\newblock In \emph{Proceedings of the 40th Annual International Symposium on
  Computer Architecture}, ISCA '13, pages 273--284, New York, NY, USA, 2013.
  ACM.
\newblock ISBN 978-1-4503-2079-5.
\newblock \doi{10.1145/2485922.2485946}.
\newblock URL \url{http://doi.acm.org/10.1145/2485922.2485946}.

\bibitem[Bakkum and Skadron(2010)]{Bakkum:2010:ASD}
P.~Bakkum and K.~Skadron.
\newblock Accelerating sql database operations on a gpu with cuda.
\newblock In \emph{Proceedings of the 3rd Workshop on General-Purpose
  Computation on Graphics Processing Units}, GPGPU '10, pages 94--103, New
  York, NY, USA, 2010. ACM.
\newblock ISBN 978-1-60558-935-0.
\newblock \doi{10.1145/1735688.1735706}.
\newblock URL \url{http://doi.acm.org/10.1145/1735688.1735706}.

\bibitem[Biswas et~al.(2009)Biswas, Franklin, Savage, Dixon, Sherwood, and
  Chong]{Biswas:ISCA:2009}
S.~Biswas, D.~Franklin, A.~Savage, R.~Dixon, T.~Sherwood, and F.~T. Chong.
\newblock {Multi-Execution: Multicore Caching for Data-Similar Executions}.
\newblock In \emph{{Proceedings of the 36th annual international symposium on
  Computer architecture}}, ISCA '09, pages 164--173, New York, NY, USA, 2009.
  ACM.
\newblock ISBN 978-1-60558-526-0.
\newblock \doi{10.1145/1555754.1555777}.

\bibitem[Branover et~al.(2012)Branover, Foley, and Steinman]{AMD:Fusion}
A.~Branover, D.~Foley, and M.~Steinman.
\newblock Amd fusion apu: Llano.
\newblock \emph{Micro, IEEE}, 32\penalty0 (2):\penalty0 28 --37, march-april
  2012.
\newblock ISSN 0272-1732.
\newblock \doi{10.1109/MM.2012.2}.

\bibitem[Buck(2007)]{BuckCUDA}
I.~Buck.
\newblock {GPU computing with NVIDIA CUDA}.
\newblock In \emph{{SIGGRAPH '07: ACM SIGGRAPH 2007 courses}}, page~6, New
  York, NY, USA, 2007. ACM.
\newblock \doi{10.1145/1281500.1281647}.

\bibitem[Chalamalasetti et~al.(2013)Chalamalasetti, Lim, Wright, AuYoung,
  Ranganathan, and Margala]{Chalamalasetti:2013:FMA:2435264.2435306}
S.~R. Chalamalasetti, K.~Lim, M.~Wright, A.~AuYoung, P.~Ranganathan, and
  M.~Margala.
\newblock An fpga memcached appliance.
\newblock In \emph{Proceedings of the ACM/SIGDA international symposium on
  Field programmable gate arrays}, FPGA '13, pages 245--254, New York, NY, USA,
  2013. ACM.
\newblock ISBN 978-1-4503-1887-7.
\newblock \doi{10.1145/2435264.2435306}.
\newblock URL \url{http://doi.acm.org/10.1145/2435264.2435306}.

\bibitem[Chase et~al.(2001)Chase, Anderson, Thakar, Vahdat, and
  Doyle]{Chase:2001:MES:502034.502045}
J.~S. Chase, D.~C. Anderson, P.~N. Thakar, A.~M. Vahdat, and R.~P. Doyle.
\newblock Managing energy and server resources in hosting centers.
\newblock In \emph{Proceedings of the eighteenth ACM symposium on Operating
  systems principles}, SOSP '01, pages 103--116, New York, NY, USA, 2001. ACM.
\newblock ISBN 1-58113-389-8.
\newblock \doi{10.1145/502034.502045}.
\newblock URL \url{http://doi.acm.org/10.1145/502034.502045}.

\bibitem[Che et~al.(2009)Che, Boyer, Meng, Tarjan, Sheaffer, Lee, and
  Skadron]{Che:IISWC:2009}
S.~Che, M.~Boyer, J.~Meng, D.~Tarjan, J.~Sheaffer, S.~Lee, and K.~Skadron.
\newblock {Rodinia: A Benchmark Suite for Heterogeneous Computing}.
\newblock In \emph{{Proceedings of the IEEE International Symposium on Workload
  Characterization}}, IISWC, pages 44--54. IEEE, 2009.
\newblock \doi{10.1109/IISWC.2009.5306797}.

\bibitem[Dean and Barroso(2013)]{Dean:2013:TS:2408776.2408794}
J.~Dean and L.~A. Barroso.
\newblock The tail at scale.
\newblock \emph{Commun. ACM}, 56\penalty0 (2):\penalty0 74--80, Feb. 2013.
\newblock ISSN 0001-0782.
\newblock \doi{10.1145/2408776.2408794}.
\newblock URL \url{http://doi.acm.org/10.1145/2408776.2408794}.

\bibitem[Dietz and Young(2010)]{Dietz:2009:MIG}
H.~G. Dietz and B.~D. Young.
\newblock Mimd interpretation on a gpu.
\newblock In \emph{Proceedings of the 22nd international conference on
  Languages and Compilers for Parallel Computing}, LCPC'09, pages 65--79,
  Berlin, Heidelberg, 2010. Springer-Verlag.
\newblock ISBN 3-642-13373-8, 978-3-642-13373-2.
\newblock \doi{10.1007/978-3-642-13374-9_5}.
\newblock URL \url{http://dx.doi.org/10.1007/978-3-642-13374-9_5}.

\bibitem[Facebook()]{facebook:hiphop}
Facebook.
\newblock Hiphop-php.
\newblock \url{https://github.com/facebook/hiphop-php/wiki}.

\bibitem[Fan et~al.(2007)Fan, Weber, and Barroso]{Fan:2007:PPW:1250662.1250665}
X.~Fan, W.-D. Weber, and L.~A. Barroso.
\newblock Power provisioning for a warehouse-sized computer.
\newblock In \emph{Proceedings of the 34th annual international symposium on
  Computer architecture}, ISCA '07, pages 13--23, New York, NY, USA, 2007. ACM.
\newblock ISBN 978-1-59593-706-3.
\newblock \doi{10.1145/1250662.1250665}.
\newblock URL \url{http://doi.acm.org/10.1145/1250662.1250665}.

\bibitem[Ferdman et~al.(2012)Ferdman, Adileh, Kocberber, Volos, Alisafaee,
  Jevdjic, Kaynak, Popescu, Ailamaki, and
  Falsafi]{Ferdman:2012:CCS:2150976.2150982}
M.~Ferdman, A.~Adileh, O.~Kocberber, S.~Volos, M.~Alisafaee, D.~Jevdjic,
  C.~Kaynak, A.~D. Popescu, A.~Ailamaki, and B.~Falsafi.
\newblock Clearing the clouds: a study of emerging scale-out workloads on
  modern hardware.
\newblock In \emph{Proceedings of the seventeenth international conference on
  Architectural Support for Programming Languages and Operating Systems},
  ASPLOS XVII, pages 37--48, New York, NY, USA, 2012. ACM.
\newblock ISBN 978-1-4503-0759-8.
\newblock \doi{10.1145/2150976.2150982}.
\newblock URL \url{http://doi.acm.org/10.1145/2150976.2150982}.

\bibitem[Fielding and Kaiser(1997)]{apache2}
R.~Fielding and G.~Kaiser.
\newblock {{The Apache HTTP Server Project}}.
\newblock \emph{IEEE Internet Computing}, 1\penalty0 (4):\penalty0 88--90,
  1997.
\newblock ISSN 1089-7801.
\newblock \doi{http://doi.ieeecomputersociety.org/10.1109/4236.612229}.

\bibitem[Fitzpatrick(2004)]{Fitzpatrick:memcached}
B.~Fitzpatrick.
\newblock Distributed caching with memcached.
\newblock \emph{Linux J.}, 2004\penalty0 (124):\penalty0 5--, Aug. 2004.
\newblock ISSN 1075-3583.
\newblock URL \url{http://dl.acm.org/citation.cfm?id=1012889.1012894}.

\bibitem[Force()]{httpspec}
T.~I. E.~T. Force.
\newblock Hypertext transfer protocol -- http/1.1.
\newblock \url{http://www.ietf.org/rfc/rfc2616.txt}.

\bibitem[Fung and Aamodt(2011)]{Fung:HPCA:2011}
W.~Fung and T.~Aamodt.
\newblock {Thread Block Compaction for Efficient SIMT Control Flow}.
\newblock In \emph{{17th IEEE International Symposium on High-Performance
  Computer Architecture}}, HPCA-17, 2011.

\bibitem[Fung et~al.(2007)Fung, Sham, Yuan, and Aamodt]{Fung:MICRO:2007}
W.~W.~L. Fung, I.~Sham, G.~Yuan, and T.~M. Aamodt.
\newblock {Dynamic Warp Formation and Scheduling for Efficient GPU Control
  Flow}.
\newblock In \emph{{Proceedings of the 40th Annual IEEE/ACM International
  Symposium on Microarchitecture}}, MICRO 40, pages 407--420, Washington, DC,
  USA, 2007. IEEE Computer Society.
\newblock ISBN 0-7695-3047-8.
\newblock \doi{10.1109/MICRO.2007.12}.

\bibitem[Gonz\'{a}lez et~al.(2008)Gonz\'{a}lez, Cai, Chaparro, Magklis, Rakvic,
  and Gonz\'{a}lez]{Gonzalez:ISLPED:2008}
J.~Gonz\'{a}lez, Q.~Cai, P.~Chaparro, G.~Magklis, R.~Rakvic, and
  A.~Gonz\'{a}lez.
\newblock {Thread Fusion}.
\newblock In \emph{Proceeding of the 13th international symposium on Low power
  electronics and design}, ISLPED '08, pages 363--368, New York, NY, USA, 2008.
  ACM.
\newblock ISBN 978-1-60558-109-5.
\newblock \doi{10.1145/1393921.1394018}.

\bibitem[Govindaraju et~al.(2004)Govindaraju, Lloyd, Wang, Lin, and
  Manocha]{Govindaraju:2004:FCD}
N.~K. Govindaraju, B.~Lloyd, W.~Wang, M.~Lin, and D.~Manocha.
\newblock Fast computation of database operations using graphics processors.
\newblock In \emph{Proceedings of the 2004 ACM SIGMOD international conference
  on Management of data}, SIGMOD '04, pages 215--226, New York, NY, USA, 2004.
  ACM.
\newblock ISBN 1-58113-859-8.
\newblock \doi{10.1145/1007568.1007594}.
\newblock URL \url{http://doi.acm.org/10.1145/1007568.1007594}.

\bibitem[Hachman()]{facebook:datacenter}
M.~Hachman.
\newblock How facebook will power graph search.
\newblock
  \url{http://slashdot.org/topic/datacenter/how-facebook-will-power-graph-sear%
ch/}.

\bibitem[Han et~al.(2010)Han, Jang, Park, and
  Moon]{Han:2010:PGS:1851182.1851207}
S.~Han, K.~Jang, K.~Park, and S.~Moon.
\newblock Packetshader: a gpu-accelerated software router.
\newblock In \emph{Proceedings of the ACM SIGCOMM 2010 conference}, SIGCOMM
  '10, pages 195--206, New York, NY, USA, 2010. ACM.
\newblock ISBN 978-1-4503-0201-2.
\newblock \doi{10.1145/1851182.1851207}.
\newblock URL \url{http://doi.acm.org/10.1145/1851182.1851207}.

\bibitem[Hensley(2007)]{AMD:CTM:2007}
J.~Hensley.
\newblock {AMD CTM Overview}.
\newblock In \emph{ACM SIGGRAPH 2007 courses}, SIGGRAPH '07, New York, NY, USA,
  2007. ACM.
\newblock \doi{10.1145/1281500.1281648}.

\bibitem[Hetherington et~al.(2012)Hetherington, Rogers, Hsu, O'Connor, and
  Aamodt]{Hetherington:2012:CEK:2310660.2310991}
T.~H. Hetherington, T.~G. Rogers, L.~Hsu, M.~O'Connor, and T.~M. Aamodt.
\newblock Characterizing and evaluating a key-value store application on
  heterogeneous cpu-gpu systems.
\newblock In \emph{Proceedings of the 2012 IEEE International Symposium on
  Performance Analysis of Systems \& Software}, ISPASS '12, pages 88--98,
  Washington, DC, USA, 2012. IEEE Computer Society.
\newblock ISBN 978-1-4673-1143-4.
\newblock \doi{10.1109/ISPASS.2012.6189209}.
\newblock URL \url{http://dx.doi.org/10.1109/ISPASS.2012.6189209}.

\bibitem[http://netronome.com()]{netronome}
http://netronome.com.

\bibitem[{Intel Corp}()]{intel:xeon:phi}
{Intel Corp}.
\newblock Intel xeon phi coprocessor.
\newblock
  \url{http://www.intel.com/content/www/us/en/processors/xeon/xeon-phi-coproce%
ssor-datasheet.html}.

\bibitem[Janapa~Reddi et~al.(2010)Janapa~Reddi, Lee, Chilimbi, and
  Vaid]{JanapaReddi:2010:WSU:1815961.1816002}
V.~Janapa~Reddi, B.~C. Lee, T.~Chilimbi, and K.~Vaid.
\newblock Web search using mobile cores: quantifying and mitigating the price
  of efficiency.
\newblock In \emph{Proceedings of the 37th annual international symposium on
  Computer architecture}, ISCA '10, pages 314--325, New York, NY, USA, 2010.
  ACM.
\newblock ISBN 978-1-4503-0053-7.
\newblock \doi{10.1145/1815961.1816002}.
\newblock URL \url{http://doi.acm.org/10.1145/1815961.1816002}.

\bibitem[Kgil et~al.(2008)Kgil, Saidi, Binkert, Reinhardt, Flautner, and
  Mudge]{Kgil:2008:PUS:1412587.1412589}
T.~Kgil, A.~Saidi, N.~Binkert, S.~Reinhardt, K.~Flautner, and T.~Mudge.
\newblock Picoserver: Using 3d stacking technology to build energy efficient
  servers.
\newblock \emph{J. Emerg. Technol. Comput. Syst.}, 4\penalty0 (4):\penalty0
  16:1--16:34, Nov. 2008.
\newblock ISSN 1550-4832.
\newblock \doi{10.1145/1412587.1412589}.
\newblock URL \url{http://doi.acm.org/10.1145/1412587.1412589}.

\bibitem[Lang and Patel(2009)]{Lang:CIDR:2009}
W.~Lang and J.~Patel.
\newblock {Towards Eco-friendly Database Management Systems}.
\newblock In \emph{{4th Biennial Conference on Innovative Data Systems
  Research}}, Asilomar, California, USA, January 4-7 2009.

\bibitem[Larus and Parkes(2001)]{CohortScheduling}
J.~R. Larus and M.~Parkes.
\newblock {{Using Cohort Scheduling to Enhance Server Performance (Extended
  Abstract)}}.
\newblock In \emph{{LCTES '01: Proceedings of the ACM SIGPLAN workshop on
  Languages, compilers and tools for embedded systems}}, pages 182--187, New
  York, NY, USA, 2001. ACM.
\newblock ISBN 1-58113-425-8.
\newblock \doi{http://doi.acm.org/10.1145/384197.384222}.

\bibitem[Li et~al.(2011)Li, Lim, Faraboschi, Chang, Ranganathan, and
  Jouppi]{Li:2011:SIS}
S.~Li, K.~Lim, P.~Faraboschi, J.~Chang, P.~Ranganathan, and N.~P. Jouppi.
\newblock System-level integrated server architectures for scale-out
  datacenters.
\newblock In \emph{Proceedings of the 44th Annual IEEE/ACM International
  Symposium on Microarchitecture}, MICRO-44 '11, pages 260--271, New York, NY,
  USA, 2011. ACM.
\newblock ISBN 978-1-4503-1053-6.
\newblock \doi{10.1145/2155620.2155651}.
\newblock URL \url{http://doi.acm.org/10.1145/2155620.2155651}.

\bibitem[Lim et~al.(2013)Lim, Meisner, Saidi, Ranganathan, and
  Wenisch]{Lim:2013:TSS}
K.~Lim, D.~Meisner, A.~G. Saidi, P.~Ranganathan, and T.~F. Wenisch.
\newblock Thin servers with smart pipes: designing soc accelerators for
  memcached.
\newblock In \emph{Proceedings of the 40th Annual International Symposium on
  Computer Architecture}, ISCA '13, pages 36--47, New York, NY, USA, 2013. ACM.
\newblock ISBN 978-1-4503-2079-5.
\newblock \doi{10.1145/2485922.2485926}.
\newblock URL \url{http://doi.acm.org/10.1145/2485922.2485926}.

\bibitem[Liu et~al.(2005)Liu, Saifullah, Greis, and
  Sreemanthula]{httpcompression}
Z.~Liu, Y.~Saifullah, M.~Greis, and S.~Sreemanthula.
\newblock Http compression techniques.
\newblock In \emph{Wireless Communications and Networking Conference, 2005
  IEEE}, volume~4, pages 2495--2500 Vol. 4, 2005.
\newblock \doi{10.1109/WCNC.2005.1424906}.

\bibitem[Long et~al.(2010)Long, Franklin, Biswas, Ortiz, Oberg, Fan, and
  Chong]{Long:MICRO:2010}
G.~Long, D.~Franklin, S.~Biswas, P.~Ortiz, J.~Oberg, D.~Fan, and F.~T. Chong.
\newblock {Minimal Multi-threading: Finding and Removing Redundant Instructions
  in Multi-threaded Processors}.
\newblock In \emph{{Proceedings of the 2010 43rd Annual IEEE/ACM International
  Symposium on Microarchitecture}}, MICRO '43, pages 337--348, Washington, DC,
  USA, 2010. IEEE Computer Society.
\newblock ISBN 978-0-7695-4299-7.
\newblock \doi{MICRO.2010.41}.

\bibitem[{Lotfi-Kamran} et~al.(2012){Lotfi-Kamran}, Grot, Ferdman, Volos,
  Kocberber, Picorel, Adileh, Jevdjic, Idgunji, Ozer, and
  Falsafi]{Lotfi-Kamran:2012:SP}
P.~{Lotfi-Kamran}, B.~Grot, M.~Ferdman, S.~Volos, O.~Kocberber, J.~Picorel,
  A.~Adileh, D.~Jevdjic, S.~Idgunji, E.~Ozer, and B.~Falsafi.
\newblock Scale-out processors.
\newblock In \emph{Proceedings of the 39th Annual International Symposium on
  Computer Architecture}, ISCA '12, pages~--, Washington, DC, USA, 2012. IEEE
  Computer Society.
\newblock ISBN 978-1-4503-1642-2.
\newblock \doi{10.1145/2337159.2337217}.
\newblock URL \url{http://dx.doi.org/10.1145/2337159.2337217}.

\bibitem[Luk et~al.(2005)Luk, Cohn, Muth, Patil, Klauser, Lowney, Wallace,
  Reddi, and Hazelwood]{Luk:PLDI:2005}
C.-K. Luk, R.~Cohn, R.~Muth, H.~Patil, A.~Klauser, G.~Lowney, S.~Wallace, V.~J.
  Reddi, and K.~Hazelwood.
\newblock {Pin: Building Customized Program Analysis Tools with Dynamic
  Instrumentation}.
\newblock In \emph{{Proceedings of the 2005 ACM SIGPLAN conference on
  Programming language design and implementation}}, PLDI '05, pages 190--200,
  New York, NY, USA, 2005. ACM.
\newblock ISBN 1-59593-056-6.
\newblock \doi{10.1145/1065010.1065034}.

\bibitem[Meisner and Wenisch(2012)]{Meisner:2012:DAS}
D.~Meisner and T.~F. Wenisch.
\newblock Dreamweaver: architectural support for deep sleep.
\newblock In \emph{Proceedings of the seventeenth international conference on
  Architectural Support for Programming Languages and Operating Systems},
  ASPLOS XVII, pages 313--324, New York, NY, USA, 2012. ACM.
\newblock ISBN 978-1-4503-0759-8.
\newblock \doi{10.1145/2150976.2151009}.
\newblock URL \url{http://doi.acm.org/10.1145/2150976.2151009}.

\bibitem[Meng et~al.(2010)Meng, Tarjan, and Skadron]{Meng:ISCA:2010}
J.~Meng, D.~Tarjan, and K.~Skadron.
\newblock {Dynamic Warp Subdivision for Integrated Branch and Memory Divergence
  Tolerance}.
\newblock In \emph{{Proceedings of the 37th annual International Symposium on
  Computer Architecture}}, ISCA '10, pages 235--246, New York, NY, USA, 2010.
  ACM.
\newblock ISBN 978-1-4503-0053-7.
\newblock \doi{10.1145/1815961.1815992}.

\bibitem[Mudge and Holzle(2010)]{Mudge:2010:COE}
T.~Mudge and U.~Holzle.
\newblock Challenges and opportunities for extremely energy-efficient
  processors.
\newblock \emph{IEEE Micro}, 30\penalty0 (4):\penalty0 20--24, July 2010.
\newblock ISSN 0272-1732.
\newblock \doi{10.1109/MM.2010.61}.
\newblock URL \url{http://dx.doi.org/10.1109/MM.2010.61}.

\bibitem[NVIDIA({\natexlab{a}})]{nvidia:kepler:spec}
NVIDIA.
\newblock Tesla k20 gpu accelerator board specification.
\newblock
  \url{http://www.nvidia.com/content/PDF/kepler/Tesla-K20-Passive-BD-06455-001%
-v05.pdf}, {\natexlab{a}}.

\bibitem[NVIDIA({\natexlab{b}})]{nvidia:tegra3}
NVIDIA.
\newblock Nvidia tegra mobile processors.
\newblock \url{http://www.nvidia.com/object/tegra-3-processor.html},
  {\natexlab{b}}.

\bibitem[Nygren et~al.(2010)Nygren, Sitaraman, and
  Sun]{Nygren:2010:ANP:1842733.1842736}
E.~Nygren, R.~K. Sitaraman, and J.~Sun.
\newblock The akamai network: a platform for high-performance internet
  applications.
\newblock \emph{SIGOPS Oper. Syst. Rev.}, 44\penalty0 (3):\penalty0 2--19, Aug.
  2010.
\newblock ISSN 0163-5980.
\newblock \doi{10.1145/1842733.1842736}.
\newblock URL \url{http://doi.acm.org/10.1145/1842733.1842736}.

\bibitem[Richardson et~al.(2013)Richardson, Fini, and
  Nelson]{Richardson:2013fk}
D.~J. Richardson, J.~M. Fini, and L.~E. Nelson.
\newblock Space-division multiplexing in optical fibres.
\newblock \emph{Nat Photon}, 7\penalty0 (5):\penalty0 354--362, 05 2013.
\newblock URL \url{http://dx.doi.org/10.1038/nphoton.2013.94}.

\bibitem[Ruetsch and Micikevicius()]{cuda:transpose}
G.~Ruetsch and P.~Micikevicius.
\newblock Optimizing matrix transpose in cuda.
\newblock
  \url{http://docs.nvidia.com/cuda/samples/6_Advanced/transpose/doc/MatrixTran%
spose.pdf}.

\bibitem[Sartori et~al.(2012)Sartori, Ahrens, and Kumar]{sartori:hpca12}
J.~Sartori, B.~Ahrens, and R.~Kumar.
\newblock Power balanced pipelines.
\newblock In \emph{High Performance Computer Architecture (HPCA), 2012 IEEE
  18th International Symposium on}, pages 1--12, 2012.
\newblock \doi{10.1109/HPCA.2012.6169032}.

\bibitem[Silberstein et~al.(2013)Silberstein, Ford, Keidar, and
  Witchel]{asplos:gpufs}
M.~Silberstein, B.~Ford, I.~Keidar, and E.~Witchel.
\newblock Gpufs: integrating file systems with gpus.
\newblock In \emph{Proceedings of the Eighteenth International Conference on
  Architectural Support for Programming Languages and Operating Systems},
  ASPLOS '13. ACM, 2013.

\bibitem[{Standard Performance Evaluation Corporation}()]{SPECweb}
{Standard Performance Evaluation Corporation}.
\newblock {{SPECweb2009}}.
\newblock \url{http://www.spec.org/web2009/}.

\bibitem[Sysoev()]{nginx}
I.~Sysoev.
\newblock Nginx introduction and architecture overview.
\newblock \url{http://nginx.org/en/docs/introduction.html}.

\bibitem[Tarjan et~al.(2009)Tarjan, Meng, and Skadron]{Tarjan:SC:2009}
D.~Tarjan, J.~Meng, and K.~Skadron.
\newblock Increasing memory miss tolerance for simd cores.
\newblock In \emph{Proceedings of the Conference on High Performance Computing
  Networking, Storage and Analysis}, SC '09, pages 22:1--22:11, New York, NY,
  USA, 2009. ACM.
\newblock ISBN 978-1-60558-744-8.
\newblock \doi{10.1145/1654059.1654082}.
\vfill\eject
\bibitem[Tseng and Tullsen(2011)]{Tseng:HPCA:2011}
H.~Tseng and D.~Tullsen.
\newblock {Data-Triggered Threads: Eliminating Redundant Computation}.
\newblock In \emph{{Proceedings of 17th International Symposium on High
  Performance Computer Architecture}}, HPCA-17, 2011.

\bibitem[Vasudevan et~al.(2012)Vasudevan, Kaminsky, and
  Andersen]{Vasudevan:2012:UVI:2391229.2391237}
V.~Vasudevan, M.~Kaminsky, and D.~G. Andersen.
\newblock Using vector interfaces to deliver millions of iops from a networked
  key-value storage server.
\newblock In \emph{Proceedings of the Third ACM Symposium on Cloud Computing},
  SoCC '12, pages 8:1--8:13, New York, NY, USA, 2012. ACM.
\newblock ISBN 978-1-4503-1761-0.
\newblock \doi{10.1145/2391229.2391237}.
\newblock URL \url{http://doi.acm.org/10.1145/2391229.2391237}.

\bibitem[Welsh et~al.(2001)Welsh, Culler, and Brewer]{SEDA}
M.~Welsh, D.~Culler, and E.~Brewer.
\newblock {SEDA: An Architecture for Well-Conditioned, Scalable Internet
  Services}.
\newblock In \emph{{SOSP '01: Proceedings of the eighteenth ACM symposium on
  Operating systems principles}}, pages 230--243, New York, NY, USA, 2001. ACM.
\newblock ISBN 1-58113-389-8.
\newblock \doi{10.1145/502034.502057}.

\bibitem[Wu et~al.(2012)Wu, Diamos, Cadambi, and
  Yalamanchili]{Wu:2012:KWA:2457472.2457490}
H.~Wu, G.~Diamos, S.~Cadambi, and S.~Yalamanchili.
\newblock Kernel weaver: Automatically fusing database primitives for efficient
  gpu computation.
\newblock In \emph{Proceedings of the 2012 45th Annual IEEE/ACM International
  Symposium on Microarchitecture}, MICRO '12, pages 107--118, Washington, DC,
  USA, 2012. IEEE Computer Society.
\newblock ISBN 978-0-7695-4924-8.
\newblock \doi{10.1109/MICRO.2012.19}.
\newblock URL \url{http://dx.doi.org/10.1109/MICRO.2012.19}.
\end{thebibliography}

\end{document}

