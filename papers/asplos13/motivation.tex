\section{Motivation}
\label{sec:motivation}
This section
motivates our work by first reviewing GPU-style accelerator
efficiencies and then discussing several networking and system
architecture trends that remove constraints of existing systems.  We conclude this section with a study that
demonstrates the existence of similarity across requests.
 

% Our work on \rhythm is motivated the need to answer the following
% questions:
% \begin{enumerate}
%   \item{Does it make sense to run a web server on a GPU-style
%       accelerator?}
%     \item{What type of performance and power efficiency
%         (throughput/Watt) can be achieved?}small
% \end{enumerate}

\subsection{Accelerator Efficiency}
GPU-style accelerators achieve efficiency through two primary
mechanisms: hardware multithreading and SIMD execution.  First, for
high throughput computing, hardware multithreading enables significant
latency tolerance by overlapping the execution
of multiple threads.   Second, by utilizing a single instruction
multiple thread (SIMT) model, properly aligned threads exploit data
parallel execution and amortize instruction fetch and decode overhead.
Recent research indicates instruction fetch and decode can be as high
as 40\%-50\% of overall core power~\cite{sartori:hpca12}.  Amortizing
this power over more operations can provide benefits
across nearly all types of microarchitectures, from complex
out-of-order cores to simple in-order cores.  Although in this paper we
focus on GPU-style accelerators, our work is broadly applicable to
data parallel architectures.

The potential efficiencies available with emerging GPU-style
accelerators is compelling.  However, several aspects of current
systems may limit the ability to exploit these efficiences.  These
limitations include PCIe bandwidth and latency, network bandwidth and
the potential lack of similarity across requests.  Below we address
these potential limitations.

\subsection{Enabling Trends}
Current systems may introduce bottlenecks that limit the
overall server throughput possible with a highly threaded
accelerator.  Here we identify three trends that can remove or
mitigate these bottlenecks enabling scale-up solutions for high
throughput servers.

\subsubsection{Increasing Bandwidth}
Current systems may have bandwidth bottlenecks at either the network
or the PCIe bus that limit overall throughput.  Today's systems
utilize 10 Gbps - 40 Gbps networking infrastructure and PCIe 3.0 for
the system interconnect.  A single 10 Gbps link limits throughput to
approximately 1M req/sec for 1KB requests, and is lower for larger
messages.  However, efforts are underway to create 100 Gbps and 400
Gbps network standards~\cite{ieee802.3} and announced systems claim to
sustain these bandwidths~\cite{netronome}.  Furthermore, research is
exploring techniques to achieve $10^{12}$ bps to $10^{15}$ bps using
novel multicore fiber optics~\cite{Richardson:2013fk}.

Once network bandwidth increases, the PCIe bus may become the
bottleneck. Fortunately, PCIe 4.0 doubles bandwidth to 16G Transfers/sec~\cite{pciesig} and
as discussed below, system on chip architectural changes can
eliminate/reduce system bus bandwidth limitations. We believe that 
future server platforms can exploit these progressive enhancements 
in network and system bandwidth to sustain significant throughput per node.

\subsubsection{System on Chip}
Another trend in processor and system architecture is heterogeneous
computing where specialized accelerators and general purpose cores are
combined in a system-on-chip (SoC) design (e.g., Tegra~\cite{nvidia:tegra3}, AMD
Fusion~\cite{AMD:Fusion}).  SoCs can reduce latency, improve bandwidth and reduce power by avoiding off-chip
latency and exploiting on-chip density for a high bandwidth
interconnect.  Recent analysis~\cite{Li:2011:SIS,Lim:2013:TSS} 
shows overall benefits to data center design by using SoC architectures
to reduce TCO, and mention the possibility of integrated accelerators
for servers.

\subsubsection{Operating System and Backend Services}
A final enabling trend is support for high throughput access to OS
services.  Recent research is exploring both high throughput and clean
abstractions for parallel access to OS services.
GPUfs~\cite{asplos:gpufs} provides a
filesystem abstraction that enables access from the GPU.  Similar in
spirit is the recent development of vector
interfaces~\cite{Vasudevan:2012:UVI:2391229.2391237} that can provide
1M input/output operations per second to a high performance solid
state disk (SSD).  There is also recent work on providing high
throughput backend services by exploiting
GPUs~\cite{Bakkum:2010:ASD,Hetherington:2012:CEK:2310660.2310991,Wu:2012:KWA:2457472.2457490} or
constructing specialized hardware for key-value (e.g., memcached~\cite{Fitzpatrick:memcached})
servers~\cite{Chalamalasetti:2013:FMA:2435264.2435306,Lim:2013:TSS}.

% The above trends all point to a future where high request rates can be
% delivered to a single server in a data center.  This represents a
% potential turning point in server design for some workloads that
% requires an increase in per node throughput per watt (scale-up)
% approach to complement today's scale-out designs.  GPU-style
% accelerators might meet this need, if request-based workloads can be
% transformed to match the restricted execution environment. 
 
\subsection{Request Similarity}

% Usage patterns found in web servers are generally viewed as
% transactional. In its simplest form, a client issues a request to the
% server and receives a response in return. Traditionally, each request
% is handled individually, either with a dedicated thread (i.e.,
% Apache2~\cite{Apache,ApacheProject}) or by a thread from a pool in an
% event-based server ~\cite{CohortScheduling,SEDA,nginx}. A
% first step at improving efficiency exploits the throughput-oriented
% aspect of servers to execute low ILP threads on simple
% cores~\cite{Andersen:2009:FAWN}; however, these techniques may not
% work for all workloads~\cite{JanapaReddi:2010:WSU:1815961.1816002,Mudge:2010:COE}. Furthermore,
% since each thread is still treated independently these techniques do
% not exploit the potential efficiency improvements available through
% data parallel hardware.

% We propose to improve efficiency based on the observation of
% similarity between requests.  Intelligently grouping and scheduling
% incoming requests that execute the same code can be used to reduce
% overheads.  Previous research demonstrated that request batching
% increases server throughput by using cohort
% scheduling~\cite{CohortScheduling}. The idea is to delay servicing a request
% until additional requests arrive at a similar point in their
% processing, and then execute the threads in this group (cohort)
% consecutively.  This approach improves instruction and data locality
% of the server workload, improving overall performance.  We believe
% that similarity of requests can be further exploited by the hardware
% to improve efficiency.

% Our hypothesis is that sufficient similarity in requests exists within
% a given class of server workloads (e.g, the login function) that we
% can exploit cohort scheduling to expose parallelism in instruction
% processing.  In particular, we seek to improve efficiency by removing
% redundant portions of instruction processing (e.g., fetch) by using
% data parallel or Single Instruction Multiple Data (SIMD)
% execution~\cite{Fynn:IEEE:1966}.  


The potential efficiencies of parallel accelerators is compelling, and
the above trends expose the opportunity to exploit those efficiences
in future high throughput server designs.  However, it remains to be
determined if server workloads can exploit those efficiencies.  As a
first step toward answering this question we performed preliminary
analysis of the SPECWeb2009 Banking workload~\cite{SPECweb}.  This workload is a PHP
web site representative of typical web-based banking.

% The general sequence to process a request for our example workload
% begins with an HTTP request arriving at the server.  The request is
% parsed to obtain the top-level PHP file accessed (e.g., login, account
% summary) and extract any arguments.  The specific request is then
% executed, which may involve a network transaction to a backend
% database to retrieve user-specific information (e.g., password,
% account balance, recent transactions, etc.) or filesystem accesses to
% retrieve images (e.g., checks).  In SPECWeb the backend database is
% implemented as a standalone application---called BESIM, whereas in
% real deployed workloads this would be SQL and/or a key-value store (e.g.,
% memcached~\cite{Fitzpatrick:memcached}).  Finally, an HTML response is
% created that is sent back to the originating client.

We use Pin~\cite{Luk:PLDI:2005} to obtain x86 dynamic basic block traces for
61 individual dynamic web content (PHP) requests from the SPECWeb2009 Banking
workload.  These requests access 14 distinct top-level PHP files
served by the Apache web server.\footnote{There are a total of 16
  requests in this workload, but our experiments only exercised 14 of
  them.}  We use the UNIX utility {\em diff} to merge traces and obtain a
measure of similarity between request traces based on the length of
the merged trace.  Minimal differences between traces indicates the
two executions followed the same control paths during their execution.

The opportunity for exploiting GPU-style hardware for server workloads
is obtained by examining the length of merged traces (e.g., shorter
merged traces implies faster execution time).  The resulting merge is
an approximation of the actual execution order for all the batched
requests executing on data parallel hardware.  Note that our analysis
is performed offline, and thus assumes a very high request
arrival rate.
% \arl{Do we need the stuff after here?} In a deployed system, the combination of work per
% request and rate of arrival must be sufficiently high to offset the
% overheads of utilizing the GPU.  Integrated heterogenous systems and
% more sophisticated workloads may reduce these overheads or their
% impact, and thus improve the opportunity for utilizing the GPU.

We merge traces for independent requests that access the same initial PHP file, resulting
in 14 distinct merged traces.
Execution speedup is approximated as the sum of traces (in number of basic blocks)
divided by the merged trace size (i.e., cohort execution on idealized SIMD
hardware).  In our experiments between 2 and 6 traces per request (PHP file) are merged,
with most requests having 5 unique traces.
Figure~\ref{fig:cluster-speedup} shows per request speedup
normalized to ideal (linear) speedup.  From these results we observe
nearly linear speedup (i.e., nearly identical executions) for each
request type. 

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\textwidth]{figs/cluster_speedup.pdf}
 \caption{Potential Speedup of SPECWeb2009 Banking Workload on Data
    Parallel Hardware Relative to Ideal Speedup}
   \label{fig:cluster-speedup}
\end{figure}

We performed a similar study using Facebook's HipHop PHP to C++
system~\cite{facebook:hiphop}.  This experiment removes any potential
affects of online PHP interpreting in Apache/Zend.  We observed
qualitatively similar results (not shown) with significant similarity
among requests of the same type.  Furthermore, on average there are approximately
33,000 instructions between system calls with 97\% of them related to filesystem or network I/O and
could utilize the high throughput interfaces described above (i.e., GPUfs
and vector interfaces).

The above results quantitatively demonstrate the intuitive result that
similar instruction control flow exists in a server
workload for requests of the same type. Similar control
flow is just one aspect required to efficiently utilize GPU-style
compute accelerators.  Other additional challenges include, but are not limited to: 1)
scheduling, 2) data divergence, 3) copy overheads, and 4) current
platform limitations.



