\section{Methodology}
\label{sec:methodology}

\subsection{Platforms}
Table~\ref{tbl:platforms} shows the various platforms we 
use to test workload performance. We use the quad-core 
Core i5 and Core i7 to represent the x86 family, and the dual-core 
Cortex A9 to represent the ARM family. The NVIDIA GTX Titan 
is used for our GPU measurements.
\begin{table}[htb]
\small
\begin{tabular}{|c|c|p{2in}|}
\hline
Platform & GHz  & Description\\ 
\hline
\hline
Core i5 & 3.4 & Core i5 3570, 22 nm, 4 cores (4 threads), 8GB DDR3 RAM, 1Gbps NIC\\
\hline  
Core i7 & 3.4 & Core i7 3770, 22 nm, 4 cores (8 threads), 16GB DDR3 RAM, 1Gbps NIC\\
\hline  
ARM A9 & 1.2  & OMAP 4460, 45 nm, Panda board, 2 cores, 1GB LPDDR2 RAM\\
\hline  
Titan & 0.8 & GTX Titan, 28 nm, 14 Streaming Multiprocessors, 6GB GDDR5 Memory\\
\hline
\end{tabular}
\caption{Experimental System Platforms}
\label{tbl:platforms}
\end{table}

\begin{table*}[htb]
\centering
\small
\begin{tabular}{|l|c|c|c|c|c|}
\hline
Request & x86 Instructions & \multicolumn{2}{c|}{Response Size (KB)} & Fraction of & Backend \\
\cline{3-4}
Type & per Request& SPECWeb & Rhythm & Requests (\%) & Requests\\
\hline 
\hline
login & 132,401 & 4 & 8 & 28.17 & 2\\
\hline 
account summary & 392,243 & 17 & 32 & 19.77 & 1\\
\hline 
add payee & 335,605 & 18 & 32 & 1.47 & 0\\
\hline 
bill pay & 334,105 & 15 & 32 & 18.18 & 1\\
\hline 
bill pay status output & 485,176 & 24 & 32 & 2.92 & 1\\
\hline 
change profile & 560,505 & 29 & 32 & 1.60 & 1\\
\hline 
check detail html & 240,615 & 11 & 16 & 11.06 & 1\\
\hline 
order check & 433,352 & 21 & 32 & 1.60 & 1\\
\hline 
place check order & 466,283 & 25 & 32 & 1.15 & 1\\
\hline 
post payee & 638,598 & 34 & 64 & 1.05 & 1\\
\hline 
post transfer & 334,267 & 16 & 32 & 1.60 & 1\\
\hline 
profile & 590,816 & 32 & 64 & 1.15 & 1\\
\hline 
transfer & 277,235 & 13 & 16 & 2.24 & 1\\
\hline 
logout & 792,684 & 46 & 64 & 8.06 & 0\\
\hline
Average & 429,563 & 15.5 & 26.4 & 100 & 1.2\\
\hline
\end{tabular}
\caption{SPECWeb Banking Workload}
\label{tbl:specwebdistr}
\end{table*}

We use the SPECWeb Banking benchmark~\cite{SPECweb} for our studies.  For general
purpose processors we implement a standalone
event-based C version and for the GPU we implement a
\rhythm C+CUDA version. We implement 14 out 
of 16 Banking requests, and normalize the request percentages 
to sum to 100\%. We skip the \emph{quick pay} and \emph{check detail images} benchmarks. 
\emph{Quick pay} uses a variable number of kernel launches based on backend 
data, making it difficult to implement, and \emph{check detail images} 
is completely disk bound, requiring GPUfs integration to allow us to 
process it on the GPU. We plan to address both these requests in future 
work. Table~\ref{tbl:specwebdistr} summarizes
characteristics about the Banking workload. The second column is the
dynamic instruction count for our standalone C implementation and is
the average across 100 random requests, and when combined with the
third column we see a diverse mix of requests with varying
compute/response byte ratios.

We use Ubuntu 12.04 with CUDA 5.5RC on our x86 platform and Linaro 13.01 
for our ARM platform. All code is compiled using gcc with -O3 enabled.
We test our server against the SPECWeb client
validator to guarantee correctness.  The X server is shut down for all
our benchmark runs. We unplug the GPU for the baseline x86 test runs.
For the CUDA version, we
allocate 1KB per backend request and 4KB per backend response.  We use
the next higher power of two for the HTML response size
(Table~\ref{tbl:specwebdistr}), since powers of two allow us to easily
divide work on the hardware for the response transpose.

We implement support for static images in our prototype, however, 
image throughput is primarily dictated by network bandwidth since there 
is no processing involved. The parser groups image requests into an 
image cohort, these cohorts bypass the process stage and the image  
responses are sent to the respective clients. Image cohorts can be 
processed on the device as well using GPUfs~\cite{asplos:gpufs}, however,
we leave that to future work. Static images can also be served at high 
throughputs via Content Delivery Networks (CDNs) like Akamai~\cite{Nygren:2010:ANP:1842733.1842736}. 
We do not evaluate image throughput for our prototype.

\subsection{Metrics}
Our metrics of interest include 1) throughput, 2) power, 
3) latency, and 4) throughput/watt. We obtain throughput 
using the unix \emph{clock\_gettime()} interface to measure end-to-end time 
to process a set number of requests. Latency is calculated 
by logging the time that a request arrives and subtracting 
the request completion time, and we compute an average latency over
all requests.
Power is measured at the wall outlet using a Kill-A-Watt meter. 
We measure the idle and test power for each of our runs. 
Subtracting the two gives us the dynamic\footnote{We use
  dynamic to represent the non-idle power under load. This is distinct
from dynamic/static power used for circuit analysis.} power consumed by 
the workload. We examine throughput/Watt for both wall power 
and dynamic power as both of these represent different 
viewpoints on system efficiency. A system's cost of ownership 
is effectively based on wall power, whereas dynamic power 
measures the marginal costs incurred due to load.

\subsection{Modeling Future Systems}
SPECWeb's default test harness is based on Java and is quite slow, 
rendering it unusable for our system. We create our own test 
harness and use various optimizations to allow us 
to efficiently model future high bandwidth networks and 
datacenter-level throughputs.

\subsubsection{Input Generation}
We use the C \emph{rand()} function to randomly generate input request 
data. For request types other than \emph{login}, we randomly generate 
session identifiers and populate the session array with random user 
ids. We test each request type in isolation and process 48M requests. Using the 
request distributions from Table~\ref{tbl:specwebdistr}, we compute a weighted harmonic mean 
of request efficiency(throughput/watt) to obtain the efficiency for the entire workload.

\subsubsection{Emulation}
Our test system uses a 1Gbps NIC, and for an average response 
size of 16KB, cannot support more than $\sim8$K requests/sec. Similarly,
PCIE bandwidth may artificially limit throughput.  To explore future
system architectures, we model three \rhythm different systems that
progressively add capabilities: Titan A, Titan B, and Titan C.

{\bf x86 and ARM platforms} run our C version of the Banking workload. 
For maximum throughput, we eliminate network and PCI limitations by generating 
requests from and copying responses to main memory and implement the backend 
as a function call.

{\bf Titan A} models a high bandwidth network by generating requests 
locally and not sending responses across the network. We also run the 
backend locally as one or more threads on the local machine to emulate
the requisite backend throughput.  We pre-generate requests into a buffer, 
and read them from memory on the fly to emulate high arrival 
rates. 

{\bf Titan B} extends the above design to eliminate the PCIe bandwidth
bottleneck by implementing the SPECWeb Besim backend on the GPU. This
emulates the effect of an SoC style approach with an integrated
general purpose core and NIC. A local device backend also avoids the
need to transpose the backend request and response data, potentially
further improving performance.

{\bf Titan C} further extends our system to emulate specialized
hardware that performs the final transpose on the device after response generation,
just prior to sending the response on the network. The response
transpose could be performed on the host, on the NIC while reading
data to send to the clients, or 
by a specialized logic unit associated with the memory
controller (e.g., the logic layer of a 3D DRAM~\cite{memorycube}).
The latter is a general approach that could be used for other
transpose operations in the \rhythm pipeline.

All of our optimizations are validated for correctness since we run on
real hardware, and we can examine the output. Eliminating the network
only eliminates the overheads of the \emph{read()} and \emph{write()}
system calls and the network stack, and optimizing them is an
important, but orthogonal issue to our work.  A local or device
backend emulates a high throughput key-value
store~\cite{Vasudevan:2012:UVI:2391229.2391237}, or the use of a
database cache~\cite{Fitzpatrick:memcached} on the local machine,
which is commonly used to tolerate backend latencies.
