\section{Rhythm Server Implementation}
\label{sec:implementation}

We implement a prototype version of \rhythm to evaluate its potential
on existing NVIDIA GPU hardware.  We apply several implementation
specific optimizations to the \rhythm design. Many of these
optimizations are well known software constructions and \rhythm
exploits them for its goals of maximum throughput on accelerator
hardware.

\subsection{Pipeline Control}

\paragraph{Event loop}
\rhythm is single threaded by design, therefore it uses a central
event loop based on \emph{epoll} to process all I/O events and device
interactions. These include requests for new connections, backend responses, service
requests for existing clients, and file system responses.
Asynchronous stage execution (i.e., transitions in the cohort FSM) is
managed using callbacks and local unix \emph{pipes}. Callbacks are maintained 
as a linked list that is traversed on each iteration of the
event loop.

\paragraph{Callbacks}
We use callbacks to track stage completion and transitions. The 
pipeline stages execute asynchronously on the device, and we 
need a mechanism to detect completion. Unfortunately, our current 
platform does not support interrupts from the device, therefore 
we use callbacks to implement a polling mechanism in the event 
loop. At the beginning of each stage, a callback is added to 
the callback list. We execute this callback to poll the stage
and only remove it from the list if the stage is complete. When a 
stage completes, it adds another callback to start execution 
of the next stage in the pipeline. Although this approach doesn't 
appear to limit throughput, it introduces unnecessary power 
consumption for polling many inflight stages.

\paragraph{Cohort context synchronization}
Our current implementation uses the host for cohort dispatch, and thus 
requires maintaining cohort context on both the 
host and the device. We synchronize these two copies of the context 
at the parser since it is the only stage that modifies device 
contexts (setting them to \emph{Partially\_Full} and \emph{Full}) by 
populating cohorts with requests. The host contexts are copied to the device
at parser launch, and the device contexts are copied to the host at parser
termination for use in dispatch. Dispatch and response both execute
on the host and modify the host cohort context by setting it to \emph{Busy} 
or \emph{Free}, respectively. 

\subsection{Pipeline Stages}
\paragraph{Reader/Parser} 
The reader is double buffered to overlap
request processing and accumulation. When a cohort becomes full, 
the reader swaps the front and back buffers, signals the parser to 
begin processing, and resumes reading requests into the new front 
buffer. If the back buffer is not free, the reader stalls, waiting 
for the parser. The reader and parser are low latency stages, and 
a single instance of each is sufficient to achieve high throughput in
our experiments. 

\paragraph{Process/Backend}
The overall processing of a request is divided into one or more
process stages separated by backend accesses. In our current 
implementation we preallocate pipeline resources (i.e., memory)
for all process stages (including the backend when appropriate) and 
the response stage at the first process stage launch. Each stage is a 
computational unit on the device, called a \emph{kernel}. Kernel progress and 
termination is tracked by the event loop. The overall process phase 
is latency bound depending on the specific request type. We provide 
multiple instances of process and response resources to maintain 
as many cohorts in flight as possible, allowing
us to hide the latency of individual kernels and increase throughput.
Exploring alternative implementations that do not preallocate resources is
part of our future work.

\paragraph{Response}
The final process stage adds a
callback upon termination, and the response stage is invoked at the
next iteration of the event loop. The callback is removed after the
responses for the cohort are sent to the respective clients.  All
resources used by this cohort become available for reuse by
subsequent cohorts.

\subsection{Data Structures}
\rhythm uses four primary data structures to support the web server: 
a cohort pool and the associated cohort contexts, a session array, a request buffer
and a response buffer, each optimized to enable efficient data parallel execution. The cohort pool and contexts are implemented as static arrays to avoid allocation and synchronization overheads.

\subsubsection{HTTP Session Array/Cookies}
The session array is a device only structure that stores HTTP session
state for the clients handled by the server. Sessions are created at
login and destroyed upon logout. Since the session array is accessed
for every request, its performance is critical to server
throughput. To ensure conflict-free access for a cohort, the session
array is implemented as a hash table with the number of buckets equal
to the cohort size. Each request thread accesses a unique bucket, and
insertion into a bucket is performed randomly based on a hash of the userid. The
session identifier is a hash of the node index and the bucket index,
ensuring O(1) time lookup for a session node. Collisions upon
insertion are handled using a linear search for a free node, resulting
in O(1) for collision free insertion, and O(n) time in the case of a
collision. Since lookups are constant time and the array is static,
deletions are O(1) time.

\subsubsection{Request and Response Buffer Layout}
In a typical server application each request is allocated 
contiguous buffers for incoming and outgoing data. Unfortunately, when executing on a
GPU this data layout can undermine the potential benefits of executing
multiple requests simultaneously. Specifically, GPU memory systems
work best when memory references of the co-scheduled threads (referred to as a \emph{warp}) exhibit 
good spatial locality (often called \emph{coalesced} memory accesses). While 
a single thread has good spatial locality, across threads the memory 
locations accessed are separated by large distances.

To overcome this challenge we explore several methods. One approach
utilizes the GPU threads to cooperatively perform the operations
of a single request (intra-request concurrency). Unfortunately, this approach does
not exploit the similarity in instruction control flow across requests
(inter-request concurrency) and performs poorly.  Therefore, we use a second
approach that performs a data transformation on the buffers by 
transposing them to improve spatial locality. We also insert whitespaces 
in the generated HTML content to tolerate control divergence in the 
response generation stages.

\paragraph{Buffer Transpose}
\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{figs/transpose.pdf}
  \caption{Request Buffer Layout (for illustrative purposes only.)}
  \label{fig:layout}
\end{figure}
\begin{figure*}[t]
  \centering
  \includegraphics[scale=0.5]{figs/request_flow.pdf}
  \caption{Rhythm Web Server Request Processing Overview}
  \label{fig:reqflow}
\end{figure*}
We view the buffers per cohort as a 2D array with each row
representing the contiguous buffer for a given request, as shown in
Figure~\ref{fig:layout}. Initially these buffers are in row-major
layout. To improve spatial locality within a cohort, we need the
array in column-major layout so that thread buffers are interleaved in
the sequential address space. To achieve this, we perform a
simple array transpose operation and leverage existing
techniques to optimize the transpose~\cite{cuda:transpose}. When the
requests are finished processing we perform an additional 
transpose to convert the responses back to row-major
layout, with each buffer occupying contiguous locations in the linear
address space. Other server workloads may require similar data structure
design/transformations to fully utilize the hardware capabilities
available. Our ongoing work is exploring other server workloads.

\paragraph{Whitespace Padding in HTML Content}
Transposing buffers can provide coalesced memory accesses if the
individual thread buffer pointers are aligned (i.e., each thread uses
the same row index value). However, the web pages in our system are
dynamically generated with data returned from the backend database;
differences in returned data (i.e., string lengths) can result in
unaligned buffer pointers. Fortunately, we can exploit the HTML
specification, which allows an arbitrary number of linear white spaces
in the response body, to embed the appropriate number of whitespace
characters after newline characters for each buffer to realign the
buffer pointers.

\paragraph{Whitespace Padding in HTML Headers}
The HTTP response header requires the \emph{Content-length} field
whose value can only be known after the response is generated.
Conventional servers can generate the header after the entire response
content is created and use separate {\em send()} system calls for the
header and response.  For our
\rhythm implementation, we avoid the overhead of an additional header generation
stage by integrating header creation with response content creation.
This creates an issue since the header is located near the beginning
of the response buffer.  To overcome this and ensure buffer pointer
alignment we again exploit the HTML specification which allows white
spaces after a header field. We reserve a fixed amount of space in the
buffer by inserting white space characters (10 for a 32-bit content
length), and replace whitespace with the actual content length value after the response
is generated.

\subsection{Error Handling}
\rhythm maintains per request error state information to guarantee 
correctness.  Request errors create control divergence among 
threads in a cohort; however, we assume that these scenarios 
are rare and do not impact throughput.

\subsection{Request Flow in \rhythm}
Figure~\ref{fig:reqflow} shows an overview of the request flow through
the \rhythm pipeline.  The ovals represent execution on the host
and the boxes represent execution on the accelerator.
The event-based server on the CPU copies request information into buffers on the device
(step 1).  When a sufficient number of
requests arrive or the oldest request reaches a preset timeout, the
parser is launched (steps 2-3) to identify and sort requests
so that requests of the same type (PHP file) are
contiguous in memory.  The next stage (step 4) 
dispatches cohorts (and possibly processes some requests on the host CPU).
Subsequent stages in the server include accessing backend storage services (step 7), handling
backend responses (steps 8-10), and generating final HTML responses
(steps 11-13).

\subsection{CUDA Specifics}
\label{sec:rhythmcuda}
Our \rhythm prototype is implemented using CUDA on NVIDIA GPUs.
A \emph{stream} is defined as a sequence of dependent 
requests (memory copies or kernels) to the device, and different 
streams can execute concurrently on the device. We use asynchronous 
streams to implement the parser, the various process stages and the response stage of 
the \rhythm pipeline.  Memory pools are created at startup to 
avoid allocation and synchronization overheads, and memory is 
recycled.  We use atomics to perform lock-free insertion and deletion 
into the session and cohort pools.  We also perform several 
optimizations to \rhythm based on CUDA features, including:

\begin{itemize}
\item For padding in HTML content we perform a max butterfly reduction across 
a warp that uses CUDA shared memory to calculate the padding 
amount for each thread.
\item We use CUDA constant memory where possible to store static HTML 
content for pages.
\item We store frequently used pointers in CUDA constant memory instead 
of local memory to optimize register usage and enable more inflight
\rhythm cohorts.
\end{itemize}

\rhythm is designed from the ground up keeping high throughput in 
mind, ideally with request arrival rate as the only limiter. Our
implementation is guided by our experience with SPECWeb Banking, and
there are nearly endless opportunities for continued optimization.  We evaluate 
the benefits and pitfalls of \rhythm on existing platforms, and 
explore its potential to serve as a guideline for future server 
architectures.