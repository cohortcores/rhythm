\section{Evaluation}
\label{sec:eval}
\begin{figure*}[ht]
  \centering
\subfloat[Wall Power] {
 \includegraphics[width=0.45\textwidth]{figs/wallpower.pdf}
}
\subfloat[Dynamic Power] {
  \includegraphics[width=0.45\textwidth]{figs/dynapower.pdf}
}
\caption{Throughput-Efficiency for Wall Power (a) and Dynamic Power
  (b). Throughput (y-axis) is normalized to Core i7 8 workers and
  efficiency (x-axis) to ARM A9 2 workers. The shaded region
  represents the desired operating range.}
  \label{fig:finaleff}
\end{figure*}
\begin{table*}[t]
\small
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
\multirow{2}{*}{Platform} & \multicolumn{3}{c|}{Power (Watts)} & Latency & Throughput & \multicolumn{2}{c|}{Reqs/Joule (Efficiency)}\\ 
\cline{2-4}
\cline{7-8}
 & Idle & Wall & Dynamic & (ms) & (KReqs/s) & Wall & Dynamic\\ 
\hline
\hline
Core i5 1 worker & 47 & 67 & 20 & 0.016 & 75 & 972 & 3283\\
\hline
Core i5 4 workers & 47 & 98 & 51 & 0.016 & 282 & 2447 & 4712\\
\hline  
Core i7 4 workers & 45 & 147 & 102 & 0.014 & 331 & 1901 & 2735\\
\hline  
Core i7 8 workers & 45 & 156 & 111 & 0.014 & 377 & 2042 & 2873\\
\hline  
ARM a9 1 worker & 2 & 3.4 & 1.4 & 0.176 & 8 & 1672 & 4061\\
\hline  
ARM a9 2 workers & 2 & 4.5 & 2.5 & 0.176 & 16 & 2683 & 4830\\
\hline
Titan A & 74 & 226 & 152 & 86 & 398 & 1469 & 2193\\
\hline
Titan B & 74 & 306 & 232 & 24 & 1535 & 3329 & 4410\\
\hline  
Titan C & 74 & 285 & 211 & 10* & 3082 & 9070 & 12264\\
\hline
\end{tabular}
\caption{SPECWeb Banking Experimental Results. *Titan C Latency is for transposed response}
\label{tbl:finalresults}
\end{table*}
\rhythm is designed as a free-flowing pipeline that serves to 
maximally utilize the underlying accelerator hardware to achieve 
optimal efficiency.  This section presents our experimental results that confirm the
expected limitations of current system bottlenecks on throughput.  We
then show how removing these bottlenecks enables \rhythm on a GPU to
operate at high throughput with high efficiency. We also demonstrate that
replicating general purpose cores to achieve high throughput cannot match the
efficiency of \rhythm on today's GPUs.  However, we caution that our
results represent an initial exploration of the overall server design space
primarily to gauge where \rhythm sits with respect to potential
alternative platforms.  A more comprehensive study, which is beyond the
scope of this paper, would account for many differences such as
technology node, different accelerators, etc. and include additional workloads.  Nonetheless, our results point towards
the design of future data parallel accelerators specialized for
server workloads.

\subsection{Overall Results}

Table~\ref{tbl:finalresults} shows the 
throughput, latency, wall and dynamic power of the banking workload for 
our various modeled platforms. More worker threads is always 
more beneficial for the general purpose cores since it amortizes 
the fixed costs associated with powering on the chip. For our 
evaluation we only consider the higher worker threads since 
they represent the best operating point for each platform.

To gain better insight into the results we utilize a
throughput-efficiency plot that normalizes throughput to the Core i7 eight
worker threads and efficiency to the ARM A9 two worker threads. Figure~\ref{fig:finaleff} shows the
throughput-efficiency for the various platforms.
Considering both wall power(Figure~\ref{fig:finaleff}a) and dynamic power (Figure~\ref{fig:finaleff}b), 
the Core i5 is more power efficient than the i7, with efficiencies comparable 
to the ARM, while delivering 75\% of the i7's throughput. On the other hand, 
the ARM achieves only 4\% of the i7's throughput, and 6\% of the 
i5's throughput. The results also show that Titan A performs poorly in 
terms of efficiency, and provides only marginal throughput
improvements. In contrast, Titan C provides massive gains in 
both throughput and power efficiency, processing $\sim1.5\times$ 
more requests per Joule compared to the ARM and 
delivering $7\times$ more requests per second compared 
to the Core i7. Titan B provides more than $4\times$ 
the throughput of the i7, though at 91\% dynamic efficiency and 
124\% wall efficiency of the ARM.

In terms of latency, the Core i7 and i5 both exhibit low response latency, and 
even the ARM chip manages latencies of a few hundred 
microseconds. Titan A exhibits high response latencies, rendering it 
unusable for real server applications. Titan B and C perform relatively 
well, with latencies in 10s of milliseconds. These latencies are
tolerable~\cite{Dean:2013:TS:2408776.2408794}, 
and expected, since \rhythm sacrifices latency to achieve massive gains 
in throughput and efficiency. We now evaluate the nature of each Titan 
platform and their potential as exposed by \emph{Rhythm}.

\subsubsection{Titan A: Emulated Remote Backend}
\begin{figure} [htb]
  \centering
  \includegraphics[width=0.45\textwidth]{figs/titanAPCI.pdf}
  \caption{PCIe 3.0 Limitations in Titan A for various request types}
  \label{fig:titanAPCI}
\end{figure}
Running with an emulated remote backend involves copies over the PCI
Express bus for requests to, and responses from the backend. On current
platforms, this limits overall throughput. \rhythm transfers 1KB for
the request buffers, 1KB for the backend request, 4KB for the backend
response and 26.4KB on an average for the response over the PCIe
bus. We can calculate the throughput bound of the PCIe 3.0 bus by
taking the ratio of the peak bandwidth (12GB/s) and the data
transferred per request. Figure~\ref{fig:titanAPCI} shows the achieved
throughput and throughput bounded by the available PCIe 3.0 bandwidth
for the different request types. We can see that all requests achieve 
throughputs ranging from 83\% to 95\% of the PCI bounds. The minor difference between
the two is expected since we transfer data in smaller
chunks, which does not allow us to reach peak PCIe bandwidth. We can see
that \rhythm on Titan A is primarily limited by the PCIe 3.0 bandwidth,
which creates a structural hazard in the \rhythm pipeline, leading to
stalls and a loss in power efficiency.

A potential enabling trend is the PCIe 4.0 standard, which doubles
usable bandwidth to 24 GB/s. This could increase Titan A's throughput to 864K
reqs/s and a commensurate increase in efficiency that may bring it
near the ARM A9's efficiency (depending on the PCIe 4.0 power).
However, even at 25 GB/s, the PCIe bus is still a bottleneck for
{\em Rhythm} on a Titan.

\subsubsection{Titan B: Integrated NIC and Device Backend}

Titan B increases {\em Rhythm}'s average throughput to more than $4\times$ that 
of the core i7 achieving more than 1.5M reqs/sec, at 91\% dynamic 
efficiency and 124\% wall efficiency of the ARM chip. 
\begin{figure} [ht]
  \centering
  \includegraphics[width=0.45\textwidth]{figs/titanBTPWcomp.pdf}
  \caption{Throughput-Efficiency for different request types on Titan
    B (dynamic power). \rhythm buffer sizes that are close to required sizes
    perform well (shaded area represents the desired
    operating range).}
  \label{fig:titanBTPWcomp}
\end{figure}

We note that Titan B's throughput and efficiency could improve if we
used a better response padding method. Many request types incur
significant overhead due to excessive padding since their total
response size is just beyond one power of two and we simply round up
to the next power of two.  This introduces exponentially more overhead
for transposes for larger response sizes. Cross-referencing response sizes 
in Table~\ref{tbl:specwebdistr} 
with dynamic efficiency in Figure~\ref{fig:titanBTPWcomp} for different requests, 
we observe that for small responses (i.e., \emph{login}) or where
\rhythm uses a response buffer close to the size of the original
response size (e.g., \emph{change\_profile} and \emph{transfer}), Titan B achieves
throughput $3.5\times-5\times$ higher than the core i7, with dynamic
efficiencies of 105\% to 120\% of the ARM, showing room for further
optimization. We further analyze this difference and observe that the 
response transpose takes up a significant fraction of device time, 
and creates bubbles in the \rhythm pipeline. Titan C removes this limitation.

\subsubsection{Titan C: Increasing Device Utilization}
Titan C tries to push the \rhythm pipeline to its limits by 
increasing GPU utilization and offloading the response transpose. With these
optimizations, Titan C achieves more than 3M reqs/sec on an average,
or more than $8\times$ the throughput of the Core i7. Increasing GPU 
utilization amortizes the fixed power overheads, achieving a
dynamic efficiency of more than $2.5\times$ that of the ARM, and a
wall efficiency of more than $3.3\times$. These numbers ignore power 
to perform the transpose, and as we show in the next section, with an ample power 
budget for the transpose, \rhythm can still outperform the ARM in 
efficiency while achieving far greater throughputs.

\subsection{Scaling Many Core Processors}
A natural comparison for achieving high throughput is to scale the
number of general purpose cores to match the throughput of \rhythm on
both Titan B and Titan C.  We use single thread throughput for the
general purpose cores and idealistically assume linear performance
scaling.  The scaled systems incur additional {\em uncore} overhead to
support scaling, such as additional chip I/Os, on chip interconnect,
additional memory controllers and memory, etc. We use the i5 for scaling 
instead of the i7 due to its higher dynamic efficiency (Table~\ref{tbl:finalresults}). 
Based on our measurements, and
other studies~\cite{Li:2011:SIS}, we assume a dynamic power of 1W per ARM core and 10W
per i5 core.  The power available for the {\em uncore} overhead is the
difference between the idealized scaled system's power and the Titan
platform's power.

With respect to Titan B (dynamic power), we need 192 ARM cores and 21
i5 cores to match throughput, requiring 192W and 210W, respectively.
Titan B uses 232W, leaving only 40W for the ARM (21\%) or 22W (10\%)
for the i5 available for uncore scaling overhead.
Compared to Titan C, we need 385 ARM cores and 41 i5 cores to match
throughput.  This results in 385W for the ARM system and 410W for the
i5 system, and Titan C has more than 170W in which to implement the
transpose operation and still outperform the scaled systems.

Given that the Titan-based systems also have room to
reduce overall power consumption (e.g., lower power DRAM, eliminate
GPU specific features, etc.) it appears
difficult for simple replicated designs to match the overall throughput and
throughput/watt of \rhythm on GPU-style accelerators.  However, a more
detailed analysis of complete server design, including specialized
data parallel accelerators, is required, and we leave
that to future work.

\subsection{System Resource Requirements}
\paragraph{Network Bandwidth}
We perform our experiments on \rhythm for a request size of 512B, a backend 
request size of 1KB and a 4KB backend response size. Using the average value of 
the response size of the SPECWeb Banking workload (Table~\ref{tbl:specwebdistr}),
and the average number of backend responses, we can easily calculate the 
network bandwidth requirements of our Titan platforms. At an average throughput of 
398K reqs/s, Titan A requires a N/W bandwidth of 67 Gbps, Titan B requires 258 Gbps 
and Titan C requires 517 Gbps. All of these numbers assume raw uncompressed data. 
Most modern web browsers support compression and research has demonstrated more 
than 80\% compression of HTML content in pages for popular 
websites~\cite{httpcompression}. A compression ratio of ~80\% means that 
Titan C can easily be operated on a 100Gbps link, already defined by the 
IEEE 802.3bj standard~\cite{ieee802.3}.

\paragraph{Memory Capacity}
We are currently limited by the memory on the device. For our experiments, 
we emulate 16M active sessions on the GPU, and at 40B per session, 
this requires 640MB of memory. The session array is implemented as
a hash table using random insertion, and we allocate memory for 64M sessions 
to reduce the chance of a collision to 25\%, but this requires 
2.5GB of device memory. Since all memory is preallocated in pools, we also 
need to allocate enough memory for the process phase, backend data,
request buffers, response buffers and transpose buffers to avoid 
structural hazards in the pipeline. The memory 
required for buffers increases linearly with cohort size, therefore, 
we are limited to 8 cohorts in flight of size 4096 requests each on the 
GTX Titan.

\subsection{Miscellaneous}
\paragraph{Cohort Size sensitivity}
We performed experiments on \rhythm for cohort sizes ranging from 
256 to 8192, and found 
4096 to provide the right balance between 
high throughput and memory limitations. Larger cohort sizes are 
better for throughput since they allow more work to be launched on the GPU, 
however, they require more memory. Larger cohort sizes also 
impact response latency, since it takes more time to form a cohort. However, 
for arrival rates of the order of a million reqs/sec, cohort formation 
times are negligible.

\paragraph{Parser divergence}
Our current experiments run the same type for requests on the parser, 
however, this reduces control divergence 
in the parser. In a real world system, multiple request types would 
arrive at the parser, increasing control divergence and reducing 
single parser throughput. We measured parser latency for a real Specweb 
Banking Trace containing a mix of requests and images. On an average, 
the parser takes 556us including the request buffer tranpose, giving a 
throughput of 7.4M reqs/sec for a cohort size of 4096. Therefore, the parser is fast enough even 
when it is processing cohorts of different types. The \rhythm design 
also allows for multiple parsers to be launched concurrently, and for higher 
throughputs, this would further help in hiding parser latency.

\paragraph{HyperQ}
We performed our experiments on a NVIDIA GTX690 as well, however, a single 
work queue between the host and device created false dependencies among process 
kernels, limiting throughput. The GTX Titan supports 32 simultaneous work queues (HyperQ), 
allowing for much higher throughput and GPU utilization.
\rhythm can expose significant concurrency and the hardware must be 
capable of exploiting it. Emulating future platforms with integrated
high bandwidth devices exposed 
the benefit of having HyperQ scheduling on the GPU.
