#if !defined(UTILS_H_INCLUDED)

#define UTILS_H_INCLUDED

#include <stddef.h>

DEVICE int d_strcasecmp(const char *s1, const char *s2);
DEVICE int d_strncmp(const char *s1, const char *s2, size_t n);
DEVICE char* d_strcpy(char* s1, const char* s2);
DEVICE size_t d_strlen(const char *s);
DEVICE char* d_itoa(int value, char* result, int base);
DEVICE char* d_strchr(const char *s, int c);
DEVICE char* d_strstr(const char *haystack, const char *needle);
DEVICE char* d_strtok_r(char *s, const char *delimiters, char **lasts);
DEVICE size_t d_strcspn(const char *s1, const char *s2);
DEVICE size_t d_strspn(const char *s1, const char *s2);
DEVICE char* d_strncpy(char* s1, const char* s2, size_t n);
DEVICE int d_atoi(const char *s);
#endif
