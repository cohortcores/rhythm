#ifndef LIST_C_INCLUDED
#define LIST_C_INCLUDED

typedef struct lnode {
 struct lnode* next;
 void* data;
 long enqueue_time;
}ln;

typedef struct {
 struct lnode* head;
 int count;
} list;

void l_insert(list* l, void* data)
{
 struct lnode* p;
 assert(l != NULL);
 p = (struct lnode*)malloc(sizeof(struct lnode));
 p->data = data;
 p->next = l->head;
 l->head = p;
 l->count++;
}

void* l_remove(list* l, void* data)
{
 assert(l != NULL);
 struct lnode* current = l->head;
 struct lnode* before_current = NULL;
 void* ret_data = NULL;
 while(current) {
  if(current->data == data) {
   if(current == l->head) {
    l->head = l->head->next;
   } else {
    before_current->next = current->next;
   }
   ret_data = data;
   free(current);
   l->count--;
   break;
  }
  before_current = current;
  current = current->next;
 }
 return ret_data;
}

int l_count(list* l)
{
 assert(l != NULL);
 return l->count;
}


void l_init(list* l)
{
 assert(l != NULL);
 l->head = NULL;
 l->count = 0;
}

#endif