#ifndef UTILS_C_INCLUDED
#define UTILS_C_INCLUDED

/*
 * This array is designed for mapping upper and lower case letter together for
 * a case independent comparison. The mappings are based upon ASCII character
 * sequences.
 */

DEVICE unsigned char charmap[] = {
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
    0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
    0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
    0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
    0x40, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,
    0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f,
    0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,
    0x78, 0x79, 0x7a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
    0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,
    0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f,
    0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,
    0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f,
    0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87,
    0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
    0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97,
    0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
    0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7,
    0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
    0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7,
    0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
    0xc0, 0xe1, 0xe2, 0xe3, 0xe4, 0xc5, 0xe6, 0xe7,
    0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
    0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7,
    0xf8, 0xf9, 0xfa, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,
    0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7,
    0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
    0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7,
    0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff,
};

DEVICE int d_strcasecmp(const char *s1, const char *s2)
{
  unsigned char u1, u2;
  for ( ; ; s1++, s2++) {
    u1 = (unsigned char) *s1;
    u2 = (unsigned char) *s2;
    if ((u1 == '\0') || (charmap[u1] != charmap[u2])) {
      break;
    }
  }
  return charmap[u1] - charmap[u2];
}

DEVICE int d_strncasecmp(const char *s1, const char *s2, size_t n)
{
 unsigned char u1, u2;
 for (; n!=0; --n) {
  u1 = (unsigned char)*s1++;
  u2 = (unsigned char)*s2++;
  if (charmap[u1] != charmap[u2]) {
   return charmap[u1] - charmap[u2];
  }
  if (u1 == '\0') {
   return 0;
  }
 }
 return 0;
}

DEVICE int d_strncasecmp_transposed(const char *s1, const char *s2, size_t n)
{
 unsigned char u1, u2;
 int i = 0;
 for (; n!=0; --n, i++) {
  u1 = (unsigned char)s1[TRANSPOSE(i)];
  u2 = (unsigned char)s2[i];
  if (charmap[u1] != charmap[u2]) {
   return charmap[u1] - charmap[u2];
  }
  if (u1 == '\0') {
   return 0;
  }
 }
 return 0;
}

/* Source: http://en.wikibooks.org/wiki/C_Programming/Strings#The_strncmp_function */
HOST DEVICE int d_strncmp(const char *s1, const char *s2, size_t n)
{
  unsigned char uc1, uc2;
  if (n == 0)
    return 0;
  while (n-- > 0 && *s1 == *s2) {
    if (n == 0 || *s1 == '\0')
      return 0;
    s1++;
    s2++;
  }
  uc1 = (*(unsigned char *) s1);
  uc2 = (*(unsigned char *) s2);
  return ((uc1 < uc2) ? -1 : (uc1 > uc2));
}

HOST DEVICE int d_strncmp_transposed(const char *s1, const char *s2, size_t n)
{
  unsigned char uc1, uc2;
  if (n == 0)
    return 0;
  while (n-- > 0 && *s1 == *s2) {
    if (n == 0 || *s1 == '\0')
      return 0;
    s1 += TRANSPOSE(1);
    s2++;
  }
  uc1 = (*(unsigned char *) s1);
  uc2 = (*(unsigned char *) s2);
  return ((uc1 < uc2) ? -1 : (uc1 > uc2));
}

/* Source: http://en.wikibooks.org/wiki/C_Programming/Strings#The_strcmp_function */
HOST DEVICE int d_strcmp(const char *s1, const char *s2)
{
 unsigned char uc1, uc2;
 while (*s1 != '\0' && *s1 == *s2) {
  s1++;
  s2++;
 }
 uc1 = (*(unsigned char *) s1);
 uc2 = (*(unsigned char *) s2);
 return ((uc1 < uc2) ? -1 : (uc1 > uc2));
}

/* Source: http://en.wikibooks.org/wiki/C_Programming/Strings#The_strcpy_function */
HOST DEVICE char* d_strcpy(char* s1, const char* s2)
{
  char *dst = s1;
  const char *src = s2;
  /* Do the copying in a loop.  */
  while ((*dst++ = *src++) != '\0')
    ;/* The body of this loop is left empty. */
  /* Return the destination string.  */
  return s1;
}

HOST DEVICE char* d_strcpy_transposed_src(char* s1, const char* s2)
{
  char *dst = s1;
  const char *src = s2;
  /* Do the copying in a loop.  */
  while ((*dst++ = *src) != '\0') {
    src += TRANSPOSE(1);
  }  
  /* Return the destination string.  */
  return s1;
}

/* Source: http://en.wikibooks.org/wiki/C_Programming/Strings#The_strlen_function */
HOST DEVICE size_t d_strlen(const char *s)
{
  const char *p = s;
  /* Loop over the data in s.  */
  while (*p != '\0')
    p++;
  return (size_t)(p - s);
}

HOST DEVICE size_t d_strlen_transposed(const char *s)
{
  const char *p = s;
  /* Loop over the data in s.  */
  while (*p != '\0')
    p += TRANSPOSE(1);
  return (size_t)((p - s)/COHORT_SIZE);
}

/**
* C++ version 0.4 char* style "itoa":
* Written by Lukás Chmela
* Released under GPLv3.
*/
HOST DEVICE char* d_itoa(int value, char* result, int base, int* len) 
{
  if (base < 2 || base > 36) { 
    *result = '\0'; 
    return result; 
  }
	
  char* ptr = result, *ptr1 = result, tmp_char;
  int tmp_value;
  int tlen = 0;
  do {
    tmp_value = value;
    value /= base;
    *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	tlen++;
  } while ( value );
	
  // Apply negative sign
  if (tmp_value < 0) {
    *ptr++ = '-';
	tlen++;
  }
  *ptr-- = '\0';
  while(ptr1 < ptr) {
    tmp_char = *ptr;
    *ptr--= *ptr1;
    *ptr1++ = tmp_char;
  }
  if(len) {
   *len = tlen;
  }
  return result;
}

/* Source: http://en.wikibooks.org/wiki/C_Programming/Strings#The_strchr_function */
HOST DEVICE char* d_strchr(const char *s, int c)
{
  /* Scan s for the character.  When this loop is finished,
  s will either point to the end of the string or the
  character we were looking for.  */
  while (*s != '\0' && *s != (char)c)
    s++;
  return ( (*s == c) ? (char *) s : NULL );
}

HOST DEVICE char* d_strchr_transposed(const char *s, int c)
{
  /* Scan s for the character.  When this loop is finished,
  s will either point to the end of the string or the
  character we were looking for.  */
  while (*s != '\0' && *s != (char)c)
    s += TRANSPOSE(1);
  return ( (*s == c) ? (char *) s : NULL );
}

/* Source: http://en.wikibooks.org/wiki/C_Programming/Strings#The_strstr_function */
HOST DEVICE char* d_strstr(const char *haystack, const char *needle)
{
  size_t needlelen;
  /* Check for the null needle case.  */
  if (*needle == '\0')
    return (char *) haystack;
  needlelen = d_strlen(needle);
  for (; (haystack = d_strchr(haystack, *needle)) != NULL; haystack++) {
    if (d_strncmp(haystack, needle, needlelen) == 0)
      return (char*)haystack;
  }
  return NULL;
}

HOST DEVICE char* d_strstr_transposed(const char *haystack, const char *needle)
{
  size_t needlelen;
  /* Check for the null needle case.  */
  if (*needle == '\0')
    return (char *) haystack;
  needlelen = d_strlen(needle);
  for (; (haystack = d_strchr_transposed(haystack, *needle)) != NULL; haystack += TRANSPOSE(1)) {
    if (d_strncmp_transposed(haystack, needle, needlelen) == 0)
      return (char*)haystack;
  }
  return NULL;
}

HOST DEVICE size_t d_strcspn(const char *s1, const char *s2)
{
  const char *sc1;
  for (sc1 = s1; *sc1 != '\0'; sc1++)
    if (d_strchr(s2, *sc1) != NULL)
      return (sc1 - s1);
  return sc1 - s1;            /* terminating nulls match */
}

HOST DEVICE size_t d_strcspn_transposed(const char *s1, const char *s2)
{
  const char *sc1, *sc2;
  for (sc1 = s1, sc2 = s1; *sc1 != '\0'; sc1+=TRANSPOSE(1), sc2++)
    if (d_strchr(s2, *sc1) != NULL)
      return (sc2 - s1);
  return sc2 - s1;            /* terminating nulls match */
}

HOST DEVICE size_t d_strspn(const char *s1, const char *s2)
{
  const char *sc1;
  for (sc1 = s1; *sc1 != '\0'; sc1++)
    if (d_strchr(s2, *sc1) == NULL)
      return (sc1 - s1);
  return sc1 - s1;            /* terminating nulls don't match */
}

HOST DEVICE size_t d_strspn_transposed(const char *s1, const char *s2)
{
  const char *sc1, *sc2;
  for (sc1 = s1, sc2 = s1; *sc1 != '\0'; sc1+=TRANSPOSE(1), sc2++)
    if (d_strchr(s2, *sc1) == NULL)
      return (sc2 - s1);
  return sc2 - s1;            /* terminating nulls don't match */
}
 
/* Source: http://en.wikibooks.org/wiki/C_Programming/Strings#The_strtok_function */
HOST DEVICE char* d_strtok_r(char *s, const char *delimiters, char **lasts)
{
  char *sbegin, *send;
  sbegin = s ? s : *lasts;
  sbegin += d_strspn(sbegin, delimiters);
  if (*sbegin == '\0') {
    *lasts = "";
    return NULL;
  }
  send = sbegin + d_strcspn(sbegin, delimiters);
  if (*send != '\0')
    *send++ = '\0';
  *lasts = send;
  return sbegin;
}

HOST DEVICE char* d_strtok_r_transposed(char *s, const char *delimiters, char **lasts)
{
  char *sbegin, *send;
  sbegin = s ? s : *lasts;
  sbegin += TRANSPOSE(d_strspn_transposed(sbegin, delimiters));
  if (*sbegin == '\0') {
    *lasts = "";
    return NULL;
  }
  send = sbegin + TRANSPOSE(d_strcspn_transposed(sbegin, delimiters));
  if (*send != '\0') {
    *send = '\0';
    send += TRANSPOSE(1);
  }
  *lasts = send;
  return sbegin;
}

HOST DEVICE char* d_strncpy(char* s1, const char* s2, size_t n)
{
  char *dst = s1;
  const char *src = s2;
  /* Copy bytes, one at a time.  */
  while (n > 0) {
    n--;
    if ((*dst++ = *src++) == '\0') {
      /* If we get here, we found a null character at the end
      of s2, so use memset to put null bytes at the end of
      s1.  */
      memset(dst, '\0', n);
      break;
    }
  }
  return s1;
}

HOST DEVICE int d_atoi(const char *s)
{
  const char digits[] = "0123456789";  /* legal digits in order */
  unsigned val=0;         /* value we're accumulating */
  int neg=0;              /* set to true if we see a minus sign */

  while (*s==' ' || *s=='\t') {
    s++;
  }
  if (*s=='-') {
    neg=1;
    s++;
  }
  else if (*s=='+') {
    s++;
  }
  while (*s) {
    const char *where;
    unsigned digit;
    where = d_strchr(digits, *s);
    if (where==NULL) {
      break;
    }
    digit = (where - digits);
    val = val*10 + digit;
    s++;
  }
  if (neg) {
    return -val;
  }
  return val;
}

HOST DEVICE int d_atoi_transposed(const char *s)
{
 const char digits[] = "0123456789";  /* legal digits in order */
 unsigned val=0;         /* value we're accumulating */
 int neg=0;              /* set to true if we see a minus sign */
 int i = 0;

 while(s[TRANSPOSE(i)]==' ' || s[TRANSPOSE(i)]=='\t') {
  i++;
 }
 if(s[TRANSPOSE(i)]=='-') {
  neg=1;
  i++;
 } else if(s[TRANSPOSE(i)]=='+') {
  i++;
 }
 while(s[TRANSPOSE(i)]) {
  const char *where;
  unsigned digit;
  where = d_strchr(digits, s[TRANSPOSE(i)]);
  if(where==NULL) {
   break;
  }
  digit = (where - digits);
  val = val*10 + digit;
  i++;
 }
 if (neg) {
  return -val;
 }
 return val;
}

HOST DEVICE int d_isdigit(int c)
{
 return (c <= '9' && c >= '0');
}

/* Source : http://www6.uniovi.es/python/dev/src/c/html/atof_8c-source.html */
HOST DEVICE double d_atof(char* s)
{
 double a = 0.0;
 int e = 0;
 int c;
 while ((c = *s++) != '\0' && d_isdigit(c)) {
  a = a*10.0 + (c - '0');
 }
 if (c == '.') {
  while ((c = *s++) != '\0' && d_isdigit(c)) {
   a = a*10.0 + (c - '0');
   e = e-1;
  }
 }
 if (c == 'e' || c == 'E') {
  int sign = 1;
  int i = 0;
  c = *s++;
  if (c == '+')
   c = *s++;
  else if (c == '-') {
   c = *s++;
  sign = -1;
  }
  while (d_isdigit(c)) {
   i = i*10 + (c - '0');
   c = *s++;
  }
  e += i*sign;
 }
 while (e > 0) {
  a *= 10.0;
  e--;
 }
 while (e < 0) {
  a *= 0.1;
  e++;
 }
 return a;
}

HOST DEVICE char* d_strrchr(const char *cp, int ch)
{
 char *save;
 char c;
 for(save = (char*)0; (c = *cp); cp++) {
  if(c==ch) {
   save = (char*)cp;
  }
 }
 return save;
}

HOST DEVICE inline int d_isspace(char c)
{
    return (c == ' ' || c == '\t' || c == '\n' || c == '\12');
}
#endif
