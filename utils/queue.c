#ifndef QUEUE_C_INCLUDED
#define QUEUE_C_INCLUDED

struct qnode {
 struct qnode* next;
 void* data;
 long enqueue_time;
}qn;

typedef struct {
 struct qnode* front;
 struct qnode* rear;
 int count;
 pthread_mutex_t mutex;
} queue;

int q_enqueue(queue* q, void* data)
{
 struct qnode *p;
 struct timespec time;
 assert(q != NULL);
 clock_gettime(CLOCK_REALTIME, &time);
 p = (struct qnode*)malloc(sizeof(struct qnode));
 p->data = data;
 p->next = NULL;
 p->enqueue_time = time.tv_sec*1000000 + (time.tv_nsec/1000);
 if(q->rear == NULL || q->front == NULL) {
  q->front = p;
 } else {
  q->rear->next = p;
 }
 q->rear = p;
 q->count++;
 return q->count;
}

void* q_dequeue(queue* q)
{
 struct qnode *p;
 void* data;
 assert(q != NULL);
 if(q->front == NULL) {
  return NULL;
 } else {
  p = q->front;
  data = p->data;
  q->front = q->front->next;
  free(p);
 }
 q->count--;
 return data;
}

int q_count(queue* q)
{
 assert(q != NULL);
 return q->count;
}

void* q_peek_front(queue* q)
{
 assert(q != NULL);
 assert(q->front != NULL);
 return q->front->data;
}

void* q_peek_rear(queue* q)
{
 assert(q != NULL);
 assert(q->rear != NULL);
 return q->rear->data;
}

void q_lock(queue* q)
{
 pthread_mutex_lock(&q->mutex);
}

void q_unlock(queue* q)
{
 pthread_mutex_unlock(&q->mutex);
}

void q_init(queue* q)
{
 assert(q != NULL);
 q->front = NULL;
 q->rear = NULL;
 q->count = 0;
 pthread_mutex_init(&q->mutex, NULL);
}

#endif
