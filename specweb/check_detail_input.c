#ifndef CHECK_DETAIL_INPUT_C_INCLUDED
#define CHECK_DETAIL_INPUT_C_INCLUDED

DEVICE char* check_detail_input_html(char* response, int* response_len, char* userid)
{
 char* buffer_iter = response;
 int diff;
 int laneId = threadIdx.x & 0x1f;
 int warpId = threadIdx.x & 0xffffffe0;
 SHARED int tile[THREAD_BLOCK_SIZE];
 
 APPEND_TO_BUFFER_TRANSPOSED(check_detail_input_str1, len_check_detail_input_str1)
 APPEND_TO_BUFFER_TRANSPOSED(userid, d_strlen(userid))
 APPEND_TO_BUFFER_TRANSPOSED("       <ul>\r\n", 13)
 APPEND_WHITESPACE_PADDING()
 APPEND_TO_BUFFER_TRANSPOSED(check_detail_input_str2, len_check_detail_input_str2)
 /* Terminate the string */
 *buffer_iter = 0;
 *response_len = (int)((buffer_iter - response)/COHORT_SIZE);
 return buffer_iter;
}

int _check_detail_input_php_cb_1(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  response_add(r->resp);
  cohort_free(r->cohort_id);
  request_handler_free(r);
  return false;
 }
 return true;
}

GLOBAL void check_detail_input_php_1(int* req_state, char* resp, int* resp_len, int cohort_id, session_bucket* session_array, int* fds)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 fds[tid] = r->client;
 char scratch[10];
 char* buffer_iter = resp + tid;
 char* content_len_iter = NULL;
 int response_len = 0;
 int len = 0;
 
 if(userid == NULL) {
  login_html(resp + tid, resp_len + tid, "Please login", 12);
  req_state[tid] = REQ_STATE_ERROR;
  return;
 }
 APPEND_TO_BUFFER_TRANSPOSED(response_header_str, len_response_header_str)
 APPEND_TO_BUFFER_TRANSPOSED("Content-Length: ", 16)
 content_len_iter = buffer_iter;
 APPEND_TO_BUFFER_TRANSPOSED("          ", 10)
 APPEND_TO_BUFFER_TRANSPOSED("\r\n\r\n", 4)
 buffer_iter = check_detail_input_html(buffer_iter, &response_len, userid);
 resp_len[tid] = (int)((buffer_iter - resp)/COHORT_SIZE);
 /* Insert content len here padded by whitespaces */
 buffer_iter = content_len_iter;
 d_itoa(response_len, scratch, 10, &len);
 APPEND_TO_BUFFER_TRANSPOSED(scratch, len)
 req_state[tid] = REQ_STATE_CONTINUE; 
}

void check_detail_input_php(request_handler* r)
{
 check_detail_input_php_1<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->cohort_id, r->d_session_array, r->resp->d_fds);
 callbacks_add(_check_detail_input_php_cb_1, r);
}

#endif
