#ifndef POST_PAYEE_C_INCLUDED
#define POST_PAYEE_C_INCLUDED

#include "backend/backend.c"

CONSTANT char* post_payee_padding;

DEVICE void _get_post_payee_request(char* request, char* userid, char* payee_id, char* name, char* street, char* city, char* zip, char* state, char* phone)
{
 char* buffer_iter = request;
 unsigned char buf[128];
 APPEND_TO_BUFFER_TRANSPOSED("GET /fcgi-bin/besim_fcgi.fcgi?1&6&")
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(payee_id, d_strlen(payee_id), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(name, d_strlen(name), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(street, d_strlen(street), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(city, d_strlen(city), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(state, d_strlen(state), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(zip, d_strlen(zip), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(phone, d_strlen(phone), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED(" HTTP/1.1\r\nHost: localhost\r\n\r\n")
 *buffer_iter = 0;
}

DEVICE char* post_payee_html(char* response, int* response_len, char* userid, char* be_resp, char* payee_id)
{
 char* buffer_iter = response;
 int diff;
 int laneId = threadIdx.x & 0x1f;
 int warpId = threadIdx.x & 0xffffffe0;
 SHARED int tile[THREAD_BLOCK_SIZE];
 char *ssave1 = "", *p1;
 
 APPEND_TO_BUFFER_TRANSPOSED(post_payee_str1)
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED(post_payee_str2)
 APPEND_TO_BUFFER_TRANSPOSED("Payee ")
 APPEND_TO_BUFFER_TRANSPOSED(payee_id)
 APPEND_TO_BUFFER_TRANSPOSED(" succesfully added")
 APPEND_TO_BUFFER_TRANSPOSED(post_payee_str3)
 p1 = d_strtok_r_transposed(be_resp, "\n", &ssave1);
 APPEND_TO_BUFFER_TRANSPOSED_2(p1)
 APPEND_TO_BUFFER_TRANSPOSED("</td></tr>\r\n")
 APPEND_WHITESPACE_PADDING()
 APPEND_TO_BUFFER_TRANSPOSED(post_payee_str4)
 APPEND_TO_BUFFER_TRANSPOSED(post_payee_padding)
 APPEND_TO_BUFFER_TRANSPOSED(post_payee_str5)
 /* Terminate the string */
 *buffer_iter = 0;
 *response_len = (int)((buffer_iter - response)/COHORT_SIZE);
 return buffer_iter;
}

int _post_payee_php_cb_2(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  response_add(r->resp);
  cohort_free(r->cohort_id);
  request_handler_free(r);
  return false;
 }
 return true;
}

GLOBAL void post_payee_php_2(char* be_resp, int* req_state, char* resp, int* resp_len, session_bucket* session_array, int cohort_id)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char payee_id[32];
 char* userid = session_find(session_array, r->session_id);
 char scratch[10];
 char* buffer_iter = resp + tid;
 char* content_len_iter = NULL;
 int response_len = 0;
 
 if(req_state[tid] != REQ_STATE_CONTINUE) {
  return;
 }
 APPEND_TO_BUFFER_TRANSPOSED(response_header_str)
 APPEND_TO_BUFFER_TRANSPOSED("Content-Length: ")
 content_len_iter = buffer_iter;
 APPEND_TO_BUFFER_TRANSPOSED("          ")
 APPEND_TO_BUFFER_TRANSPOSED("\r\n\r\n")
 get_value(r, "payee_id", 8, payee_id);
 buffer_iter = post_payee_html(buffer_iter, &response_len, userid, process_backend_response_transposed(be_resp + tid, NULL), payee_id);
 resp_len[tid] = (int)((buffer_iter - resp)/COHORT_SIZE);
 /* Insert content len here padded by whitespaces */
 buffer_iter = content_len_iter;
 d_itoa(response_len, scratch, 10, NULL);
 APPEND_TO_BUFFER_TRANSPOSED(scratch)
 req_state[tid] = REQ_STATE_CONTINUE;
}

void _post_payee_php_cb_1_continue(void* data)
{
 request_handler* r = (request_handler*)data;
 backend_copy_response_to_device(r->be, r->stream);
 post_payee_php_2<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_resp, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->d_session_array, r->cohort_id);
 callbacks_add(_post_payee_php_cb_2, r);
}


int _post_payee_php_cb_1(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  backend_process(r, _post_payee_php_cb_1_continue);
  return false;
 }
 return true;
}

GLOBAL void post_payee_php_1(char* be_req, int* req_state, char* resp, int* resp_len, int cohort_id, session_bucket* session_array, int* fds)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 char payee_id[32];
 char name[32];
 char street[32];
 char city[32];
 char zip[32];
 char state[32];
 char phone[32];
 fds[tid] = r->client;
 
 if(userid == NULL) {
  login_html(resp + tid, resp_len + tid, "Please login");
  req_state[tid] = REQ_STATE_ERROR;
 } else if(!get_value(r, "payee_id", 8, payee_id) || !get_value(r, "name", 4, name) ||
    !get_value(r, "street", 6, street) || !get_value(r, "city", 4, city) ||
    !get_value(r, "zip", 3, zip) || !get_value(r, "state", 5, state) || !get_value(r, "phone", 5, phone)) {
  message_html(resp + tid, resp_len + tid, "No fields should be left blank");
  req_state[tid] = REQ_STATE_ERROR;
 } else if(!d_strlen(payee_id) || !d_strlen(name) ||
    !d_strlen(street) || !d_strlen(city) ||
    !d_strlen(zip) || !d_strlen(state) || !d_strlen(phone)) {
  message_html(resp + tid, resp_len + tid, "No fields should be left blank");
  req_state[tid] = REQ_STATE_ERROR;
 } else {
  _get_post_payee_request(be_req + tid, userid, payee_id, name, street, city, zip, state, phone);
  req_state[tid] = REQ_STATE_CONTINUE;
 }
}

void post_payee_php(request_handler* r)
{
 post_payee_php_1<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_req, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->cohort_id, r->d_session_array, r->resp->d_fds);
 backend_copy_requests_to_host(r->be, r->stream);
 callbacks_add(_post_payee_php_cb_1, r);
}

void post_payee_php_once()
{
 GENERATE_PADDING(post_payee)
}

#endif