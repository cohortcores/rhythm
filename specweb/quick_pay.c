#ifndef QUICK_PAY_C_INCLUDED
#define QUICK_PAY_C_INCLUDED

#include "backend/backend.c"

#define MAX_PAYEES MAX_VALUES
#define MSG_LEN 1024

typedef struct {
 backend_data* backend;
 char* msg;
 int* msg_len;
} quick_pay_php_private;

DEVICE char* quick_pay_padding;
DEVICE long int quick_pay_padding_len = 0;

DEVICE void _get_quick_pay_request(char* request, char* userid, char* id, char* date, char* amount, long int* request_len)
{
 char* buffer_iter = request;
 APPEND_TO_BUFFER("GET /fcgi-bin/besim_fcgi.fcgi?1&7&", 34)
 APPEND_TO_BUFFER(userid, d_strlen(userid))
 APPEND_TO_BUFFER("&", 1)
 APPEND_TO_BUFFER(id, d_strlen(id))
 APPEND_TO_BUFFER("&", 1)
 APPEND_TO_BUFFER(date, d_strlen(date))
 APPEND_TO_BUFFER("&", 1)
 APPEND_TO_BUFFER(amount, d_strlen(amount))
 APPEND_TO_BUFFER(" HTTP/1.1\r\nHost: localhost\r\n\r\n", 30)
 *buffer_iter = 0;
 *request_len = (long int)(buffer_iter - request);
}

DEVICE void quick_pay_html(char* response, long int* response_len, char* userid, char* msg, char conf[MAX_PAYEES][VALUE_LEN], int num_payees)
{
 char* buffer_iter = response;
 int len = 0, i;
 
 APPEND_TO_BUFFER(quick_pay_str1, len_quick_pay_str1)
 APPEND_TO_BUFFER(userid, d_strlen(userid))
 APPEND_TO_BUFFER(quick_pay_str2, len_quick_pay_str2)

 for(i=0; i<num_payees; i++) {
  APPEND_TO_BUFFER("      <tr><td>", 14)
  APPEND_TO_BUFFER((msg+i*MSG_LEN), d_strlen(msg+i*MSG_LEN))
  APPEND_TO_BUFFER("</td></tr>\r\n", 12)
 }
 APPEND_TO_BUFFER("    <tr><td>Confirmation Code</td></tr>\r\n    <tr><td>", 53)
 len = d_strlen(conf[0]);
 if(len > 0) {
  APPEND_TO_BUFFER(conf[0], len)
 }
 APPEND_TO_BUFFER(quick_pay_str3, len_quick_pay_str3)
 APPEND_TO_BUFFER(quick_pay_padding, quick_pay_padding_len)
 APPEND_TO_BUFFER(quick_pay_str4, len_quick_pay_str4)
 /* Terminate the string */
 *buffer_iter = 0;
 *response_len = (int)(buffer_iter - response);
}

void* quick_pay_php_create(int n_req)
{
 quick_pay_php_private* p = (quick_pay_php_private*)malloc(sizeof(quick_pay_php_private));
 check_cuda(cudaMalloc((void**)&p->msg, n_req*MAX_PAYEES*MSG_LEN));
 check_cuda(cudaMalloc((void**)&p->msg_len, n_req*MAX_PAYEES*sizeof(int)));
 p->backend = backend_create(n_req*MAX_PAYEES);
 return p;
}

void quick_pay_php_destroy(void* data)
{
 quick_pay_php_private* p = (quick_pay_php_private*)data;
 check_cuda(cudaFree(p->msg));
 check_cuda(cudaFree(p->msg_len));
 backend_destroy(p->backend);
 free(p);
}

int _quick_pay_php_cb_2(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  /* Free private state */
  quick_pay_php_destroy(r->req_private);
  response_add(r->resp);
  cohort_free(r->cohorts, r->cohort_id);
  request_handler_destroy(r);
  return false;
 }
 return true;
}

GLOBAL void quick_pay_php_2(cohort* c, char* be_resp, long int* be_resp_len, int* req_state, char* resp, long int* resp_len, session_bucket* session_array, int cohort_id, char* msg, int* msg_len)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &c[cohort_id].buffer[tid];
 char* temp;
 int num_payees = 0, num_dates = 0, num_amounts = 0, i;
 char payee[MAX_PAYEES][VALUE_LEN];
 char date[MAX_PAYEES][VALUE_LEN];
 char amount[MAX_PAYEES][VALUE_LEN];
 char conf[MAX_PAYEES][32];
 char* userid = session_find(session_array, r->session_id);
 
 if(req_state[tid] != REQ_STATE_CONTINUE) {
  return;
 }
 get_array(r, "payee", 5, &num_payees, payee);
 get_array(r, "payee", 5, &num_dates, date);
 get_array(r, "payee", 5, &num_amounts, amount);
 for(i=0; i<num_payees; i++) {
  int offset = tid*MAX_PAYEES+i;
  char* buffer_iter = msg + offset*MSG_LEN + *(msg_len+offset);
  temp = process_backend_response(be_resp + offset*BESIM_RESPONSE_LEN, NULL);
  d_strcpy(conf[i], temp);
  APPEND_TO_BUFFER("Scheduled: payee=", 17)
  APPEND_TO_BUFFER(payee[i], d_strlen(payee[i]))
  APPEND_TO_BUFFER(", date=", 7)
  APPEND_TO_BUFFER(date[i], d_strlen(date[i]))
  APPEND_TO_BUFFER(", amount=", 9)
  APPEND_TO_BUFFER(amount[i], d_strlen(amount[i]))
  APPEND_TO_BUFFER("\n", 1)
  *buffer_iter = 0;
 }
  
 quick_pay_html(resp + tid*RESPONSE_LEN, resp_len + tid, userid, msg + tid*MAX_PAYEES*MSG_LEN, conf, num_payees);
 req_state[tid] = REQ_STATE_CONTINUE;
}

int _quick_pay_php_cb_1(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  int n_req = r->n_req;
  quick_pay_php_private* p = (quick_pay_php_private*)r->req_private;
  backend_process(p->backend);
  backend_copy_response_to_device(p->backend, r->stream);
  quick_pay_php_2<<<1, n_req, 0, r->stream>>>(r->cohorts->d_cohorts, r->be->d_resp, r->be->d_resp_len, r->d_req_state, r->resp->d_resp, r->resp->d_resp_len, r->d_session_array, r->cohort_id, p->msg, p->msg_len);
  response_copy_to_host(r->resp);
  callbacks_add(_quick_pay_php_cb_2, r);
  return false;
 }
 return true;
}

DEVICE int check_date(int year, int month, int day)
{
 int days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
 if(year < 1 || year > 32767) {
  return 0;
 }
 if(month < 1 || month > 12) {
  return 0;
 }
 if(day < 1 || day > days_in_month[month-1]) {
  return 0;
 }
 return 1;
}

DEVICE int compare_date(int year1, int month1, int day1, int year2, int month2, int day2)
{
 if(year1 < year2) {
  return -1;
 } else if(year1 > year2) {
  return 1;
 }
 if(month1 < month2) {
  return -1;
 } else if(month1 > month2) {
  return 1;
 }
 if(day1 < day2) {
  return -1;
 } else if(day1 > day2) {
  return 1;
 } else {
  return 0;
 }
}

GLOBAL void quick_pay_php_1(cohort* c, char* be_req, long int* be_req_len, int* req_state, char* resp, long int* resp_len, int cohort_id, session_bucket* session_array, char* msg, int* msg_len)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &c[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 int num_payees = 0, num_dates = 0, num_amounts = 0, i;
 char payee[MAX_PAYEES][VALUE_LEN];
 char date[MAX_PAYEES][VALUE_LEN];
 char amount[MAX_PAYEES][VALUE_LEN];
 char temp_date[VALUE_LEN];
 int year, month, day;
 
 if(userid == NULL) {
  login_html(resp + tid, resp_len + tid, "Please login", 12);
  req_state[tid] = REQ_STATE_ERROR;
  return;
 }
 get_array(r, "payee", 5, &num_payees, payee);
 if(num_payees == 0) {
  message_html(resp + tid, resp_len + tid, "No payee selected", 17);
  req_state[tid] = REQ_STATE_ERROR;
  return;
 }
 get_array(r, "date", 4, &num_dates, date);
 if(num_dates == 0) {
  message_html(resp + tid, resp_len + tid, "No date selected", 16);
  req_state[tid] = REQ_STATE_ERROR;
  return;
 }
 get_array(r, "amount", 6, &num_amounts, amount);
 if(num_amounts == 0) {
  message_html(resp + tid, resp_len + tid, "No amount selected", 18);
  req_state[tid] = REQ_STATE_ERROR;
  return;
 }
 for(i=0; i<num_payees; i++) {
  int offset = tid*MAX_PAYEES+i;
  char* buffer_iter = msg + offset*MSG_LEN;
  if(d_strlen(payee[i]) == 0) {
   continue;
  }
  if(d_strlen(date[i]) == 0) {
   APPEND_TO_BUFFER("Failed: payee=", 14)
   APPEND_TO_BUFFER(payee[i], d_strlen(payee[i]))
   APPEND_TO_BUFFER("  Reason: No date\n", 18)
   continue;
  }
  d_strcpy(temp_date, date[i]);
  temp_date[4] = '\0';
  temp_date[7] = '\0';
  year = d_atoi(temp_date);
  month = d_atoi(temp_date+5);
  day = d_atoi(temp_date+8);
  if(!check_date(year, month, day)) {
   APPEND_TO_BUFFER("Failed: payee=", 14)
   APPEND_TO_BUFFER(payee[i], d_strlen(payee[i]))
   APPEND_TO_BUFFER("  Reason: Invalid date format\n", 30)
   continue;
  }
  if(compare_date(year, month, day, (r->recv_time_tm.tm_year+1900), (r->recv_time_tm.tm_mon+1), (r->recv_time_tm.tm_mday+1)) < 0){
   APPEND_TO_BUFFER("Failed: payee=", 14)
   APPEND_TO_BUFFER(payee[i], d_strlen(payee[i]))
   APPEND_TO_BUFFER("  Reason: Tried to schedule payment earlier than today\n", 55)
   continue;
  }
  if(d_strlen(amount[i]) == 0) {
   APPEND_TO_BUFFER("Failed: payee=", 14)
   APPEND_TO_BUFFER(payee[i], d_strlen(payee[i]))
   APPEND_TO_BUFFER("  Reason: No amount\n", 20)
   continue;
  }
  float amt = d_atof(amount[i]);
  if (amt <= 0.0) {
   APPEND_TO_BUFFER("Failed: payee=", 14)
   APPEND_TO_BUFFER(payee[i], d_strlen(payee[i]))
   APPEND_TO_BUFFER("  Reason: Illegal amount\n", 25)
   continue;
  }
  *(msg_len+offset) = buffer_iter - (msg + offset*MSG_LEN);
  _get_quick_pay_request(be_req + offset*BESIM_REQUEST_LEN, userid, payee[i], date[i], amount[i], be_req_len + offset);
 }
 req_state[tid] = REQ_STATE_CONTINUE;
}

void quick_pay_php(request_handler* r)
{
 int n_req = r->n_req;
 quick_pay_php_private* p = (quick_pay_php_private*)r->req_private;
 check_cuda(cudaMemsetAsync(r->d_req_state, REQ_STATE_ERROR, n_req*sizeof(int), r->stream));
 quick_pay_php_1<<<1, n_req, 0, r->stream>>>(r->cohorts->d_cohorts, p->backend->d_req, p->backend->d_req_len, r->d_req_state, r->resp->d_resp, r->resp->d_resp_len, r->cohort_id, r->d_session_array, p->msg, p->msg_len);
 backend_copy_requests_to_host(p->backend, r->stream);
 callbacks_add(_quick_pay_php_cb_1, r);
}

void quick_pay_once()
{
 GENERATE_PADDING(quick_pay)
}
#endif
