#ifndef LOGIN_C_INCLUDED
#define LOGIN_C_INCLUDED

#include "utils/md5.c"
#include "backend/backend.c"

CONSTANT char* welcome_padding;

#define PASSWD_LEN 32


DEVICE char* welcome_html(char* response, int* response_len, char* userid, char* be_resp)
{
 char* buffer_iter = response;
 int acct_type;
 char *ssave1 = "", *ssave2 = "", *p1, *p2;
 int diff;
 int laneId = threadIdx.x & 0x1f;
 int warpId = threadIdx.x & 0xffffffe0;
 SHARED int tile[THREAD_BLOCK_SIZE];
 
 APPEND_TO_BUFFER_TRANSPOSED(welcome_str1)
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED("</td></tr>\n")
 APPEND_WHITESPACE_PADDING()
 APPEND_TO_BUFFER_TRANSPOSED(welcome_str2)
 
 p1 = d_strtok_r_transposed(be_resp, "\n", &ssave1);
 while(p1 != NULL) {
  p2 = d_strtok_r_transposed(p1, "&", &ssave2);
  APPEND_TO_BUFFER_TRANSPOSED("  <tr><td>")
  APPEND_TO_BUFFER_TRANSPOSED_2(p2)
  p2 = d_strtok_r_transposed(NULL, "&", &ssave2);
  acct_type = d_atoi_transposed(p2);
  switch(acct_type) {
   case 1:
    APPEND_TO_BUFFER_TRANSPOSED("</td>\n  <td>Checking</td>\n  <td>")
    break;
   case 2:
    APPEND_TO_BUFFER_TRANSPOSED("</td>\n  <td>Saving</td>\n  <td>")
    break;
   default:
    APPEND_TO_BUFFER_TRANSPOSED("</td>\n  <td>Other</td>\n  <td>")
    break;
  }
  p2 = d_strtok_r_transposed(NULL, "&", &ssave2);
  APPEND_TO_BUFFER_TRANSPOSED_2(p2)
  APPEND_TO_BUFFER_TRANSPOSED("</td></tr>\n")
  p1 = d_strtok_r_transposed(NULL, "\n", &ssave1);
 }
 APPEND_WHITESPACE_PADDING()
 APPEND_TO_BUFFER_TRANSPOSED(welcome_str3)
 APPEND_TO_BUFFER_TRANSPOSED(welcome_padding)
 APPEND_TO_BUFFER_TRANSPOSED(welcome_str4)
 /* Terminate the string */
 *buffer_iter = 0;
 *response_len = (int)((buffer_iter - response)/COHORT_SIZE);
 return buffer_iter;
}

DEVICE void login_html(char* response, int* response_len, char* message)
{
 char* buffer_iter = response;
 APPEND_TO_BUFFER_TRANSPOSED(login_str1)
 APPEND_TO_BUFFER_TRANSPOSED(message)
 APPEND_TO_BUFFER_TRANSPOSED(login_str2)
 *buffer_iter = 0;
 *response_len = (int)((buffer_iter - response)/COHORT_SIZE);
}

DEVICE void _get_passwd_request(char* request, char* userid)
{
 char* buffer_iter = request;
 APPEND_TO_BUFFER_TRANSPOSED("GET /fcgi-bin/besim_fcgi.fcgi?1&1&")
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED(" HTTP/1.1\r\nHost: localhost\r\n\r\n")
 *buffer_iter = 0;
}

DEVICE void _get_acct_balance_request(char* request, char* userid)
{
 char* buffer_iter = request;
 APPEND_TO_BUFFER_TRANSPOSED("GET /fcgi-bin/besim_fcgi.fcgi?1&2&")
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED(" HTTP/1.1\r\nHost: localhost\r\n\r\n")
 *buffer_iter = 0;
}

int _login_php_cb_3(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  response_add(r->resp);
  cohort_free(r->cohort_id);
  request_handler_free(r);
  return false;
 }
 return true;
}

GLOBAL void login_php_3(char* be_resp, int* req_state, char* resp, int* resp_len, session_bucket* session_array, int cohort_id)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char userid[USERID_LEN];
 char scratch[10];
 char* buffer_iter = resp + tid;
 char* content_len_iter = NULL;
 int response_len = 0;
 int session_id = 0;
 
 if(req_state[tid] != REQ_STATE_CONTINUE) {
  return;
 }
 get_value(r, "userid", 6, userid);
 session_id = session_create(session_array, tid, userid);
 APPEND_TO_BUFFER_TRANSPOSED(response_header_str)
 APPEND_TO_BUFFER_TRANSPOSED("Content-Length: ")
 content_len_iter = buffer_iter;
 APPEND_TO_BUFFER_TRANSPOSED("          ")
 APPEND_TO_BUFFER_TRANSPOSED("\r\nSet-cookie: SESSIONID=")
 d_itoa(session_id, scratch, 10, NULL);
 APPEND_TO_BUFFER_TRANSPOSED(scratch)
 APPEND_TO_BUFFER_TRANSPOSED("\r\n\r\n")
 buffer_iter = welcome_html(buffer_iter, &response_len, userid, process_backend_response_transposed(be_resp + tid, NULL));
 resp_len[tid] = (int)((buffer_iter - resp)/COHORT_SIZE);
 /* Insert content len here padded by whitespaces */
 buffer_iter = content_len_iter;
 d_itoa(response_len, scratch, 10, NULL);
 APPEND_TO_BUFFER_TRANSPOSED(scratch)
 req_state[tid] = REQ_STATE_CONTINUE;
}

void _login_php_cb_2_continue(void* data)
{
 request_handler* r = (request_handler*)data;
 backend_copy_response_to_device(r->be, r->stream);
 login_php_3<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_resp, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->d_session_array, r->cohort_id);
 callbacks_add(_login_php_cb_3, r);
}

int _login_php_cb_2(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  backend_process(r, _login_php_cb_2_continue);
  return false;
 }
 return true;
}

DEVICE int todigit(char hex)
{
 if(hex >= 48 && hex <= 57) {
  return (int)(hex - 48);
 } else if(hex >= 97 && hex <= 102) {
  return (int)(hex - 87);
 }
 return 0;
}

GLOBAL void login_php_2(char* be_req, char* be_resp, int* req_state, char* resp, int* resp_len, int cohort_id)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 char userid[USERID_LEN];
 char passwd[PASSWD_LEN];
 unsigned char digest[16];
 char real_passwd[16];
 char* temp;
 MD5_CTX context;
 int i;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 
 if(req_state[tid] != REQ_STATE_CONTINUE) {
  return;
 }
 get_value(r, "userid", 6, userid);
 get_value(r, "password", 8, passwd);
 temp = process_backend_response_transposed(be_resp + tid, NULL);
 for(i=0; i<16; i++) {
  real_passwd[i] = (char)(todigit(temp[TRANSPOSE((2*i+1))]) + 16*todigit(temp[TRANSPOSE((2*i))]));
 }
 MD5_Init(&context);
 MD5_Update(&context, (unsigned char *)passwd, d_strlen(passwd));
 MD5_Final(digest, &context);
 if(!d_strncmp((char*)digest, real_passwd, 16)) {
  _get_acct_balance_request(be_req + tid, userid);
  req_state[tid] = REQ_STATE_CONTINUE;
 } else {
  login_html(resp + tid, resp_len + tid, "Incorrect user id or password!");
  req_state[tid] = REQ_STATE_ERROR;
 }
}

void _login_php_cb_1_continue(void* data)
{
 request_handler* r = (request_handler*)data;
 backend_copy_response_to_device(r->be, r->stream);
 login_php_2<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_req, r->be->d_transposed_resp, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->cohort_id);
 backend_copy_requests_to_host(r->be, r->stream);
 callbacks_add(_login_php_cb_2, r);
}

int _login_php_cb_1(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  backend_process(r, _login_php_cb_1_continue);
  return false;
 }
 return true;
}

GLOBAL void login_php_1(char* be_req, int* req_state, char* resp, int* resp_len, int cohort_id, int* fds)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 fds[tid] = r->client;
 
 if(r->type == REQUEST_TYPE_GET || r->query_string == NULL) {
  login_html(resp + tid, resp_len + tid, "");
  req_state[tid] = REQ_STATE_ERROR;
 } else {
  char userid[USERID_LEN];
  char passwd[PASSWD_LEN];
  int ret1 = get_value(r, "userid", 6, userid);
  int ret2 = get_value(r, "password", 8, passwd);
  if(!ret1 && !ret2) {
   login_html(resp + tid, resp_len + tid, "");
   req_state[tid] = REQ_STATE_ERROR;
  } else if(!d_strlen(userid) || !d_strlen(passwd)) {
   login_html(resp + tid, resp_len + tid, "No empty user id or password allowed!");
   req_state[tid] = REQ_STATE_ERROR;
  } else {
   _get_passwd_request(be_req + tid, userid);
   req_state[tid] = REQ_STATE_CONTINUE;
  }
 }
}

void login_php(request_handler* r)
{
 login_php_1<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_req, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->cohort_id, r->resp->d_fds);
 backend_copy_requests_to_host(r->be, r->stream);
 callbacks_add(_login_php_cb_1, r);
}

void login_php_once()
{
 GENERATE_PADDING(welcome)
}
#endif
