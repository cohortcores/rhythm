#ifndef SPECWEB_C_INCLUDED
#define SPECWEB_C_INCLUDED

#include "utils/utils.c"
#include "banking.h"
#include "strings_banking.h"
#include "init.c"
#include "message.c"
#include "login.c"
#include "account_summary.c"
#include "add_payee.c"
#include "bill_pay.c"
//#include "bill_pay_status_input.c"
#include "bill_pay_status_output.c"
#include "change_profile.c"
#include "check_detail_html.c"
//#include "check_detail_image.c"
//#include "check_detail_input.c"
#include "logout.c"
#include "order_check.c"
#include "place_check_order.c"
#include "post_payee.c"
#include "post_transfer.c"
#include "profile.c"
//#include "quick_pay.c"
#include "transfer.c"

typedef enum {
 REQUEST_URI_SPECWEB_BANKING_INIT = 1 ,
 REQUEST_URI_SPECWEB_BANKING_LOGIN = 2,
 REQUEST_URI_SPECWEB_BANKING_ACCOUNT_SUMMARY = 3,
 REQUEST_URI_SPECWEB_BANKING_ADD_PAYEE = 4,
 REQUEST_URI_SPECWEB_BANKING_BILL_PAY = 5,
 REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_INPUT = 6,
 REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_OUTPUT,
 REQUEST_URI_SPECWEB_BANKING_CHANGE_PROFILE,
 REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_HTML,
 REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_IMAGE,
 REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_INPUT,
 REQUEST_URI_SPECWEB_BANKING_ORDER_CHECK,
 REQUEST_URI_SPECWEB_BANKING_PLACE_CHECK_ORDER,
 REQUEST_URI_SPECWEB_BANKING_POST_PAYEE,
 REQUEST_URI_SPECWEB_BANKING_POST_TRANSFER,
 REQUEST_URI_SPECWEB_BANKING_PROFILE,
 REQUEST_URI_SPECWEB_BANKING_QUICK_PAY,
 REQUEST_URI_SPECWEB_BANKING_TRANSFER,
 REQUEST_URI_SPECWEB_BANKING_LOGOUT,
 REQUEST_URI_MAX
} request_uri;

void (*once_funcs[REQUEST_URI_MAX])() = {
 NULL, NULL, login_php_once, account_summary_php_once, add_payee_php_once, bill_pay_php_once, NULL, bill_pay_status_output_php_once, change_profile_php_once, check_detail_html_php_once, NULL, NULL, order_check_php_once, place_check_order_php_once, post_payee_php_once, post_transfer_php_once, profile_php_once, NULL, transfer_php_once, logout_php_once };

void (*process_funcs[REQUEST_URI_MAX])(request_handler* r) = {
 NULL, NULL, login_php, account_summary_php, add_payee_php, bill_pay_php, NULL, bill_pay_status_output_php, change_profile_php, check_detail_html_php, NULL, NULL, order_check_php, place_check_order_php, post_payee_php, post_transfer_php,profile_php, NULL, transfer_php, logout_php };

DEVICE int get_uri(char* uri)
{
 if(!d_strcasecmp(uri, "/bank/init.php")) {
  return REQUEST_URI_SPECWEB_BANKING_INIT;
 } else if(!d_strcasecmp(uri, "/bank/login.php")) {
  return REQUEST_URI_SPECWEB_BANKING_LOGIN;
 } else if(!d_strcasecmp(uri, "/bank/account_summary.php")) {
  return REQUEST_URI_SPECWEB_BANKING_ACCOUNT_SUMMARY;
 } else if(!d_strcasecmp(uri, "/bank/check_detail_input.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_INPUT;
 } else if(!d_strcasecmp(uri, "/bank/check_detail_html.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_HTML;
 } else if(!d_strcasecmp(uri, "/bank/check_detail_image.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_IMAGE;
 } else if(!d_strcasecmp(uri, "/bank/bill_pay.php")) {
  return REQUEST_URI_SPECWEB_BANKING_BILL_PAY;
 } else if(!d_strcasecmp(uri, "/bank/quick_pay.php")) {
  return REQUEST_URI_SPECWEB_BANKING_QUICK_PAY;
 } else if(!d_strcasecmp(uri, "/bank/add_payee.php")) {
  return REQUEST_URI_SPECWEB_BANKING_ADD_PAYEE;
 } else if(!d_strcasecmp(uri, "/bank/post_payee.php")) {
  return REQUEST_URI_SPECWEB_BANKING_POST_PAYEE;
 } else if(!d_strcasecmp(uri, "/bank/bill_pay_status_input.php")) {
  return REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_INPUT;
 } else if(!d_strcasecmp(uri, "/bank/bill_pay_status_output.php")) {
  return REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_OUTPUT;
 } else if(!d_strcasecmp(uri, "/bank/profile.php")) {
  return REQUEST_URI_SPECWEB_BANKING_PROFILE;
 } else if(!d_strcasecmp(uri, "/bank/change_profile.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHANGE_PROFILE;
 } else if(!d_strcasecmp(uri, "/bank/order_check.php")) {
  return REQUEST_URI_SPECWEB_BANKING_ORDER_CHECK;
 } else if(!d_strcasecmp(uri, "/bank/place_check_order.php")) {
  return REQUEST_URI_SPECWEB_BANKING_PLACE_CHECK_ORDER;
 } else if(!d_strcasecmp(uri, "/bank/transfer.php")) {
  return REQUEST_URI_SPECWEB_BANKING_TRANSFER;
 } else if(!d_strcasecmp(uri, "/bank/post_transfer.php")) {
  return REQUEST_URI_SPECWEB_BANKING_POST_TRANSFER;
 } else if(!d_strcasecmp(uri, "/bank/logout.php")) {
  return REQUEST_URI_SPECWEB_BANKING_LOGOUT;
 }
 return REQUEST_URI_NONE;
}

int host_get_uri(char* uri)
{
 if(!strcasecmp(uri, "/bank/init.php")) {
  return REQUEST_URI_SPECWEB_BANKING_INIT;
 } else if(!strcasecmp(uri, "/bank/login.php")) {
  return REQUEST_URI_SPECWEB_BANKING_LOGIN;
 } else if(!strcasecmp(uri, "/bank/account_summary.php")) {
  return REQUEST_URI_SPECWEB_BANKING_ACCOUNT_SUMMARY;
 } else if(!strcasecmp(uri, "/bank/check_detail_input.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_INPUT;
 } else if(!strcasecmp(uri, "/bank/check_detail_html.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_HTML;
 } else if(!strcasecmp(uri, "/bank/check_detail_image.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_IMAGE;
 } else if(!strcasecmp(uri, "/bank/bill_pay.php")) {
  return REQUEST_URI_SPECWEB_BANKING_BILL_PAY;
 } else if(!strcasecmp(uri, "/bank/quick_pay.php")) {
  return REQUEST_URI_SPECWEB_BANKING_QUICK_PAY;
 } else if(!strcasecmp(uri, "/bank/add_payee.php")) {
  return REQUEST_URI_SPECWEB_BANKING_ADD_PAYEE;
 } else if(!strcasecmp(uri, "/bank/post_payee.php")) {
  return REQUEST_URI_SPECWEB_BANKING_POST_PAYEE;
 } else if(!strcasecmp(uri, "/bank/bill_pay_status_input.php")) {
  return REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_INPUT;
 } else if(!strcasecmp(uri, "/bank/bill_pay_status_output.php")) {
  return REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_OUTPUT;
 } else if(!strcasecmp(uri, "/bank/profile.php")) {
  return REQUEST_URI_SPECWEB_BANKING_PROFILE;
 } else if(!strcasecmp(uri, "/bank/change_profile.php")) {
  return REQUEST_URI_SPECWEB_BANKING_CHANGE_PROFILE;
 } else if(!strcasecmp(uri, "/bank/order_check.php")) {
  return REQUEST_URI_SPECWEB_BANKING_ORDER_CHECK;
 } else if(!strcasecmp(uri, "/bank/place_check_order.php")) {
  return REQUEST_URI_SPECWEB_BANKING_PLACE_CHECK_ORDER;
 } else if(!strcasecmp(uri, "/bank/transfer.php")) {
  return REQUEST_URI_SPECWEB_BANKING_TRANSFER;
 } else if(!strcasecmp(uri, "/bank/post_transfer.php")) {
  return REQUEST_URI_SPECWEB_BANKING_POST_TRANSFER;
 } else if(!strcasecmp(uri, "/bank/logout.php")) {
  return REQUEST_URI_SPECWEB_BANKING_LOGOUT;
 }
 return REQUEST_URI_NONE;
}

#endif
