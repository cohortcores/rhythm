#ifndef PROFILE_C_INLCUDED
#define PROFILE_C_INLCUDED

CONSTANT char* profile_padding;
CONSTANT int profile_padding_len = 0;

DEVICE void _get_profile_request(char* request, char* userid)
{
 char* buffer_iter = request;
 APPEND_TO_BUFFER_TRANSPOSED("GET /fcgi-bin/besim_fcgi.fcgi?1&9&")
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED(" HTTP/1.1\r\nHost: localhost\r\n\r\n")
 *buffer_iter = 0;
}

DEVICE char* profile_html(char* response, int* response_len, char* userid, char* be_resp)
{
 char* buffer_iter = response;
 char *ssave1 = "", *ssave2 = "", *p1, *p2;
 int diff;
 int laneId = threadIdx.x & 0x1f;
 int warpId = threadIdx.x & 0xffffffe0;
 SHARED int tile[THREAD_BLOCK_SIZE];
 
 APPEND_TO_BUFFER_TRANSPOSED(profile_str1)
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED(profile_str2)

 p1 = d_strtok_r_transposed(be_resp, "\n", &ssave1);
 p2 = d_strtok_r_transposed(p1, "&", &ssave2);
 APPEND_TO_BUFFER_TRANSPOSED_2(p2)
 APPEND_TO_BUFFER_TRANSPOSED("</td>\r\n    </tr>\r\n    <tr>\r\n      <th>City </th>\r\n      <td>")
 p2 = d_strtok_r_transposed(NULL, "&", &ssave2);
 APPEND_TO_BUFFER_TRANSPOSED_2(p2)
 APPEND_TO_BUFFER_TRANSPOSED("</td>\r\n    </tr>\r\n    <tr>\r\n      <th>State </th>\r\n      <td>")
 p2 = d_strtok_r_transposed(NULL, "&", &ssave2);
 APPEND_TO_BUFFER_TRANSPOSED_2(p2)
 APPEND_TO_BUFFER_TRANSPOSED("</td>\r\n    </tr>\r\n    <tr>\r\n      <th>Zip </th>\r\n      <td>")
 p2 = d_strtok_r_transposed(NULL, "&", &ssave2);
 APPEND_TO_BUFFER_TRANSPOSED_2(p2)
 APPEND_TO_BUFFER_TRANSPOSED("</td>\r\n    </tr>\r\n    <tr>\r\n      <th>Phone </th>\r\n      <td>")
 p2 = d_strtok_r_transposed(NULL, "&", &ssave2);
 APPEND_TO_BUFFER_TRANSPOSED_2(p2)
 APPEND_TO_BUFFER_TRANSPOSED("</td>\r\n    </tr>\r\n    <tr>\r\n      <th>Email </th>\r\n      <td>")
 p2 = d_strtok_r_transposed(NULL, "&", &ssave2);
 APPEND_TO_BUFFER_TRANSPOSED_2(p2)
 APPEND_TO_BUFFER_TRANSPOSED("</td>\r\n")
 APPEND_WHITESPACE_PADDING()
 APPEND_TO_BUFFER_TRANSPOSED(profile_str3)
 APPEND_TO_BUFFER_TRANSPOSED(profile_padding)
 APPEND_TO_BUFFER_TRANSPOSED(profile_str4)
 /* Terminate the string */
 *buffer_iter = 0;
 *response_len = (int)((buffer_iter - response)/COHORT_SIZE);
 return buffer_iter;
}

int _profile_php_cb_2(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  response_add(r->resp);
  cohort_free(r->cohort_id);
  request_handler_free(r);
  return false;
 }
 return true;
}

GLOBAL void profile_php_2(char* be_resp, int* req_state, char* resp, int* resp_len, session_bucket* session_array, int cohort_id)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 char scratch[10];
 char* buffer_iter = resp + tid;
 char* content_len_iter = NULL;
 int response_len = 0;
 
 if(req_state[tid] != REQ_STATE_CONTINUE) {
  return;
 }
 APPEND_TO_BUFFER_TRANSPOSED(response_header_str)
 APPEND_TO_BUFFER_TRANSPOSED("Content-Length: ")
 content_len_iter = buffer_iter;
 APPEND_TO_BUFFER_TRANSPOSED("          ")
 APPEND_TO_BUFFER_TRANSPOSED("\r\n\r\n")
 buffer_iter = profile_html(buffer_iter, &response_len, userid, process_backend_response_transposed(be_resp + tid, NULL));
 resp_len[tid] = (int)((buffer_iter - resp)/COHORT_SIZE);
 /* Insert content len here padded by whitespaces */
 buffer_iter = content_len_iter;
 d_itoa(response_len, scratch, 10, NULL);
 APPEND_TO_BUFFER_TRANSPOSED(scratch)
 req_state[tid] = REQ_STATE_CONTINUE;
}

void _profile_php_cb_1_continue(void* data)
{
 request_handler* r = (request_handler*)data;
 backend_copy_response_to_device(r->be, r->stream);
 profile_php_2<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_resp, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->d_session_array, r->cohort_id);
 callbacks_add(_profile_php_cb_2, r);
}

int _profile_php_cb_1(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  backend_process(r, _profile_php_cb_1_continue);
  return false;
 }
 return true;
}

GLOBAL void profile_php_1(char* be_req, int* req_state, char* resp, int* resp_len, int cohort_id, session_bucket* session_array, int* fds)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 fds[tid] = r->client;
 
 if(userid == NULL) {
  login_html(resp + tid, resp_len + tid, "Please login");
  req_state[tid] = REQ_STATE_ERROR;
 } else {
  _get_profile_request(be_req + tid, userid);
  req_state[tid] = REQ_STATE_CONTINUE;
 }
}

void profile_php(request_handler* r)
{
 profile_php_1<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_req, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->cohort_id, r->d_session_array, r->resp->d_fds);
 backend_copy_requests_to_host(r->be, r->stream);
 callbacks_add(_profile_php_cb_1, r);
}

void profile_php_once()
{
 GENERATE_PADDING(profile)
}

#endif