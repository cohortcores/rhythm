/*
 * login.c
 *
 *  Created on: Sep 4, 2012
 *      Author: sandeep
 */
#ifndef LOGIN_CU_INCLUDED
#define LOGIN_CU_INCLUDED

#if !defined(__CUDACC__)
#define __align__(n) __attribute__ ((aligned (n)))
#endif

#include "welcome.h"
#include "login.h"
#include "utils/utils.cu"
#include "backend/besim_banking.cu"

DEVICE void backend_get_passwd(char* userid, char* passwd)
{
  LoginPasswd(userid, passwd);
}

DEVICE void backend_get_acct_balance(char* userid, acct_info* accts, int* num)
{
  AcctBalances(userid, accts, num);
}


#define COPY_TO_BUFFER(str) \
  {  \
    int len = d_strlen(str); \
    memcpy(buffer_iter, str, len);  \
    buffer_iter += len; \
  }
#if defined(__CUDACC__)
#define COPY_TO_BUFFER_LEN(str, len) \
  {  \
    int j; \
	if(id_within_request==0) { \
	  for(j=0; j<len; j++) { \
        buffer_iter[j] = str[j]; \
	  } \
    } \
    buffer_iter += len; \
  }

DEVICE char* coalesced_aligned_copy(char* dest, char* src, int len, int threads_per_request, int n)
{
  int i = 0;
  int global_thread_id = blockIdx.x*blockDim.x + threadIdx.x;
  int id_within_request = global_thread_id % threads_per_request;
  
  /* pad spaces to maintain alignment */
  while(((size_t)dest%n) != 0 && (len > 0)) {
    if(id_within_request==0) {
      *dest = ' ';
    }
    dest++;
  }
  
  for(i=id_within_request; i<(len/n); i+=threads_per_request) {
    switch(n) {
      case 1:
        *(dest+i) = *(src+i);
        break;
      case 4:
        *(int*)(dest + i*4) = (int)*(int*)(src + i*4);
	break;
      case 8:
        *(long long int*)(dest + i*8) = (long long int)*(long long int*)(src + i*8);
        break;
    }
  }
  /* Trailing bytes */
  dest += (len/n)*n;
  src += (len/n)*n;
  len -= (len/n)*n;
  while(len > 0) {
    if(id_within_request==0) {
      *dest = *src;
    }
    dest++;
    src++;
    len--;
  }
  return dest;
}  
#else
#define COPY_TO_BUFFER_LEN(str, len) \
  {  \
    int j; \
	for(j=0; j<len; j++) { \
      buffer_iter[j] = str[j]; \
	} \
    buffer_iter += len; \
  }  
  
DEVICE char* coalesced_aligned_copy(char* dest, char* src, int len, int threads_per_request, int n)
{
  memcpy(dest, src, len);
  dest += len;
  return dest;
}  
#endif

DEVICE void welcome_html(char* response, char* userid, acct_info* accts, int num_accts, int threads_per_request)
{
  int i = 0;
  char* tmp;
  char scratch[20] = {0}; /* max length of a 64 bit integer is 19 + 1 for terminating char */
  char* buffer_iter = response;
  int len = 0;
#if defined(__CUDACC__)  
  int global_thread_id = blockIdx.x*blockDim.x + threadIdx.x;
  int id_within_request = global_thread_id % threads_per_request;
#endif
  buffer_iter = coalesced_aligned_copy(buffer_iter, welcome_str1, len_welcome_str1, threads_per_request, 4);
  COPY_TO_BUFFER(userid)
  buffer_iter = coalesced_aligned_copy(buffer_iter, welcome_str2, len_welcome_str2, threads_per_request, 4);
  
  for(i=0; i<num_accts; i++) {
    COPY_TO_BUFFER_LEN("  <tr><td>", 10)
    tmp = d_itoa(accts[i].acct_num, scratch, 10, &len);
    COPY_TO_BUFFER_LEN(tmp, len)
    switch(accts[i].acct_type) {
      case 1:
	    buffer_iter = coalesced_aligned_copy(buffer_iter, "</td>\n  <td>Checking</td>\n  <td>", 32, threads_per_request, 1);
        //COPY_TO_BUFFER_LEN("</td>\n  <td>Checking</td>\n  <td>", 32)
        break;
      case 2:
	    buffer_iter = coalesced_aligned_copy(buffer_iter, "</td>\n  <td>Saving</td>\n  <td>", 30, threads_per_request, 1);
        //COPY_TO_BUFFER_LEN("</td>\n  <td>Saving</td>\n  <td>", 30)
        break;
      default:
	    buffer_iter = coalesced_aligned_copy(buffer_iter, "</td>\n  <td>Other</td>\n  <td>", 29, threads_per_request, 1);
        //COPY_TO_BUFFER_LEN("</td>\n  <td>Other</td>\n  <td>", 29)
        break;
    }
    tmp = d_itoa(accts[i].dollars, scratch, 10, &len);
    COPY_TO_BUFFER_LEN(tmp, len)
    COPY_TO_BUFFER_LEN(".", 1)
    tmp = d_itoa(accts[i].cents, scratch, 10, &len);
    COPY_TO_BUFFER_LEN(tmp, len)
    COPY_TO_BUFFER_LEN("</td></tr>\n", 11)
  }
  buffer_iter = coalesced_aligned_copy(buffer_iter, welcome_str3, len_welcome_str3, threads_per_request, 4);
  //COPY_TO_BUFFER(welcome_padding)
  buffer_iter = coalesced_aligned_copy(buffer_iter, welcome_str4, len_welcome_str4, threads_per_request, 4);
  /* Terminate the string */
  *buffer_iter = 0;
}

DEVICE void login_html(char* response, char* message, int message_len, int threads_per_request)
{
  char* buffer_iter = response;

  buffer_iter = coalesced_aligned_copy(buffer_iter, login_str1, d_strlen(login_str1), threads_per_request, 4);
  buffer_iter = coalesced_aligned_copy(buffer_iter, message, message_len, threads_per_request, 4);
  buffer_iter = coalesced_aligned_copy(buffer_iter, login_str2, d_strlen(login_str2), threads_per_request, 4);

  *buffer_iter = 0;
}

DEVICE void login_php(char* response, char* userid, char* is_match, int threads_per_request)
{
  if(*is_match) {
    int num_accts = 0;
    acct_info accts[10];
    backend_get_acct_balance(userid, accts, &num_accts);
    welcome_html(response, userid, accts, num_accts, threads_per_request);
  } else {
    login_html(response, "Incorrect user id or password!", 30, threads_per_request);
  }
}

DEVICE void check_password(char* userid, char* password, char* is_match)
{
  MD5_CTX context;
  unsigned char digest[16];
  char real_passwd[16];
  MD5_Init(&context);
  MD5_Update(&context, (unsigned char *)password, d_strlen(password));
  MD5_Final(digest, &context);
  backend_get_passwd(userid, real_passwd);
  *is_match = !d_strncmp((char*)digest, real_passwd, 16);
}

void process_post_data(char* data, char* userid, char* password, int n_requests)
{
  char *ssave = "";
  char* delimiters = "&";
  char* p1;
  int i;
  for(i=0; i<n_requests; i++) {
    p1 = strtok_r(data+i*DATA_LEN, delimiters, &ssave);
    while(p1 != NULL) {
      if(!strncmp(p1, "userid=", 7)) {
        strcpy(userid+i*USERID_LEN, p1 + 7);
      } else if(!strncmp(p1, "password=", 9)) {
        strcpy(password+i*PASSWD_LEN, p1 + 9);
      }
      p1 = strtok_r(NULL, delimiters, &ssave);
    }
  }
}
#endif
