#ifndef CHANGE_PROFILE_C_INLCUDED
#define CHANGE_PROFILE_C_INLCUDED

CONSTANT char* change_profile_padding;
CONSTANT int change_profile_padding_len = 0;

/* from the php source code */
DEVICE char* raw_url_encode(char const *s, int len, int *new_length, unsigned char *str)
{
 int x, y;
 unsigned char hexchars[] = "0123456789ABCDEF";

 for (x = 0, y = 0; len--; x++, y++) {
  str[y] = (unsigned char) s[x];

  if ((str[y] < '0' && str[y] != '-' && str[y] != '.') ||
  (str[y] < 'A' && str[y] > '9') ||
  (str[y] > 'Z' && str[y] < 'a' && str[y] != '_') ||
  (str[y] > 'z' && str[y] != '~')) {
   str[y++] = '%';
   str[y++] = hexchars[(unsigned char) s[x] >> 4];
   str[y] = hexchars[(unsigned char) s[x] & 15];
  }
 }
 str[y] = '\0';
 if (new_length) {
  *new_length = y;
 }
 return ((char *) str);
}

DEVICE void _get_change_profile_request(char* request, char* userid, profile* p)
{
 char* buffer_iter = request;
 unsigned char buf[128];
 APPEND_TO_BUFFER_TRANSPOSED("GET /fcgi-bin/besim_fcgi.fcgi?1&10&")
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(p->address, d_strlen(p->address), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(p->city, d_strlen(p->city), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(p->state, d_strlen(p->state), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(p->zip, d_strlen(p->zip), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(p->email, d_strlen(p->email), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED("&")
 raw_url_encode(p->phone, d_strlen(p->phone), NULL, buf);
 APPEND_TO_BUFFER_TRANSPOSED(buf)
 APPEND_TO_BUFFER_TRANSPOSED(" HTTP/1.1\r\nHost: localhost\r\n\r\n")
 *buffer_iter = 0;
}

DEVICE char* change_profile_html(char* response, int* response_len, char* userid, char* be_resp)
{
 char* buffer_iter = response;
 int diff;
 int laneId = threadIdx.x & 0x1f;
 int warpId = threadIdx.x & 0xffffffe0;
 SHARED int tile[THREAD_BLOCK_SIZE];
 char *ssave1 = "", *p1;
 
 APPEND_TO_BUFFER_TRANSPOSED(change_profile_str1)
 APPEND_TO_BUFFER_TRANSPOSED(userid)
 APPEND_TO_BUFFER_TRANSPOSED(change_profile_str2)
 APPEND_TO_BUFFER_TRANSPOSED("Profile Changed.")
 APPEND_TO_BUFFER_TRANSPOSED(change_profile_str3)
 p1 = d_strtok_r_transposed(be_resp, "\n", &ssave1);
 APPEND_TO_BUFFER_TRANSPOSED_2(p1)
 APPEND_TO_BUFFER_TRANSPOSED("</td></tr>\r\n")
 APPEND_WHITESPACE_PADDING()
 APPEND_TO_BUFFER_TRANSPOSED(change_profile_str4) 
 APPEND_TO_BUFFER_TRANSPOSED(change_profile_padding)
 APPEND_TO_BUFFER_TRANSPOSED(change_profile_str5)
 /* Terminate the string */
 *buffer_iter = 0;
 *response_len = (int)((buffer_iter - response)/COHORT_SIZE);
 return buffer_iter;
}

int _change_profile_php_cb_2(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  response_add(r->resp);
  cohort_free(r->cohort_id);
  request_handler_free(r);
  return false;
 }
 return true;
}

GLOBAL void change_profile_php_2(char* be_resp, int* req_state, char* resp, int* resp_len, session_bucket* session_array, int cohort_id)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 char scratch[10];
 char* buffer_iter = resp + tid;
 char* content_len_iter = NULL;
 int response_len = 0;
 
 if(req_state[tid] != REQ_STATE_CONTINUE) {
  return;
 }
 APPEND_TO_BUFFER_TRANSPOSED(response_header_str)
 APPEND_TO_BUFFER_TRANSPOSED("Content-Length: ")
 content_len_iter = buffer_iter;
 APPEND_TO_BUFFER_TRANSPOSED("          ")
 APPEND_TO_BUFFER_TRANSPOSED("\r\n\r\n")
 buffer_iter = change_profile_html(buffer_iter, &response_len, userid, process_backend_response_transposed(be_resp + tid, NULL));
 resp_len[tid] = (int)((buffer_iter - resp)/COHORT_SIZE);
 /* Insert content len here padded by whitespaces */
 buffer_iter = content_len_iter;
 d_itoa(response_len, scratch, 10, NULL);
 APPEND_TO_BUFFER_TRANSPOSED(scratch)
 req_state[tid] = REQ_STATE_CONTINUE;
}

void _change_profile_php_cb_1_continue(void* data)
{
 request_handler* r = (request_handler*)data;
 backend_copy_response_to_device(r->be, r->stream);
 change_profile_php_2<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_resp, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->d_session_array, r->cohort_id);
 callbacks_add(_change_profile_php_cb_2, r);
}

int _change_profile_php_cb_1(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  backend_process(r, _change_profile_php_cb_1_continue);
  return false;
 }
 return true;
}

GLOBAL void change_profile_php_1(char* be_req, int* req_state, char* resp, int* resp_len, int cohort_id, session_bucket* session_array, int* fds)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &d_cohorts[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 profile p;
 fds[tid] = r->client;
 
 if(userid == NULL) {
  login_html(resp + tid, resp_len + tid, "Please login");
  req_state[tid] = REQ_STATE_ERROR;
 } else {
  get_value(r, "address", 7, p.address);
  get_value(r, "city", 4, p.city);
  get_value(r, "state", 5, p.state);
  get_value(r, "zip", 3, p.zip);
  get_value(r, "phone", 5, p.phone);
  get_value(r, "email", 5, p.email);
 
  if(!d_strlen(p.address) || !d_strlen(p.city) || !d_strlen(p.state) || !d_strlen(p.zip) || !d_strlen(p.phone) || !d_strlen(p.email)) {
   message_html(resp + tid, resp_len + tid, "Profile update failed: you submitted empty fields");
   req_state[tid] = REQ_STATE_ERROR;
  } else {
   _get_change_profile_request(be_req + tid, userid, &p);
   req_state[tid] = REQ_STATE_CONTINUE;
  }
 }
}

void change_profile_php(request_handler* r)
{
 change_profile_php_1<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, r->stream>>>(r->be->d_transposed_req, r->d_req_state, r->resp->d_transposed_resp, r->resp->d_resp_len, r->cohort_id, r->d_session_array, r->resp->d_fds);
 backend_copy_requests_to_host(r->be, r->stream);
 callbacks_add(_change_profile_php_cb_1, r);
}

void change_profile_php_once()
{
 GENERATE_PADDING(change_profile)
}

#endif