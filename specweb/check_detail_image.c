#ifndef CHECK_DETAIL_IMAGE_C_INCLUDED
#define CHECK_DETAIL_IMAGE_C_INCLUDED

#include "backend/backend.c"

#define PATH_LEN 1024

typedef struct {
 char* image_path;
 char* d_image_path;
} check_detail_image_php_private;

void* check_detail_image_php_create(int n_req)
{
 check_detail_image_php_private* p = (check_detail_image_php_private*)malloc(sizeof(check_detail_image_php_private));
 p->image_path = (char*)malloc(n_req*PATH_LEN);
 check_cuda(cudaMalloc((void**)&p->d_image_path, n_req*PATH_LEN));
 return p;
}

void check_detail_image_php_destroy(void* data)
{
 check_detail_image_php_private* p = (check_detail_image_php_private*)data;
 check_cuda(cudaFree(p->d_image_path));
 free(p->image_path);
 free(p);
}

int _check_detail_image_php_cb_2(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  int n_req = r->n_req;
  int i;
  check_detail_image_php_private* p = (check_detail_image_php_private*)r->req_private;
  for(i=0; i<n_req; i++) {
   read_file(p->image_path+i*PATH_LEN, r->resp->resp + i*RESPONSE_LEN, r->resp->resp_len + i);
  }
  /* Free private state */
  check_detail_image_php_destroy(r->req_private);
  response_add(r->resp);
  cohort_free(r->cohorts, r->cohort_id);
  request_handler_destroy(r);
  return false;
 }
 return true;
}

GLOBAL void check_detail_image_php_2(cohort* c, char* be_resp, long int* be_resp_len, int* req_state, char* resp, long int* resp_len, session_bucket* session_array, int cohort_id, char* image_path)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &c[cohort_id].buffer[tid];
 char* temp;
 char acct_num[32];
 char front_image[1024];
 char back_image[1024];
 char side[32];
 char *ssave1 = "", *ssave2 = "", *p1, *p2;
 
 if(req_state[tid] != REQ_STATE_CONTINUE) {
  return;
 }
 get_value(r, "side", 4, side);
 temp = process_backend_response(be_resp + tid*BESIM_RESPONSE_LEN, NULL);
 p1 = d_strtok_r(temp, "\n", &ssave1);
 while(p1 != NULL) {
  p2 = d_strtok_r(p1, "&", &ssave2);
  d_strcpy(acct_num, p2);
  p2 = d_strtok_r(NULL, "&", &ssave2);
  d_strcpy(front_image, p2);
  p2 = d_strtok_r(NULL, "&", &ssave2);
  d_strcpy(back_image, p2);
  p1 = d_strtok_r(NULL, "\n", &ssave1);
 }
 if(!d_strncmp(side, "front", 5)) {
  d_strcpy(image_path + tid*PATH_LEN, d_strstr(front_image, "images"));
 } else {
  d_strcpy(image_path + tid*PATH_LEN, d_strstr(back_image, "images"));
 }
 req_state[tid] = REQ_STATE_CONTINUE;
}


int _check_detail_image_php_cb_1(void* data)
{
 request_handler* r = (request_handler*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  int n_req = r->n_req;
  check_detail_image_php_private* p = (check_detail_image_php_private*)r->req_private;
  backend_process(r->be);
  backend_copy_response_to_device(r->be, r->stream);
  check_detail_image_php_2<<<1, n_req, 0, r->stream>>>(r->cohorts->d_cohorts, r->be->d_resp, r->be->d_resp_len, r->d_req_state, r->resp->d_resp, r->resp->d_resp_len, r->d_session_array, r->cohort_id, p->d_image_path);
  check_cuda(cudaMemcpyAsync(p->image_path, p->d_image_path, n_req*PATH_LEN, cudaMemcpyDeviceToHost, r->stream));
  callbacks_add(_check_detail_image_php_cb_2, r);
  return false;
 }
 return true;
}

GLOBAL void check_detail_image_php_1(cohort* c, char* be_req, long int* be_req_len, int* req_state, char* resp, long int* resp_len, int cohort_id, session_bucket* session_array)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 request* r = &c[cohort_id].buffer[tid];
 char* userid = session_find(session_array, r->session_id);
 char check_no[32];
 char side[32];
 
 if(userid == NULL) {
  login_html(resp + tid*RESPONSE_LEN, resp_len + tid, "Please login", 12);
  req_state[tid] = REQ_STATE_ERROR;
 } else {
  if(!get_value(r, "check_no", 8, check_no) || !d_strlen(check_no)) {
   message_html(resp + tid*RESPONSE_LEN, resp_len + tid, "No empty check number allowed", 29);
   req_state[tid] = REQ_STATE_ERROR;
  } else if(!get_value(r, "side", 4, side) || (d_strncmp(side, "front", 5) && d_strncmp(side, "back", 4))) {
   message_html(resp + tid*RESPONSE_LEN, resp_len + tid, "Wrong side", 10);
   req_state[tid] = REQ_STATE_ERROR;
  } else {
   _get_query_check_request(be_req + tid*BESIM_REQUEST_LEN, userid, check_no, be_req_len + tid);
   req_state[tid] = REQ_STATE_CONTINUE;
  }
 }
}

void check_detail_image_php(request_handler* r)
{
 int n_req = r->n_req;
 check_cuda(cudaMemsetAsync(r->d_req_state, REQ_STATE_ERROR, n_req*sizeof(int), r->stream));
 check_detail_image_php_1<<<1, n_req, 0, r->stream>>>(r->cohorts->d_cohorts, r->be->d_req, r->be->d_req_len, r->d_req_state, r->resp->d_resp, r->resp->d_resp_len, r->cohort_id, r->d_session_array);
 backend_copy_requests_to_host(r->be, r->stream);
 check_cuda(cudaMemcpyAsync(r->req_state, r->d_req_state, n_req*sizeof(int), cudaMemcpyDeviceToHost, r->stream));
 callbacks_add(_check_detail_html_php_cb_1, r);
}

#endif
