#ifndef MESSAGE_C_INCLUDED
#define MESSAGE_C_INCLUDED

DEVICE void message_html(char* response, int* response_len, char* message)
{
 char* buffer_iter = response;
 APPEND_TO_BUFFER_TRANSPOSED(message_str1)
 APPEND_TO_BUFFER_TRANSPOSED(message)
 APPEND_TO_BUFFER_TRANSPOSED(message_str2)
 *buffer_iter = 0;
 *response_len = (int)((buffer_iter - response)/COHORT_SIZE);
}

#endif
