#ifndef INIT_C_INCLUDED
#define INIT_C_INCLUDED

char init_str1[] = 
"<html>\r\n"
"        <head>\r\n"
"                <title>SPECweb2009 Ecommerce Workload Init</title>\r\n"
"        </head>\r\n"
"        <body>\r\n";

int len_init_str1 = 125;

char init_str2[] = 
"        </body>\r\n"
"</html>\r\n";

int len_init_str2 = 26;

char init_query_str[] = 
"BESIM_HOST=besimhost1+besimhost2&BESIM_PORT=81+82&BESIM_URI=/BeSim/specweb2009_besim&BESIM_PERSISTENT=true&PADDING_DIR=/data/SPECweb2009/bank/dynamic_padding/&SMARTY_DIR=/data/SPECweb2009/assl/Smarty2.6.2/libs/&SMARTY_SUPPORT_DIR=/data/SPECweb2009/support/&SEND_CONTENT_LENGTH=true";

int len_init_query_str = 281;

void init_html(char* response, long int* response_len)
{
 char scratch[256] = {0};
 char* buffer_iter = response;
  APPEND_TO_BUFFER(init_str1, len_init_str1)
 APPEND_TO_BUFFER("<P>SERVER_SOFTWARE = cohorthttpd</P>\r\n", 38)
 APPEND_TO_BUFFER("<P>REMOTE_ADDR = 152.3.137.78</P>\r\n", 35)
 APPEND_TO_BUFFER("<P>SCRIPT_NAME = ", 17)
 APPEND_TO_BUFFER("/bank/init.php", 14)
 APPEND_TO_BUFFER("</P>\r\n", 6)
 APPEND_TO_BUFFER("<P>QUERY_STRING = ", 18)
 APPEND_TO_BUFFER(init_query_str, len_init_query_str)
 APPEND_TO_BUFFER("</P>\r\n", 6)
 APPEND_TO_BUFFER("<P>SERVER_TIME = ", 17)
 sprintf(scratch, "%lu", time(NULL)*1000);
 APPEND_TO_BUFFER(scratch, strlen(scratch))
 APPEND_TO_BUFFER("</P>\r\n", 6)
 APPEND_TO_BUFFER(init_str2, len_init_str2)
 *buffer_iter = 0;
 *response_len = (int)(buffer_iter - response);
}

void init_php(char* resp, long int* resp_len)
{
 init_html(resp, resp_len);
}
#endif
