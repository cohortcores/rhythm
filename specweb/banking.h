#ifndef BANKING_H_INCLUDED
#define BANKING_H_INCLUDED

#define REQ_STATE_ERROR 0
#define REQ_STATE_CONTINUE 1

typedef struct _acct_info {
 char acct_num[32];
 int acct_type;
 char balance[32];
 char ttl_dep_balance[32];
 char avg_dep_balance[32];
 char ttl_wthdrw_balance[32];
 char avg_wthdrw_balance[32];
} acct_info;

typedef struct _payee_info {
 char payee_id[32];
 char date[32];
 char payment[32];
} payee_info;

typedef struct _profile {
 char address[32];
 char city[32];
 char state[32];
 char zip[32];
 char phone[32];
 char email[32]; 
} profile;

#endif