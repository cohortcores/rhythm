#ifndef SESSION_C_INCLUDED
#define SESSION_C_INCLUDED

/* Creates a session array of */
session_bucket* session_init(int num_buckets)
{
 session_bucket* d_array;
 d_array = (session_bucket*)alloc_device(num_buckets*sizeof(session_bucket));
 check_cuda(cudaMemset(d_array, 0, num_buckets*sizeof(session_bucket)));
 return d_array;
}

DEVICE void _find_free_session_node(session_bucket* bucket, session_node** node, unsigned int* id, int random_seed)
{
 int iter = random_seed;
 int temp = SESSION_NODE_FREE;
 do {
  temp = atomicCAS(&bucket->nodes[iter].state, SESSION_NODE_FREE, SESSION_NODE_USED);
  iter++;
  iter &= (MAX_SESSIONS_PER_BUCKET-1);
 } while (temp != SESSION_NODE_FREE && iter != random_seed);
 if(iter != random_seed) {
  *node = &bucket->nodes[iter-1];
  *id = iter - 1;
 }
}

/* A valid session id is always greater than 0 */
DEVICE unsigned int session_create(session_bucket* d_array, int tid, char* userid)
{
 unsigned int session_id = tid*MAX_SESSIONS_PER_BUCKET;
 session_bucket* bucket = &d_array[tid];
 session_node* s = NULL;
 unsigned int id = 0;
 int random_seed = d_atoi(userid) & (MAX_SESSIONS_PER_BUCKET-1);
 _find_free_session_node(bucket, &s, &id, random_seed);
 if(s == NULL) {
  return 0;
 }
 session_id += id + 1;
 d_strcpy(s->userid, userid);
 return session_id;
}

DEVICE char* session_find(session_bucket* d_array, int session_id)
{
 session_node* node = &d_array[(int)(session_id/MAX_SESSIONS_PER_BUCKET)].nodes[(int)(session_id & (MAX_SESSIONS_PER_BUCKET-1))-1];
 if(node->state == SESSION_NODE_USED) {
  return node->userid;
 }
 return NULL;
}

DEVICE void session_destroy(session_bucket* d_array, int session_id)
{
 session_node* node = &d_array[(int)(session_id/MAX_SESSIONS_PER_BUCKET)].nodes[(int)(session_id & (MAX_SESSIONS_PER_BUCKET-1))-1];
 /* commented out as the test harness cannot generate unique session ids for now */
 //atomicCAS(&node->state, SESSION_NODE_USED, SESSION_NODE_FREE);
}

#endif