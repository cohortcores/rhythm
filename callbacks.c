#if !defined(CALLBACKS_C_INCLUDED)
#define CALLBACKS_C_INCLUDED

typedef struct _cbnode {
 struct _cbnode* next;
 int (*f)(void*);
 void* data;
 long enqueue_time;
}_cbn;

typedef struct {
 struct _cbnode* head;
 struct _cbnode* tail;
 int count;
} _cblist;

_cblist callbacks;

void _cb_insert(_cblist* l, int (*f)(void*), void* data)
{
 struct _cbnode* p;
 assert(l != NULL);
 p = (struct _cbnode*)alloc_host(sizeof(struct _cbnode));
 p->f = f;
 p->data = data;
 p->next = NULL;
 /* empty list */
 if(l->count == 0) {
  l->head = p;
 } else {
  l->tail->next = p;
 }
 l->tail = p;
 l->count++;
}

void* _cb_remove(_cblist* l, int (*f)(void*))
{
 assert(l != NULL);
 assert(l->head != NULL);
 struct _cbnode* current = l->head;
 struct _cbnode* before_current = NULL;
 void* data = NULL;
 while(current) {
  if(current->f == f) {
   if(current == l->head) {
    l->head = l->head->next;
   } else {
    before_current->next = current->next;
   }
   if(current == l->tail) {
    l->tail = before_current;
   }
   data = current->data;
   free_host(current);
   l->count--;
   break;
  }
  before_current = current;
  current = current->next;
 }
 return data;
}

int _cb_count(_cblist* l)
{
 assert(l != NULL);
 return l->count;
}

void callbacks_init()
{
 callbacks.head = NULL;
 callbacks.tail = NULL;
 callbacks.count = 0;
}

/* If you are returning false from a callback, make sure you free the data yourself */
void callbacks_process()
{
 nvtxEventAttributes_t eventAttrib = {0};
 eventAttrib.version = NVTX_VERSION;
 eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
 eventAttrib.colorType = NVTX_COLOR_ARGB;
 eventAttrib.color = 0xFF0000FF;
 eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;
 eventAttrib.message.ascii = "Callbacks process";
 struct _cbnode* current = callbacks.head;
 struct _cbnode* before_current = NULL;
 while(current) {
  nvtxMarkEx(&eventAttrib);
  if((*current->f)(current->data) == false) {
   struct _cbnode* temp = current;
   if(current == callbacks.head) {
    callbacks.head = callbacks.head->next;
   } else {
    before_current->next = current->next;
   }
   if(current == callbacks.tail) {
    callbacks.tail = before_current;
   }
   current = current->next;
   free_host(temp);
   callbacks.count--;
  } else {
   before_current = current;
   current = current->next;
  }
 }
}

void callbacks_wait()
{
 while(_cb_count(&callbacks)) {
  callbacks_process();
 }
}

void callbacks_add(int (*f)(void*), void* data)
{
 assert(f != NULL);
 _cb_insert(&callbacks, f, data);
}

void callbacks_remove(int (*f)(void*))
{
 assert(f != NULL);
 _cb_remove(&callbacks, f);
}

#endif