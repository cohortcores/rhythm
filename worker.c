#ifndef WORKER_C_INCLUDED
#define WORKER_C_INCLUDED

inline void raise_event(char type, void* data);
#define MAX_EVENTS 10000
#if defined(_READER_THREAD_)
#define EVENT_LOOP_TIMEOUT 1 /* ms */
#else
#define EVENT_LOOP_TIMEOUT 0 /* ms */
#endif

int request_count = 0;
int pipefds[2];
int pipefds_server2backend1[2];
int pipefds_server2backend2[2];
int pipefds_backend2server[2];
/* Reader */
reader_data* reader_front = NULL;
reader_data** readers = NULL;
/* Parser */
parser_data** parsers = NULL;
/* Cohorts */
DEVICE cohort* d_cohorts;
cohort_image* d_cohorts_image = NULL;
cohort_image* h_cohorts_image = NULL;
cohort_data* h_cohorts_state = NULL;
cohort_data* d_cohorts_state = NULL;
/* Sessions */
session_bucket* sessions = NULL;
/* Responses */
response_data** responses = NULL;
/* Request handlers */
request_handler** handlers = NULL;
/* Images */
char* d_image_names = NULL;

pthread_mutex_t readerMutex = PTHREAD_MUTEX_INITIALIZER;

#include "memory.c"
#include "callbacks.c"
#include "cohort.c"
#include "session.c"
#include "utils/profiler.c"
#include "backend/backend.c"
#include "pipeline/response.c"
#include "pipeline/request_handler.c"
#include "specweb/specweb.c"
#include "pipeline/dispatch.c"
#include "specweb/init.c"
#include "pipeline/reader.c"
#include "pipeline/parser.c"

int init_hack = false;
char init_header_str[] = 
"HTTP/1.1 200 OK\r\n"
"Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n"
"Server: cohorthttpd/0.1.0\r\n"
"Content-Type: text/html\r\n"
"Cache-Control: no-cache\r\n";

int len_init_header_str = 131;

void handle_client_request(int fd)
{
 if(init_hack == false) {
  char buf[REQ_BUF_SIZE];
#if defined(_TRACE_USE_MODE_)
  int bytes_read = 0;
  size_t result;
  result = fread(&bytes_read, sizeof(int), 1, trace_size_fd);
  assert(result == 1);
  result = fread(buf, 1, bytes_read, trace_buf_fd);
  assert(result == bytes_read);
#else
  char header[RESPONSE_HEADERS_LEN];
  char resp[RESPONSE_LEN];
  char scratch[20] = {0};
  long int resp_len = 0;
  long int header_len = 0;
  char* buffer_iter = header;
  read(fd, buf, REQ_BUF_SIZE);
  init_php(resp, &resp_len);
  APPEND_TO_BUFFER(init_header_str, len_init_header_str)
  APPEND_TO_BUFFER("Content-Length: ", 16)
  sprintf(scratch, "%ld", resp_len);
  APPEND_TO_BUFFER(scratch, strlen(scratch))
  APPEND_TO_BUFFER("\r\n\r\n", 4)
  header_len = (long int)(buffer_iter - header);
  if(write(fd, header, header_len) != header_len) {
   error_die("write");
  }
  if(write(fd, resp, resp_len) != resp_len) {
   error_die("write");
  }
#endif  
  init_hack = true;
  return;
 }
 if(reader_front->state != READER_FULL) {
  reader_read_requests(fd, reader_front);
 }
 if(reader_front->state == READER_FULL) {
  nvtxMarkA("Reader full");
  parser_data* p = parser_find_free(parsers);
  reader_data* r = reader_find_free(readers);
  if(p != NULL && r != NULL && (response_find_free(responses) != NULL)) {
   reader_swap(&reader_front, &r);
   /* clear the buffer for the next set of requests */
   reader_free(reader_front);
   /* Launch asynchronous parser kernels */
   parser_main(p, r);
  } /*else {
   if(r == NULL) {
    fprintf(stderr, "\nNot enough readers");
   }
   if(p == NULL) {
    fprintf(stderr, "\nNot enough parsers");
   }
  }*/
 }
}

void handle_event(char type)
{
 nvtxEventAttributes_t eventAttrib = {0};
 eventAttrib.version = NVTX_VERSION;
 eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
 eventAttrib.colorType = NVTX_COLOR_ARGB;
 eventAttrib.color = 0xFF00FF00;
 eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;
 eventAttrib.message.ascii = "Reader full";
 switch(type) {
  case READER_FULL:
   {
    nvtxMarkEx(&eventAttrib);
    parser_data* p = parser_find_free(parsers);
    reader_data* r = reader_find_free(readers);
    if(p != NULL && r != NULL) {
     pthread_mutex_lock(&readerMutex);
     reader_swap(&reader_front, &r);
     /* clear the buffer for the next set of requests */
     reader_free(reader_front);
     pthread_mutex_unlock(&readerMutex);
     /* Launch asynchronous parser kernels */
     parser_main(p, r);
    }
   }
   break;
 }
}

inline void raise_event(char type, void* data)
{
 if(write(pipefds[1], &type, 1) != 1) {
  error_die("write");
 }
 if(data != NULL) {
  if(write(pipefds[1], &data, sizeof(void*)) != sizeof(void*)) {
   error_die("write");
  }
 }
}

void worker_event_loop(int server_sock, int epollfd)
{
 int client_sock = -1;
 struct sockaddr_in client_name;
 size_t client_name_len = sizeof(client_name);
 struct epoll_event event;
 struct epoll_event *events;
 long int start_time, end_time;
 int i, n;
 nvtxEventAttributes_t eventAttrib = {0};
 eventAttrib.version = NVTX_VERSION;
 eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
 eventAttrib.colorType = NVTX_COLOR_ARGB;
 eventAttrib.color = 0xFFFF0000;
 eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;
 eventAttrib.message.ascii = "Main loop entry";
#if defined(_THREADED_BACKEND_) 
 backend_thread_init();
#endif  
#if defined(_READER_THREAD_)
 /* Create the reader thread after creating the pipe */
 reader_thread_create();
#endif
 events = (struct epoll_event*)calloc(MAX_EVENTS, sizeof(struct epoll_event));
 nvtxNameOsThread(pthread_self(), "Main thread");
 start_time = profiler_get_time();
 while(1) {
  //nvtxMarkEx(&eventAttrib);
  n = epoll_wait(epollfd, events, MAX_EVENTS, EVENT_LOOP_TIMEOUT);
  //nvtxMarkEx(&eventAttrib);
  for (i=0; i<n; i++) {
   if (events[i].events & EPOLLERR || events[i].events & EPOLLHUP || !(events[i].events & EPOLLIN)) {
    close(events[i].data.fd);
   } else if(events[i].data.fd == server_sock) {
    /* new client */
    while(1) {
     client_sock = accept(server_sock, (struct sockaddr*)&client_name, (socklen_t*)&client_name_len);
     if(client_sock == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
      break;
     }
     //make_socket_non_blocking(client_sock);
     event.data.fd = client_sock;
     event.events = EPOLLIN;
     epoll_ctl(epollfd, EPOLL_CTL_ADD, client_sock, &event);
    }
   } else if(events[i].data.fd == pipefds[0]) {
    char ch;
    while(read(events[i].data.fd, &ch, 1) > 0) {
     handle_event(ch);
    }
   } else if(events[i].data.fd == pipefds_backend2server[0]) {
    int id;
    while(read(events[i].data.fd, &id, 4) > 0) {
     request_handler* r = request_handler_get_reference(id);
     r->cb(r);
    }
   } else {
    /* client request */
    handle_client_request(events[i].data.fd);
   }
  }
#if defined(_TRACE_USE_MODE_) || defined(_REQUEST_GENERATE_MODE_)
  /* Works because of the epoll timeout */
#if !defined(_READER_THREAD_)
  if(request_count < TRACE_MAX_NUM) {
   handle_client_request(0);
  }
#endif
  if(request_count == TRACE_MAX_NUM) {
   callbacks_wait();
   if(response_find_busy(responses) == NULL) {
    break;
   }
  }
#endif
  /* timeout? */
  callbacks_process();
 }
 end_time = profiler_get_time();
 fprintf(stderr, "\nTime to process %d requests of type %d is %ld ms, giving a throughput of %ldK reqs/sec\n", TRACE_MAX_NUM, _REQUEST_TYPE_, (end_time - start_time)/1000, (long int)((long int)TRACE_MAX_NUM*1000/(end_time - start_time)));
 free(events);
}

int worker_init(int server_sock)
{
 size_t stack_size = 0;
 size_t heap_size = 0;
 check_cuda(cudaSetDevice(CUDA_DEVICE));
 cudaDeviceReset();
 check_cuda(cudaDeviceSetLimit(cudaLimitMallocHeapSize, 512*1024*1024));
 check_cuda(cudaDeviceSetLimit(cudaLimitStackSize, 16*1024));
 check_cuda(cudaDeviceGetLimit(&stack_size, cudaLimitStackSize));
 check_cuda(cudaDeviceGetLimit(&heap_size, cudaLimitMallocHeapSize));
 fprintf(stderr, "\nStack size is %ld bytes", stack_size);
 fprintf(stderr, "\nHeap size is %ld bytes", heap_size);
 profiler_init();
 memory_reset_record();
#if defined(_TRACE_USE_MODE_)
 trace_buf_fd = fopen(TRACE_BUF_FILE,"r");
 trace_size_fd = fopen(TRACE_SIZE_FILE,"r");
#elif defined(_TRACE_GENERATE_MODE_)
 trace_buf_fd = fopen(TRACE_BUF_FILE,"w");
 trace_size_fd = fopen(TRACE_SIZE_FILE,"w"); 
#endif
 cohorts_create();
 sessions = session_init(COHORT_SIZE);
 readers = reader_create_pool(COHORT_SIZE, READER_NUM_BACK_BUFFERS);
 /* Assign a reader to be the front */
 reader_front = reader_find_free(readers);
 parsers = parser_create_pool(NUM_PARSERS);
 responses = response_create_pool(COHORT_SIZE, NUM_RESPONSES);
 handlers = request_handler_create_pool(COHORT_SIZE, NUM_REQUEST_HANDLERS, sessions);
 dispatch_create();
 d_image_names = (char*)alloc_device(MAX_IMAGES*256);
 copy_to_device(d_image_names, image_names, MAX_IMAGES*256);
 memory_print_record();
 print_device_memory();
 
 int epollfd;
 struct epoll_event event;
 epollfd = epoll_create(1);
 event.data.fd = server_sock;
 event.events = EPOLLIN;
 epoll_ctl(epollfd, EPOLL_CTL_ADD, server_sock, &event);
 
 if(pipe2(pipefds, O_NONBLOCK) == -1) {
  error_die("pipe2");
 }
 if(pipe2(pipefds_server2backend1, 0) == -1) {
  error_die("pipe2");
 }
 if(pipe2(pipefds_server2backend2, 0) == -1) {
  error_die("pipe2");
 }
 if(pipe2(pipefds_backend2server, O_NONBLOCK) == -1) {
  error_die("pipe2");
 }

 event.data.fd = pipefds[0];
 event.events = EPOLLIN;
 epoll_ctl(epollfd, EPOLL_CTL_ADD, pipefds[0], &event);
 
 event.data.fd = pipefds_backend2server[0];
 event.events = EPOLLIN;
 epoll_ctl(epollfd, EPOLL_CTL_ADD, pipefds_backend2server[0], &event);
 
#if defined(_REQUEST_GENERATE_MODE_)
 init_hack = true;
#endif

 return epollfd;
}
#endif
