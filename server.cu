#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>
#include <cuda_profiler_api.h>
#include <nvToolsExt.h>
#include <nvToolsExtCuda.h>
#include <nvToolsExtCudaRt.h>

#if !defined(__CUDACC__)
#define __align__(n) __attribute__ ((aligned (n)))
#define DEVICE 
#endif

    char *PayeesId[10] =
        {"Electric", "Mortgage", "CreditCard", "Oil", "Cable",
        "Insurance", "Telephone", "Wireless", "Auto", "IRS"};
    char *Payees[10] =
        {"Electric Co.", "Mortgage Inc.", "CreditCard", "Oil Co.", "Cable",
        "Insurance Inc.", "Telephone Co.", "Wireless Inc.", "Auto Inc.", "IRS"};
    char *Payments[10] =
        {"100.00", "1273.42", "20.00", "120.00", "40.00",
        "179.28", "29.00", "39.99", "207.00", "250.00"};
    char *Street[10] = 
	{"Aspen Rd", "Birch Ln", "CloudTree Ave", "Dogwood Dr", "Elm St",
		"Heath Way", "Linden Blvd", "Magnolia Ave", "Olive Cirle", "Palm way"};
    char *City[10] =
	{"Northford", "Easton", "Sudbury", "Westville", "Middletown",
        "Portmouth", "Bridgewater", "Littleton", "Wayland", "Upton"};
    char *State[10] =
	{"PA", "OH", "NJ", "MD", "DE", 
	"VA", "NC", "SC", "TN", "WV"};
    char *Zip[10] =
	{"01812", "14112", "13821", "02012", "01934",
	"01732", "01431", "03821", "05014", "01967"};
    char *Phone[10] =
	{"123-234-3456", "223-334-4456", "333-445-5678", "414-516-7271",
        "555-221-0987", "800-888-1223", "866-912-8765", "777-667-1256",
	"929-877-4929", "877-392-2855"};

    
#define RANDOM_REQ_COUNT 131072

char random_requests[RANDOM_REQ_COUNT][REQ_BUF_SIZE];
int random_requests_len[RANDOM_REQ_COUNT];

void error_die(const char *sc)
{
 perror(sc);
 //exit(1);
}

void make_socket_non_blocking(int fd);

#include "list.c"
#include "common.c"
#include "worker.c"

void init_random_session_array()
{
 session_bucket* bucket = (session_bucket*)alloc_pinned_host(sizeof(session_bucket));
 int i, j;
 srand(0);
 for(i=0; i<COHORT_SIZE; i++) {
  for(j=0; j<MAX_SESSIONS_PER_BUCKET; j++) {
   int userid = rand()%50000 + 1;
   bucket->nodes[j].state = SESSION_NODE_USED;
   sprintf(bucket->nodes[j].userid, "%d", userid);
  }
  copy_to_device(sessions+i, bucket, sizeof(session_bucket));
 }
 free_pinned_host(bucket);
}

void init_random_requests(int uri)
{
 int i;
 char query_string[512];
 int query_string_len;
 srand(0);
 for(i=0; i<RANDOM_REQ_COUNT; i++) {
  switch(uri) {
   case REQUEST_URI_SPECWEB_BANKING_LOGIN:
    {
     int userid = rand()%50000 + 1;
     query_string_len = sprintf(query_string, "userid=%d&password=%d", userid, userid);
     random_requests_len[i] = sprintf(random_requests[i], "POST /bank/login.php HTTP/1.1\r\nContent-type: application/x-www-form-urlencoded\r\nAccept: */*\r\nHost: 152.3.137.78\r\nContent-Length: %d\r\n\r\n%s", query_string_len, query_string);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_ACCOUNT_SUMMARY:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/account_summary.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_ADD_PAYEE:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/add_payee.php?userid=0000000079 HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_BILL_PAY:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/bill_pay.php?userid=11851 HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_INPUT:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/bill_pay_status_input.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_BILL_PAY_STATUS_OUTPUT:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/bill_pay_status_output.php?userid=15723&start=2013-05-9&end=2013-07-09 HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_CHANGE_PROFILE:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     int x = (session_id%10);
     query_string_len = sprintf(query_string, "address=%d+%s&city=%s&state=%s&zip=%s&phone=%s&email=customer%d@isp.net", (session_id%10000)+1, Street[x], City[x], State[x], Zip[x], Phone[x], (session_id%1000)+1);
     random_requests_len[i] = sprintf(random_requests[i], "POST /bank/change_profile.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nContent-type: application/x-www-form-urlencoded\r\nAccept: */*\r\nHost: 152.3.137.78\r\nContent-Length: %d\r\n\r\n%s", session_id, query_string_len, query_string);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_HTML:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/check_detail_html.php?check_no=%d HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", (session_id%10)+1, session_id);
    }
    break;
  case REQUEST_URI_SPECWEB_BANKING_CHECK_DETAIL_INPUT:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/check_detail_input.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_ORDER_CHECK:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/order_check.php?userid=1980 HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_PLACE_CHECK_ORDER:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     query_string_len = sprintf(query_string, "userid=9772&check_design=%d&number=%d&account_no=%010d", (session_id%10)+1, (session_id%1000)+1, session_id);
     random_requests_len[i] = sprintf(random_requests[i], "POST /bank/place_check_order.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nContent-type: application/x-www-form-urlencoded\r\nAccept: */*\r\nHost: 152.3.137.78\r\nContent-Length: %d\r\n\r\n%s", session_id, query_string_len, query_string);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_POST_PAYEE:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     int x = (session_id%10);
     query_string_len = sprintf(query_string, "payee_id=%d&name=%s&street=%d+%s&city=%s&state=%s&zip=%s&phone=%s", x, PayeesId[x], (session_id%10000)+1, Street[x], City[x], State[x], Zip[x], Phone[x]);
     random_requests_len[i] = sprintf(random_requests[i], "POST /bank/post_payee.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nContent-type: application/x-www-form-urlencoded\r\nAccept: */*\r\nHost: 152.3.137.78\r\nContent-Length: %d\r\n\r\n%s", session_id, query_string_len, query_string);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_POST_TRANSFER:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     query_string_len = sprintf(query_string, "from=%010d&to=%010d&amount=%d", session_id, session_id*2, (session_id%100));
     random_requests_len[i] = sprintf(random_requests[i], "POST /bank/post_transfer.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nContent-type: application/x-www-form-urlencoded\r\nAccept: */*\r\nHost: 152.3.137.78\r\nContent-Length: %d\r\n\r\n%s", session_id, query_string_len, query_string);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_PROFILE:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/profile.php?userid=18225 HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_TRANSFER:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/transfer.php HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
   case REQUEST_URI_SPECWEB_BANKING_LOGOUT:
    {
     int session_id = rand()%(COHORT_SIZE*MAX_SESSIONS_PER_BUCKET) + 1;
     random_requests_len[i] = sprintf(random_requests[i], "GET /bank/logout.php?userid=1080 HTTP/1.1\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nCookie: SESSIONID=%d\r\nAccept: */*\r\nHost: 152.3.137.78\r\n\r\n", session_id);
    }
    break;
  }
 }
}

void make_socket_non_blocking(int fd)
{
 int flags;
 flags = fcntl(fd, F_GETFL, 0);
 if (flags == -1) {
  error_die("fcntl");
 }
 flags |= O_NONBLOCK;
 if(fcntl(fd, F_SETFL, flags) == -1) {
  error_die("fcntl");
 }
}

int startup(u_short *port)
{
 int httpd = 0;
 struct sockaddr_in name;

 httpd = socket(PF_INET, SOCK_STREAM, 0);
 if (httpd == -1)
  error_die("socket");
 memset(&name, 0, sizeof(name));
 name.sin_family = AF_INET;
 name.sin_port = htons(*port);
 name.sin_addr.s_addr = htonl(INADDR_ANY);
 if (bind(httpd, (struct sockaddr *)&name, sizeof(name)) < 0)
  error_die("bind");
 make_socket_non_blocking(httpd);
 if (listen(httpd, 1024) < 0)
  error_die("listen");
 return(httpd);
}

int main(void)
{
 int server_sock = -1;
 u_short port = 8080;
 int epollfd = 0;
 
 read_images();
 server_sock = startup(&port);
 fprintf(stderr, "\nhttpd running on port %d", port);
 epollfd = worker_init(server_sock);
 int uri = _REQUEST_TYPE_;
 if(uri != REQUEST_URI_SPECWEB_BANKING_LOGIN) {
  init_random_session_array();
  fprintf(stderr, "\nFinished generating random session array", port);
 }
 init_random_requests(uri);
 fprintf(stderr, "\nFinished generating random requests", port);
 worker_event_loop(server_sock, epollfd);
 /* Called on program exit for accurate profiling */
 cudaDeviceSynchronize();
 cudaDeviceReset();
 close(server_sock);
 
 return(0);
}
