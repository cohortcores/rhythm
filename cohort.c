#ifndef COHORT_C_INCLUDED
#define COHORT_C_INCLUDED

char recycle_list[COHORTS_NUM];
char recycle_list_images[COHORTS_IMAGES_NUM];

void cohorts_create()
{
 cohort* h_d_cohorts = (cohort*)alloc_device(COHORTS_NUM*sizeof(cohort));
 check_cuda(cudaMemcpyToSymbol(d_cohorts, &h_d_cohorts, sizeof(cohort*)));
 d_cohorts_image = (cohort_image*)alloc_device(COHORTS_IMAGES_NUM*sizeof(cohort_image));
 h_cohorts_image = (cohort_image*)alloc_pinned_host(COHORTS_IMAGES_NUM*sizeof(cohort_image));
 h_cohorts_state = (cohort_data*)alloc_pinned_host(sizeof(cohort_data));
 d_cohorts_state = (cohort_data*)alloc_device(sizeof(cohort_data));
 check_cuda(cudaMemset(d_cohorts_state, 0, sizeof(cohort_data)));
 memset(h_cohorts_state, 0, sizeof(cohort_data));
 memset(recycle_list, 0, COHORTS_NUM);
 memset(recycle_list_images, 0, COHORTS_IMAGES_NUM);
}

DEVICE request* cohort_add(cohort_state* state, int uri)
{
 unsigned int count = 0, i;
 int success = false;
 request* r = NULL;
 /* Look for a partially filled cohort of same uri */
 for(i=0; i<COHORTS_NUM; i++) {
  if(state[i].uri == uri && state[i].state == COHORT_PARTIALLY_FULL) {
   /* All threads get assigned a unique slot between 1 and RANDOM_MAX_NUMBER, we only pick 
   "valid" slots between 1-COHORT_SIZE */
   count = atomicInc((unsigned int*)&state[i].count, RANDOM_MAX_NUMBER);
   if(count < COHORT_SIZE) {
    /* successfully found a slot in a partially filled cohort */
    r = &d_cohorts[i].buffer[count];
    success = true;
    if(count == COHORT_SIZE-1) {
      /* all slots in cohort are filled */
      state[i].state = COHORT_READY;
     }
    break;
   }
  } 
 }
 if(success == false) {
  /* Occupy a new cohort */
  for(i=0; i<COHORTS_NUM; i++) {
   int old_uri = atomicCAS(&state[i].uri, REQUEST_URI_NONE, uri);
   if((old_uri == REQUEST_URI_NONE || old_uri == uri) && state[i].count < COHORT_SIZE) {
    count = atomicInc((unsigned int*)&state[i].count, RANDOM_MAX_NUMBER);
    if(count == 0) {
     state[i].state = COHORT_PARTIALLY_FULL;
     r = &d_cohorts[i].buffer[count];
     /* Re-check count here as cohort may already be full */
     if(state[i].count == COHORT_SIZE) {
      state[i].state = COHORT_READY;
     }
     success = true;
     break;
    } else if(count < COHORT_SIZE) {
     /* successfully found a slot in a partially filled cohort */
     r = &d_cohorts[i].buffer[count];
     success = true;
     if(count == COHORT_SIZE-1) {
      /* all slots in cohort are filled */
      state[i].state = COHORT_READY;
     }
     break;
    }
   }
  }
 }
 assert(success == true);
 return r;
}

DEVICE request_image* cohort_add_image(cohort_image* c, cohort_state* state, int uri)
{
 unsigned int count = 0, i;
 int success = false;
 request_image* r = NULL;
 /* Look for a partially filled cohort of same uri */
 for(i=0; i<COHORTS_IMAGES_NUM; i++) {
  if(state[i].uri == uri && state[i].state == COHORT_PARTIALLY_FULL) {
   /* All threads get assigned a unique slot between 1 and RANDOM_MAX_NUMBER, we only pick 
   "valid" slots between 1-256 */
   count = atomicInc((unsigned int*)&state[i].count, RANDOM_MAX_NUMBER);
   if(count < COHORT_SIZE) {
    /* successfully found a slot in a partially filled cohort */
    r = &c[i].buffer[count];
    success = true;
    if(count == COHORT_SIZE-1) {
      /* all slots in cohort are filled */
      state[i].state = COHORT_READY;
     }
    break;
   }
  } 
 }
 if(success == false) {
  /* Occupy a new cohort */
  for(i=0; i<COHORTS_IMAGES_NUM; i++) {
   int old_uri = atomicCAS(&state[i].uri, REQUEST_URI_NONE, uri);
   if(old_uri == REQUEST_URI_NONE || old_uri == uri) {
    count = atomicInc((unsigned int*)&state[i].count, RANDOM_MAX_NUMBER);
    if(count == 0) {
     state[i].state = COHORT_PARTIALLY_FULL;
     r = &c[i].buffer[count];
     /* Re-check count here as cohort may already be full */
     if(state[i].count == COHORT_SIZE) {
      state[i].state = COHORT_READY;
     }
     success = true;
     break;
    } else if(count < COHORT_SIZE) {
     /* successfully found a slot in a partially filled cohort */
     r = &c[i].buffer[count];
     success = true;
     if(count == COHORT_SIZE-1) {
      /* all slots in cohort are filled */
      state[i].state = COHORT_READY;
     }
     break;
    }
   }
  }
 }
 assert(success == true);
 return r;
}

void cohort_free(int id)
{
 recycle_list[id] = 1;
}

void cohort_free_image(int id)
{
 recycle_list_images[id] = 1;
}

#endif