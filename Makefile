################################################################################
#
# Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
#
# NOTICE TO USER:   
#
# This source code is subject to NVIDIA ownership rights under U.S. and 
# international Copyright laws.  
#
# NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE 
# CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR 
# IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH 
# REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF 
# MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.   
# IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, 
# OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS 
# OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
# OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
# OR PERFORMANCE OF THIS SOURCE CODE.  
#
# U.S. Government End Users.  This source code is a "commercial item" as 
# that term is defined at 48 C.F.R. 2.101 (OCT 1995), consisting  of 
# "commercial computer software" and "commercial computer software 
# documentation" as such terms are used in 48 C.F.R. 12.212 (SEPT 1995) 
# and is provided to the U.S. Government only as a commercial end item.  
# Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through 
# 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the 
# source code with only those rights set forth herein.
#
################################################################################
#
# Makefile project only supported on Mac OSX and Linux Platforms)
#
################################################################################

# OS Name (Linux or Darwin)
OSUPPER = $(shell uname -s 2>/dev/null | tr [:lower:] [:upper:])
OSLOWER = $(shell uname -s 2>/dev/null | tr [:upper:] [:lower:])

# Flags to detect 32-bit or 64-bit OS platform
OS_SIZE = $(shell uname -m | sed -e "s/i.86/32/" -e "s/x86_64/64/")
OS_ARCH = $(shell uname -m | sed -e "s/i386/i686/")

# These flags will override any settings
ifeq ($(i386),1)
	OS_SIZE = 32
	OS_ARCH = i686
endif

ifeq ($(x86_64),1)
	OS_SIZE = 64
	OS_ARCH = x86_64
endif

# Flags to detect either a Linux system (linux) or Mac OSX (darwin)
DARWIN = $(strip $(findstring DARWIN, $(OSUPPER)))

# Location of the CUDA Toolkit binaries and libraries
CUDA_PATH       ?= /usr/local/cuda-5.5
CUDA_INC_PATH   ?= $(CUDA_PATH)/include
CUDA_BIN_PATH   ?= $(CUDA_PATH)/bin
ifneq ($(DARWIN),)
  CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib
else
  ifeq ($(OS_SIZE),32)
    CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib
  else
    CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib64
  endif
endif

# Common binaries
NVCC            ?= $(CUDA_BIN_PATH)/nvcc
GCC             ?= g++

# Extra user flags
EXTRA_NVCCFLAGS ?= -DCUDA_DEVICE=0 -DHOST=__host__ -DDEVICE=__device__ -DCONSTANT=__constant__ -DGLOBAL=__global__ -DSHARED=__shared__ -Dfalse=0 -Dtrue=1 -DREQ_BUF_SIZE=512 -DRESPONSE_HEADERS_LEN=1024 -DCOHORT_SIZE=4096 -DRANDOM_MAX_NUMBER=32768 -DCOHORTS_NUM=32 -DCOHORTS_IMAGES_NUM=4 -DMAX_SESSIONS_PER_BUCKET=16384 -DBESIM_REQUEST_LEN=1024 -DBESIM_RESPONSE_LEN=4096 -DNUM_PARSERS=1 -DREADER_NUM_BACK_BUFFERS=2 -DQUERY_STRING_LEN=512 -DTHREAD_BLOCK_SIZE=256 -D_REQUEST_GENERATE_MODE_ -D_THREADED_BACKEND_ -D_READER_THREAD_
COMMAND_NVCCFLAGS ?= 
#EXTRA_NVCCFLAGS ?= -DRESPONSE_LEN=65536 -DHOST=__host__ -DDEVICE=__device__ -DCONSTANT=__constant__ -DGLOBAL=__global__ -DSHARED=__shared__ -Dfalse=0 -Dtrue=1 -DREQ_BUF_SIZE=1024 -DRESPONSE_HEADERS_LEN=1024 -DCOHORT_SIZE=32 -DRANDOM_MAX_NUMBER=32768 -DCOHORTS_NUM=32 -DCOHORTS_IMAGES_NUM=2 -DMAX_SESSIONS_PER_BUCKET=1024 -DBESIM_REQUEST_LEN=1024 -DBESIM_RESPONSE_LEN=16384 -DNUM_PARSERS=1 -DREADER_NUM_BACK_BUFFERS=2 -DNUM_RESPONSES=8 -DQUERY_STRING_LEN=512 -DNUM_REQUEST_HANDLERS=8 -D_USE_REMOTE_BESIM_
EXTRA_LDFLAGS   ?=

# CUDA code generation flags
GENCODE_SM10    := -gencode arch=compute_10,code=sm_10
GENCODE_SM20    := -gencode arch=compute_20,code=sm_20
GENCODE_SM30    := -gencode arch=compute_30,code=sm_30
GENCODE_SM35    := -gencode arch=compute_35,code=sm_35
GENCODE_FLAGS   := $(GENCODE_SM35)

# OS-specific build flags
ifneq ($(DARWIN),) 
      LDFLAGS   := -Xlinker -rpath $(CUDA_LIB_PATH) -L$(CUDA_LIB_PATH) -lcudart -lcuda -lrt
      CCFLAGS   := -arch $(OS_ARCH) 
else
  ifeq ($(OS_SIZE),32)
      LDFLAGS   := -L$(CUDA_LIB_PATH) -lcudart -lcuda -lrt
      CCFLAGS   := -m32
  else
      LDFLAGS   := -L$(CUDA_LIB_PATH) -lcudart -lrt -lnvToolsExt
      CCFLAGS   := -m64
  endif
endif

# OS-architecture specific flags
ifeq ($(OS_SIZE),32)
      NVCCFLAGS := -m32
else
      NVCCFLAGS := -m64
endif

# Debug build flags
ifeq ($(dbg),1)
      CCFLAGS   += -g
      NVCCFLAGS += -g -G -DDEBUG --ptxas-options=-v -Xptxas -v
      TARGET    := debug
else
      NVCCFLAGS += -O3 -lineinfo -DDEBUG
      TARGET    := release
endif

# Common includes and paths for CUDA
INCLUDES      := -I$(CUDA_INC_PATH) -I. -Iutils -Ibackend -Irequests 

# Target rules
all: build

build: program.cuda

program.cuda.o: server.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(COMMAND_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -o $@ -c $<

program.cuda: program.cuda.o
	$(GCC) $(CCFLAGS) -o $@ $+ $(LDFLAGS) $(EXTRA_LDFLAGS)

clean:
	rm -f program.cuda program.cuda.o
