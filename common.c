#ifndef COMMON_C_INCLUDED
#define COMMON_C_INCLUDED

#define MAX_IMAGES 1000

#include <dirent.h>
#include "common.h"
#include "utils/utils.c"

char image_names[MAX_IMAGES][256];
char images_data[MAX_IMAGES][RESPONSE_LEN];
int images_len[MAX_IMAGES];
int num_images = 0;

void read_file(char* name, char* buffer, int* len)
{
 assert(name != NULL);
 assert(buffer != NULL);
 int bytes_read = 0;
 FILE *f = fopen(name, "rb");
 if (f == NULL) {
  fprintf(stderr, "Couldn't open file %s\n", name);
  if(len)
   *len = 0;
 }
 bytes_read = fread(buffer, sizeof(char), RESPONSE_LEN, f);
 buffer[bytes_read] = '\0';
 fclose(f);
 if(len)
  *len = bytes_read;
}

void read_images()
{
 DIR *dir;
 struct dirent *ent;
 dir = opendir("images");
 if (dir != NULL) {
  /* print all the files and directories within directory */
  while ((ent = readdir (dir)) != NULL) {
   if(ent->d_type == DT_REG) {
    char path[256] = "images/";
    read_file(strcat(path, ent->d_name), images_data[num_images], images_len + num_images);
    strcpy(image_names[num_images], ent->d_name);
    
    num_images++;
   }
  }
  fprintf(stderr, "\nRead %d images", num_images);
  closedir(dir);
 } else {
  /* could not open directory */
  perror ("");
 }
}

DEVICE int search_images(char* images, int count, char* str)
{
 int i;
 for(i=0; i<count; i++) {
  if(!d_strcmp((images + i*256), str)) {
   return i;
  }
 }
 return -1;
}


DEVICE int get_value(request* r, char* key, int key_len, char* value)
{
 char *ssave = "";
 char* p1;
 char p[QUERY_STRING_LEN];
 d_strcpy(p, r->query_string);
 p1 = d_strtok_r(p, "&", &ssave);
 while(p1 != NULL) {
  if(!d_strncmp(p1, key, key_len)) {
   d_strcpy(value, p1 + key_len + 1);
   return 1;
  }
  p1 = d_strtok_r(NULL, "&", &ssave);
 }
 return 0;
}

DEVICE void get_array(request* r, char* key, int key_len, int* num, char value[MAX_VALUES][VALUE_LEN])
{
 char* ssave = "";
 char* p1;
 int n = 0;
 char p[QUERY_STRING_LEN];
 d_strcpy(p, r->query_string);
 p1 = d_strtok_r(p, "&", &ssave);
 while(p1 != NULL) {
  if(!d_strncmp(p1, key, key_len)) {
   d_strcpy(value[n], p1 + key_len + 4);
   n++;
  }
  p1 = d_strtok_r(NULL, "&", &ssave);
 }
 if(num) {
  *num = n;
 }
}

void print_device_memory()
{
 size_t curr_mem = 0;
 size_t total_mem = 0;
 check_cuda(cudaMemGetInfo(&curr_mem, &total_mem));
 fprintf(stderr, "\nTotal Memory: %ld MB, Free Memory: %ld MB", total_mem/(1024*1024), curr_mem/(1024*1024));
}
#endif