#ifndef MEMORY_C_INCLUDED
#define MEMORY_C_INCLUDED

unsigned long int memory_pinned_host_allocated = 0;
unsigned long int memory_host_allocated = 0;
unsigned long int memory_device_allocated = 0;

void* alloc_pinned_host(size_t size)
{
 void* ptr = NULL;
 assert(size > 0);
 check_cuda(cudaHostAlloc(&ptr, size, cudaHostAllocDefault));
 assert(ptr != NULL);
 memory_pinned_host_allocated += size;
 return ptr;
}

void* alloc_host(size_t size)
{
 void* ptr = NULL;
 assert(size > 0);
 ptr = malloc(size);
 assert(ptr != NULL);
 memory_host_allocated += size;
 return ptr;
}

void* alloc_device(size_t size)
{
 void* ptr = NULL;
 assert(size > 0);
 check_cuda(cudaMalloc(&ptr, size));
 assert(ptr != NULL);
 memory_device_allocated += size;
 //fprintf(stderr, "\nHost(%ld) Pinned Host(%ld) Device(%ld) MB", memory_host_allocated/(1024*1024), memory_pinned_host_allocated/(1024*1024), memory_device_allocated/(1024*1024));
 return ptr;
}

cudaStream_t alloc_stream()
{
 cudaStream_t ptr = NULL;
 check_cuda(cudaStreamCreate(&ptr));
 assert(ptr != NULL);
 return ptr;
}

void free_pinned_host(void* ptr)
{
 assert(ptr != NULL);
 check_cuda(cudaFreeHost(ptr));
}

void free_host(void* ptr)
{
 assert(ptr != NULL);
 free(ptr);
}

void free_device(void* ptr)
{
 assert(ptr != NULL);
 check_cuda(cudaFree(ptr));
}

void free_stream(cudaStream_t ptr)
{
 assert(ptr != NULL);
 check_cuda(cudaStreamDestroy(ptr));
}

void copy_to_device(void* dst, void* src, size_t bytes)
{
 assert(dst != NULL);
 assert(src != NULL);
 assert(bytes > 0);
 check_cuda(cudaMemcpy(dst, src, bytes, cudaMemcpyHostToDevice));
}

void copy_to_device_async(void* dst, void* src, size_t bytes, cudaStream_t stream)
{
 assert(dst != NULL);
 assert(src != NULL);
 assert(bytes > 0);
 check_cuda(cudaMemcpyAsync(dst, src, bytes, cudaMemcpyHostToDevice, stream));
}

void copy_to_host_async(void* dst, void* src, size_t bytes, cudaStream_t stream)
{
 assert(dst != NULL);
 assert(src != NULL);
 assert(bytes > 0);
 check_cuda(cudaMemcpyAsync(dst, src, bytes, cudaMemcpyDeviceToHost, stream));
}

void memory_reset_record()
{
 memory_pinned_host_allocated = 0;
 memory_host_allocated = 0;
 memory_device_allocated = 0;
}

void memory_print_record()
{
 fprintf(stderr, "\nHost(%ld) Pinned Host(%ld) Device(%ld) MB", memory_host_allocated/(1024*1024), memory_pinned_host_allocated/(1024*1024), memory_device_allocated/(1024*1024));
}

#endif
