#ifndef DISPATCH_C_INCLUDED
#define DISPATCH_C_INCLUDED

void dispatch_create()
{
 int i;
 for(i=0; i<REQUEST_URI_MAX; i++) {
  if(once_funcs[i] != NULL) {
   (*once_funcs[i])();
  }
 }
}

int dispatch_main(cohort_data* c)
{
 int i, j;
 //fprintf(stderr, "\n%s(%d)", __FUNCTION__, __LINE__);
 for(i=0; i<COHORTS_IMAGES_NUM; i++) {
  cohort_state* curr_state = &c->state_image[i];
  if(curr_state->state == COHORT_READY) {
  response_data* resp = response_find_free(responses);
   if(resp == NULL) {
    fprintf(stderr, "\nNot enough responses");
    return -1;
   }
   resp->type = RESPONSE_TYPE_IMAGE;
   curr_state->state = COHORT_BEING_PROCESSED;
   for(j=0; j<COHORT_SIZE; j++) {
    request_image* curr_req = &h_cohorts_image[i].buffer[j];
    resp->resp_images[j] = images_data[curr_req->image_index];
    resp->resp_len[j] = images_len[curr_req->image_index];
    resp->fds[j] = curr_req->client;
   }
   response_add(resp);
   cohort_free_image(i);
  }
 }
 for(i=0; i<COHORTS_NUM; i++) {
  cohort_state* curr_state = &c->state[i];
  if(curr_state->state == COHORT_READY) {
   response_data* resp = response_find_free(responses);
   if(resp == NULL) {
    //fprintf(stderr, "\nNot enough responses %d", i);
    return -1;
   }
   request_handler* r = request_handler_find_free(handlers);
   if(r == NULL) {
    fprintf(stderr, "\nNot enough request handlers");
    return -1;
   }
   resp->type = RESPONSE_TYPE_HTML;
   curr_state->state = COHORT_BEING_PROCESSED;
   r->cohort_id = i;
   r->uri = curr_state->uri;
   r->resp = resp;
   if(process_funcs[curr_state->uri] != NULL) {
    (*process_funcs[curr_state->uri])(r);
   } else {
    cohort_free(i);
    request_handler_free(r);
    response_free(resp);
   }
  }
 }
 return 0;
}

#endif
