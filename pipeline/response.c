#ifndef RESPONSE_C_INCLUDED
#define RESPONSE_C_INCLUDED

#define RESPONSE_TYPE_NONE 0
#define RESPONSE_TYPE_HTML 1
#define RESPONSE_TYPE_IMAGE 2

list response_list;

CONSTANT char response_header_str[] = 
"HTTP/1.1 200 OK\r\n"
"Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n"
"Server: Rhythm/0.1.0\r\n"
"Content-Type: text/html\r\n"
"Cache-Control: no-cache\r\n";
//"Connection: Keep-Alive\r\n";

DEVICE int len_response_header_str = 126;

void response_free(response_data* r)
{
 assert(r != NULL);
 r->type = RESPONSE_TYPE_NONE;
}

GLOBAL void response_generate_headers(char* headers, int* headers_len, int* response_len)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 char scratch[20] = {0};
 char* buffer_iter = headers + tid;
 APPEND_TO_BUFFER_TRANSPOSED(response_header_str)
 APPEND_TO_BUFFER_TRANSPOSED("Content-Length: ")
 d_itoa((int)response_len[tid], scratch, 10, NULL);
 APPEND_TO_BUFFER_TRANSPOSED(scratch)
 APPEND_TO_BUFFER_TRANSPOSED("\r\n\r\n")
 *buffer_iter = 0;
 headers_len[tid] = (int)((buffer_iter - headers - tid)/COHORT_SIZE);
}

int _response_cb(void* data)
{
 response_data* r = (response_data*)data;
 if(cudaStreamQuery(r->stream) == cudaSuccess) {
  int i;
  if(r->type == RESPONSE_TYPE_IMAGE) {
   for(i=0; i<COHORT_SIZE; i++) {
#if !defined(_TRACE_USE_MODE_) && !defined(_REQUEST_GENERATE_MODE_)
    if(write(r->fds[i], r->headers + i*RESPONSE_HEADERS_LEN, r->headers_len[i]) != r->headers_len[i]) {
     error_die("write");
    }
    if(write(r->fds[i], r->resp_images[i], r->resp_len[i]) != r->resp_len[i]) {
     error_die("write");
    }
#endif    
   }
  } else {
   for(i=0; i<COHORT_SIZE; i++) {
    //fprintf(stderr, "\n%s", r->resp + i*RESPONSE_LEN);
#if !defined(_TRACE_USE_MODE_) && !defined(_REQUEST_GENERATE_MODE_)
    if(write(r->fds[i], r->resp + i*RESPONSE_LEN, r->resp_len[i]) != r->resp_len[i]) {
     error_die("write");
    }
#endif
   }
  }
  response_free(r);
  return false;
 }
 return true;
}

void response_add(response_data* r)
{
 dim3 grid1(COHORT_SIZE/TILE_DIM,RESPONSE_HEADERS_LEN/TILE_DIM),grid2(COHORT_SIZE/TILE_DIM,RESPONSE_LEN/TILE_DIM),threads(TILE_DIM,BLOCK_ROWS);
 if(r->type == RESPONSE_TYPE_IMAGE) {
  copy_to_device_async(r->d_resp_len, r->resp_len, COHORT_SIZE*sizeof(int), r->stream);
  response_generate_headers<<<1, COHORT_SIZE, 0, r->stream>>>(r->d_transposed_headers, 
                                                        r->d_headers_len, 
                                                        r->d_resp_len);
 transpose<<<grid1, threads, 0, r->stream>>>(r->d_headers, r->d_transposed_headers, COHORT_SIZE, RESPONSE_HEADERS_LEN);
 copy_to_host_async(r->headers, r->d_headers, COHORT_SIZE*RESPONSE_HEADERS_LEN, r->stream);
 copy_to_host_async(r->headers_len, r->d_headers_len, COHORT_SIZE*sizeof(int), r->stream);
 } else {
  transpose<<<grid2, threads, 0, r->stream>>>(r->d_resp, r->d_transposed_resp, COHORT_SIZE, RESPONSE_LEN);
  copy_to_host_async(r->resp, r->d_resp, COHORT_SIZE*RESPONSE_LEN, r->stream);
  copy_to_host_async(r->resp_len, r->d_resp_len, COHORT_SIZE*sizeof(int), r->stream);
  copy_to_host_async(r->fds, r->d_fds, COHORT_SIZE*sizeof(int), r->stream);
 }
 callbacks_add(_response_cb, r);
}

response_data* response_create(int id)
{
 response_data* r = (response_data*)alloc_host(sizeof(response_data));
 r->type = RESPONSE_TYPE_NONE;
 r->resp_images = (char**)alloc_host(COHORT_SIZE*sizeof(char*));
 r->resp = (char*)alloc_pinned_host(COHORT_SIZE*RESPONSE_LEN);
 r->resp_len = (int*)alloc_pinned_host(COHORT_SIZE*sizeof(int));
 r->headers = (char*)alloc_pinned_host(COHORT_SIZE*RESPONSE_HEADERS_LEN);
 r->headers_len = (int*)alloc_pinned_host(COHORT_SIZE*sizeof(int));
 r->fds = (int*)alloc_pinned_host(COHORT_SIZE*sizeof(int));
 r->d_fds = (int*)alloc_device(COHORT_SIZE*sizeof(int));
 r->d_resp = (char*)alloc_device(COHORT_SIZE*RESPONSE_LEN);
 r->d_transposed_resp = (char*)alloc_device(COHORT_SIZE*RESPONSE_LEN);
 r->d_resp_len = (int*)alloc_device(COHORT_SIZE*sizeof(int));
 r->d_headers = (char*)alloc_device(COHORT_SIZE*RESPONSE_HEADERS_LEN);
 r->d_transposed_headers = (char*)alloc_device(COHORT_SIZE*RESPONSE_HEADERS_LEN);
 r->d_headers_len = (int*)alloc_device(COHORT_SIZE*sizeof(int));
 r->d_session_id = (unsigned int*)alloc_device(COHORT_SIZE*sizeof(unsigned int));
 r->stream = alloc_stream();
 r->id = id;
 char namebuf[64];
 sprintf(namebuf, "Response %d", id);
 nvtxNameCudaStreamA(r->stream, namebuf);
 return r;
}

response_data** response_create_pool(int size, int num)
{
 int i;
 response_data** p = (response_data**)alloc_host(num*sizeof(response_data*));
 for(i=0; i<num; i++) {
  p[i] = response_create(i);
 }
 return p;
}

response_data* response_find_free(response_data** pool)
{
 int i;
 for(i=0; i<NUM_RESPONSES; i++) {
  if(pool[i]->type == RESPONSE_TYPE_NONE) {
   return pool[i];
  }
 }
 return NULL;
}

response_data* response_find_busy(response_data** pool)
{
 int i;
 for(i=0; i<NUM_RESPONSES; i++) {
  if(pool[i]->type != RESPONSE_TYPE_NONE) {
   return pool[i];
  }
 }
 return NULL;
}

#endif
