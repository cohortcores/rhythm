#ifndef REQUEST_HANDLER_C_INCLUDED
#define REQUEST_HANDLER_C_INCLUDED

request_handler* request_handler_create(int n_req, session_bucket* session_array, int id)
{
 request_handler* r = (request_handler*)alloc_host(sizeof(request_handler));
 r->uri = REQUEST_URI_NONE;
 r->resp = NULL;
 r->d_session_array = session_array;
 r->be = backend_create();
 r->stream = alloc_stream();
 char namebuf[64];
 sprintf(namebuf, "Request handler %d", id);
 nvtxNameCudaStreamA(r->stream, namebuf);
 r->d_req_state = (int*)alloc_device(n_req*sizeof(int));
 r->req_state = (int*)alloc_pinned_host(n_req*sizeof(int));
 r->id = id;
// r->d_profile = (profiler_data*)alloc_device(n_req*sizeof(profiler_data));
 return r;
}

void request_handler_free(request_handler* r)
{
 assert(r != NULL);
 r->uri = REQUEST_URI_NONE;
}

request_handler** request_handler_create_pool(int size, int num, session_bucket* session_array)
{
 int i;
 request_handler** p = (request_handler**)alloc_host(num*sizeof(request_handler*));
 for(i=0; i<num; i++) {
  p[i] = request_handler_create(size, session_array, i);
 }
 return p;
}

request_handler* request_handler_find_free(request_handler** pool)
{
 int i;
 for(i=0; i<NUM_REQUEST_HANDLERS; i++) {
  if(pool[i]->uri == REQUEST_URI_NONE) {
   return pool[i];
  }
 }
 return NULL;
}

request_handler* request_handler_get_reference(int id)
{
 if(handlers[id]->uri != REQUEST_URI_NONE) {
  return handlers[id];
 }
 return NULL;
}
#endif
