#ifndef READER_C_INCLUDED
#define READER_C_INCLUDED

#define TRACE_BUF_FILE "request_trace_buffers.txt"
#define TRACE_SIZE_FILE "request_trace_sizes.txt"
FILE* trace_buf_fd = NULL;
FILE* trace_size_fd = NULL;

void reader_swap(reader_data** r1, reader_data** r2)
{
 reader_data* temp = *r1;
 *r1 = *r2;
 *r2 = temp;
}

void reader_free(reader_data* r)
{
 r->n_clients = 0;
 r->state = READER_READY;
}

void reader_read_requests(int client, reader_data* r)
{
 int bytes_read = 0;
 nvtxEventAttributes_t eventAttrib = {0};
 eventAttrib.version = NVTX_VERSION;
 eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
 eventAttrib.colorType = NVTX_COLOR_ARGB;
 eventAttrib.color = 0xFF00FF00;
 eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;
 eventAttrib.message.ascii = "Reader full";
#if defined(_TRACE_USE_MODE_)
 size_t result;
 result = fread(&bytes_read, sizeof(int), 1, trace_size_fd);
 assert(result == 1);
 result = fread((r->reqs.req_buf + r->n_clients*REQ_BUF_SIZE), 1, bytes_read, trace_buf_fd);
 assert(result == bytes_read);
#elif defined(_REQUEST_GENERATE_MODE_)
 bytes_read = random_requests_len[request_count&(RANDOM_REQ_COUNT-1)];
 strcpy(r->reqs.req_buf + r->n_clients*REQ_BUF_SIZE, random_requests[request_count&(RANDOM_REQ_COUNT-1)]);
#else
 bytes_read = read(client, (r->reqs.req_buf + r->n_clients*REQ_BUF_SIZE), REQ_BUF_SIZE);
#endif
 request_count++;
 assert(bytes_read <= REQ_BUF_SIZE);
 if(bytes_read == 0) {
//  session_cleanup(client);
  close(client);
 } else {
  r->reqs.bytes_read[r->n_clients] = bytes_read;
  r->reqs.fds[r->n_clients] = client;
#if defined(_TRACE_GENERATE_MODE_)
  fwrite((r->reqs.req_buf + r->n_clients*REQ_BUF_SIZE), 1, bytes_read, trace_buf_fd);
  fwrite(&bytes_read, sizeof(int), 1, trace_size_fd);
#endif
  r->n_clients++;
  if(r->n_clients == COHORT_SIZE) {
   r->state = READER_FULL;
   //nvtxMarkEx(&eventAttrib);
  }
 }
}

reader_data* reader_create()
{
 reader_data* r = (reader_data*)alloc_pinned_host(sizeof(reader_data));
 r->n_clients = 0;
 r->state = READER_READY;
 return r;
}

#if defined(_READER_THREAD_)
void* reader_thread_worker(void* data)
{
 nvtxNameOsThread(pthread_self(), "Reader thread");
 while(1) {
  pthread_mutex_lock(&readerMutex);
  if(reader_front->state != READER_FULL) {
   reader_read_requests(0, reader_front);
   if(reader_front->state == READER_FULL) {
    raise_event(READER_FULL, NULL);
   }
  }
  pthread_mutex_unlock(&readerMutex);
  if(request_count == TRACE_MAX_NUM) {
   break;
  }
 }
 pthread_exit(NULL);
 return NULL;
}

void reader_thread_create()
{
 pthread_t t;
 pthread_create(&t, NULL, &reader_thread_worker, NULL);
}
#endif

reader_data** reader_create_pool(int size, int num)
{
 int i;
 reader_data** r = (reader_data**)alloc_host(num*sizeof(reader_data*));
 for(i=0; i<num; i++) {
  r[i] = reader_create();
 }
 return r;
}

reader_data* reader_find_free(reader_data** pool)
{
 int i;
 for(i=0; i<READER_NUM_BACK_BUFFERS; i++) {
  if(pool[i]->state == READER_READY) {
   return pool[i];
  }
 }
 return NULL;
}

#endif