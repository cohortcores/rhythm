#ifndef PARSER_C_INCLUDED
#define PARSER_C_INCLUDED

DEVICE char *get_filename_ext(const char *filename) {
 char *dot = d_strrchr(filename, '.');
 if(!dot || dot == filename) return "";
 return dot + 1;
}

DEVICE void _request_init(request* r)
{
 r->type = REQUEST_TYPE_NONE;
 r->session_id = -1;
 r->content_length = 0;
 r->connection_close = 0;
 r->query_string[0] = '\0';
}

DEVICE int _parse_one_request_1(char* buf, request* r, char* images, int images_count)
{
 char* line = buf;
 char method[8];
 char uri[512];
 char* query_string = NULL;
 int i = 0, j = 0;
 
 _request_init(r);
  /* get method */
 while(!d_isspace(line[TRANSPOSE(j)])) {
  method[i] = line[TRANSPOSE(j)];
  i++; j++;
 }
 method[i] = '\0';
 if(d_strcasecmp(method, "GET") == 0) {
  r->type = REQUEST_TYPE_GET;
 } else if(d_strcasecmp(method, "POST") == 0) {
  r->type = REQUEST_TYPE_POST;
 }
 /* get uri */
 i = 0;
 while(d_isspace(line[TRANSPOSE(j)]))
  j++;
 while(!d_isspace(line[TRANSPOSE(j)])) {
  uri[i] = line[TRANSPOSE(j)];
  if(uri[i] == '?') {
   uri[i] = '\0';
   query_string = uri+i+1;
  }
  i++; j++;
 }
 uri[i] = '\0';
 /* check for query string */
 if(r->type == REQUEST_TYPE_GET && query_string != NULL) {
  d_strcpy(r->query_string, query_string);
 }
 if(!d_strcasecmp(get_filename_ext(uri), "jpg")) {
  r->uri = REQUEST_URI_IMAGE;
 } else {
  r->uri = get_uri(uri);
 }
 if(r->uri == REQUEST_URI_IMAGE) {
  r->image_index = search_images(images, images_count, uri+13);
 }
 /* move to next line */
 while(line[TRANSPOSE(j)] != '\n')
  j++;
 j++;
 return j;
}

DEVICE int _parse_one_request_2(char* buf, request* r)
{
 char* line = buf;
 int i = 0, j = 0;
 
 /* read & discard headers */
 while(!(line[0] == '\r' && line[TRANSPOSE(1)] == '\n')) {
  if(d_strncasecmp_transposed(line, "Content-Length:", 15) == 0) {
   r->content_length = d_atoi_transposed(&(line[TRANSPOSE(16)]));
  } else if(d_strncasecmp_transposed(line, "Cookie: SESSIONID=", 18) == 0) {
   r->session_id = d_atoi_transposed(&(line[TRANSPOSE(18)]));
  } else if(d_strncasecmp_transposed(line, "Connection: close", 17) == 0) {
   r->connection_close = 1;
  }
  /* move to next line */
  while(buf[TRANSPOSE(j)] != '\n')
   j++;
  j++;
  line = buf+TRANSPOSE(j);
 }
 j++; j++; /* \r\n */
 if(r->type == REQUEST_TYPE_POST && r->content_length > 0) {
  for(i=0; i<r->content_length; i++) {
   r->query_string[i] = buf[TRANSPOSE((j+i))];
  }
  r->query_string[r->content_length] = '\0';
  j += r->content_length;
 }
 return j;
}

DEVICE int _parse_one_request_2_image(char* buf, request_image* r)
{
 char* line = buf;
 int j = 0;
 /* read & discard headers */
 while(!(line[0] == '\r' && line[TRANSPOSE(1)] == '\n')) {
  if(d_strncasecmp_transposed(line, "Cookie: SESSIONID=", 18) == 0) {
   r->session_id = d_atoi_transposed(&(line[TRANSPOSE(18)]));
  } else if(d_strncasecmp_transposed(line, "Connection: close", 17) == 0) {
   r->connection_close = 1;
  }
  /* move to next line */
  while(buf[TRANSPOSE(j)] != '\n')
   j++;
  j++;
  line = buf+TRANSPOSE(j);
 }
 j++; j++; /* \r\n */
 return j;
}

GLOBAL void _parse_http_request(request_data* reqs, cohort_data* cohorts_state, cohort_image* c_image, char* images, int images_count)
{
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 char* buf = reqs->transposed_req_buf + tid;
 int bytes_processed = 0;

 while(bytes_processed < reqs->bytes_read[tid]) {
  request r1;
  bytes_processed += _parse_one_request_1(buf+TRANSPOSE(bytes_processed), &r1, images, images_count);
  if(r1.uri == REQUEST_URI_IMAGE) {
   request_image* r2 = cohort_add_image(c_image, cohorts_state->state_image, r1.uri);
   r2->type = r1.type;
   r2->uri = r1.uri;
   r2->client = reqs->fds[tid];
   r2->image_index = r1.image_index;
   bytes_processed += _parse_one_request_2_image(buf+TRANSPOSE(bytes_processed), r2);
   //printf("\n%d\t%d\t%d", r2->client, r2->uri, r2->image_index);
  } else {
   request* r2 = cohort_add(cohorts_state->state, r1.uri);
   r2->type = r1.type;
   r2->uri = r1.uri;
   r2->client = reqs->fds[tid];
   if(r2->type == REQUEST_TYPE_GET) {
    d_strcpy(r2->query_string, r1.query_string);
   }
   bytes_processed += _parse_one_request_2(buf+TRANSPOSE(bytes_processed), r2);
   //printf("\n%d\t%d\t%s", r2->client, r2->uri, r2->query_string);
  }
 }
}

void parser_main(parser_data* p, reader_data* r);

int parser_cb(void* data)
{
 parser_data* p = (parser_data*)data;
 if(p->state == PARSER_BUSY && cudaStreamQuery(p->stream) == cudaSuccess) {
  //fprintf(stderr, "\n%s(%d)", __FUNCTION__, __LINE__);
  if(dispatch_main(h_cohorts_state) == -1) {
   return true;
  }
  p->state = PARSER_READY;
#if defined(_READER_THREAD_)
  pthread_mutex_lock(&readerMutex);
  reader_free(p->reader);
  reader_data* r = reader_find_free(readers);
  if(r != NULL && reader_front->state == READER_FULL) {
   reader_swap(&reader_front, &r);
   pthread_mutex_unlock(&readerMutex);
   /* Launch asynchronous parser kernels */
   parser_main(p, r);
   return false;
  }
  pthread_mutex_unlock(&readerMutex);
#else
  reader_free(p->reader);
#endif  
  return false;
 }
 return true;
}

void _update_recycle_state(cohort_data* h)
{
 int i;
 for(i=0; i<COHORTS_NUM; i++) {
  if(recycle_list[i] == 1) {
   memset(&h->state[i], 0, sizeof(cohort_state));
   recycle_list[i] = 0;
  }
 }
 for(i=0; i<COHORTS_IMAGES_NUM; i++) {
  if(recycle_list_images[i] == 1) {
   memset(&h->state_image[i], 0, sizeof(cohort_state));
   recycle_list_images[i] = 0;
  }
 }
}

GLOBAL void transpose_parse(request_data* r, int width, int height)
{
 SHARED int tile[TILE_DIM][TILE_DIM+1];
 int i;
 char* idata = r->req_buf;
 char* odata = r->transposed_req_buf;
 int xIndex = blockIdx.x*TILE_DIM + threadIdx.x;
 int yIndex = blockIdx.y*TILE_DIM + threadIdx.y;
 int index_in = xIndex + (yIndex)*width;
 xIndex = blockIdx.y*TILE_DIM + threadIdx.x;
 yIndex = blockIdx.x*TILE_DIM + threadIdx.y;
 int index_out = xIndex + (yIndex)*height;

 for(i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
  tile[threadIdx.y+i][threadIdx.x] = idata[index_in+i*width];
 }
 __syncthreads();
 for(i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
  odata[index_out+i*height] = tile[threadIdx.x][threadIdx.y+i];
 }
}

void parser_main(parser_data* p, reader_data* r)
{
 //fprintf(stderr, "\n%s(%d)", __FUNCTION__, __LINE__);
 assert(p != NULL);
 p->state = PARSER_BUSY;
 p->reader = r;
 dim3 grid(REQ_BUF_SIZE/TILE_DIM, COHORT_SIZE/TILE_DIM), threads(TILE_DIM,BLOCK_ROWS);

 _update_recycle_state(h_cohorts_state);

 copy_to_device_async(p->d_reqs, &r->reqs, sizeof(request_data), p->stream);
 copy_to_device_async(d_cohorts_state, h_cohorts_state, sizeof(cohort_data), p->stream);

 transpose_parse<<<grid, threads, 0, p->stream>>>(p->d_reqs, REQ_BUF_SIZE, COHORT_SIZE);
 _parse_http_request<<<COHORT_SIZE/THREAD_BLOCK_SIZE, THREAD_BLOCK_SIZE, 0, p->stream>>>(p->d_reqs, d_cohorts_state, d_cohorts_image, d_image_names, num_images);

 copy_to_host_async(h_cohorts_state, d_cohorts_state, sizeof(cohort_data), p->stream);
 copy_to_host_async(h_cohorts_image, d_cohorts_image, COHORTS_IMAGES_NUM*sizeof(cohort_image), p->stream);
 callbacks_add(parser_cb, p);
}

parser_data* parser_create()
{
 parser_data* p = (parser_data*)alloc_host(sizeof(parser_data));
 p->state = PARSER_READY;
 p->d_reqs = (request_data*)alloc_device(sizeof(request_data));
 p->stream = alloc_stream();
 nvtxNameCudaStreamA(p->stream, "Parser");
 return p;
}

parser_data** parser_create_pool(int num_parsers)
{
 int i;
 parser_data** p = (parser_data**)alloc_host(num_parsers*sizeof(parser_data*));
 for(i=0; i<num_parsers; i++) {
  p[i] = parser_create();
 }
 return p;
}

parser_data* parser_find_free(parser_data** pool)
{
 int i;
 for(i=0; i<NUM_PARSERS; i++) {
  if(pool[i]->state == PARSER_READY) {
   return pool[i];
  }
 }
 return NULL;
}

void parser_free(parser_data* p)
{
 p->state = PARSER_READY;
}

#endif
